import { TestBed } from '@angular/core/testing';

import { OfficeDepotService } from './office-depot.service';

describe('OfficeDepotService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OfficeDepotService = TestBed.get(OfficeDepotService);
    expect(service).toBeTruthy();
  });
});
