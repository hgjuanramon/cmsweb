import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { CardStatusResponse } from "../models/response/card-status.response";
import { CardDataResponse } from "../models/response/card-data.response";
import { CardHistoryRequest } from "../models/request/card-history.request";
import { CardHistoryResponse } from "../models/response/card-history.response";
import { CardExpiredResponse } from "../models/response/card-expired.response";
import { CardExpiredSearchRequest } from "../models/request/card-expired-search.request";
import { CardStatusRequest } from "../models/request/card-status.request";
import { Response } from "src/app/core/models/response";
import { CardExpiredRequest } from "../models/request/card-expired.request";
import { ApiOfficeDepotConstant } from "../constants/api-office-depot.constant";

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json",
    token:
      "I;R@N)6qr!mx=r_ll<-B?FdOQN)=o{I]'MkE6Dk_9|#:Yc2[^#r6ED/`|Xg$=I5_16}h49X8g+W;*{~VR,eC63/}(4NuNCV#l$H/2iyxSa8c+Ey[2`r*b95xGanip^"
  })
};

@Injectable({
  providedIn: "root"
})
export class OfficeDepotService {
  constructor(private httpClient: HttpClient) {}

  public getCardStatus = (): Observable<CardStatusResponse> => {
    return this.httpClient.post<CardStatusResponse>(
      ApiOfficeDepotConstant.GET_CARD_STATUS_ENDPOINT,
      {},
      httpOptions
    );
  };

  public getCardData = (cardNumber: string): Observable<CardDataResponse> => {
    return this.httpClient.post<CardDataResponse>(
      ApiOfficeDepotConstant.GET_CARD_DATA_ENDPOINT,
      { Cadena: cardNumber },
      httpOptions
    );
  };

  public getCardHistory = (
    request: CardHistoryRequest
  ): Observable<CardHistoryResponse> => {
    return this.httpClient.post<CardHistoryResponse>(
      ApiOfficeDepotConstant.GET_CARD_HISTORY_ENDPOINT,
      request,
      httpOptions
    );
  };

  public getCardExpired = (
    request: CardExpiredSearchRequest
  ): Observable<CardExpiredResponse> => {
    return this.httpClient.post<CardExpiredResponse>(
      ApiOfficeDepotConstant.GET_CARD_EXPIRED_ENDPOINT,
      request,
      httpOptions
    );
  };

  public updateCardStatus = (
    request: CardStatusRequest
  ): Observable<Response> => {
    return this.httpClient.post<Response>(
      ApiOfficeDepotConstant.UPDATE_CARD_STATUS_ENDPOINT,
      request,
      httpOptions
    );
  };

  public updateCardExpired = (
    request: CardExpiredRequest
  ): Observable<Response> => {
    return this.httpClient.post<Response>(
      ApiOfficeDepotConstant.UPDATE_CARD_EXPIRED_ENDPOINT,
      request,
      httpOptions
    );
  };
}
