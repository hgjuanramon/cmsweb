export interface CardHistoryRequest{
    tarjeta: string;
    fechaInicial: string;
    fechaFinal: string;
}