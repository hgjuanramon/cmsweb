import { Response } from 'src/app/core/models/response';
import { CardData } from '../entity/card-data';

export interface CardDataResponse{
    Respuesta: Response;
    DatosTarjeta: CardData;
}