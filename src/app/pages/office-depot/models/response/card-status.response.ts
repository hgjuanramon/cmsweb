import { Response } from 'src/app/core/models/response';
import { CardStatus } from '../entity/card-status';

export interface CardStatusResponse{
    Respuesta: Response;
    StatusTarjetas: CardStatus[];
}