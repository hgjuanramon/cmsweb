import { Response } from 'src/app/core/models/response';
import { CardHistory } from '../entity/card-history';

export interface CardHistoryResponse{
    Respuesta: Response;
    MovimientosOd: CardHistory[];
}