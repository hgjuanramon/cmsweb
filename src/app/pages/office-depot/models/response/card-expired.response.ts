import { Response } from 'src/app/core/models/response';
import { CardExpired } from '../entity/card-expired';

export interface CardExpiredResponse{
    Respuesta: Response;
    DatosTarjetaVencidas: CardExpired[];
}