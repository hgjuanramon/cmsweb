export interface CardHistory {
  Autorizacion: String;
  Tarjeta: string;
  EAN: string;
  Importe: string;
  Servicio: string;
  Estatus: string;
  FechaRegistro: string;
}
