export interface CardData {
    Tarjeta: string;
    IdStatusTarjeta: number;
    EstatusTarjeta: string;
    EAN: string;
    FechaVigencia: string;
    EstatusCuenta: string;
    Saldo: string;
    SaldoInicial: string;
}