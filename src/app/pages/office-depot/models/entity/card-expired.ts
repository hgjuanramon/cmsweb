export interface CardExpired{
    Tarjeta: string;
    EstatusTarjeta: string;
    EAN: string;
    FechaVigencia: string;
    EstatusCuenta: string;
    Saldo: string;
    SaldoInicial: string;
}