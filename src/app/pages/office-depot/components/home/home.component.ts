import {
  Component,
  OnInit,
  ChangeDetectorRef,
  AfterViewInit,
  ViewChild
} from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { OfficeDepotService } from "../../services/office-depot.service";
import { AlertService } from "src/app/core/services/alert.service";
import { CardData } from "../../models/entity/card-data";
import { DatePipe } from "@angular/common";
import { AuthService } from "src/app/pages/auth/auth.service";
import { Title } from "@angular/platform-browser";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
  providers: [DatePipe]
})
export class HomeComponent implements OnInit, AfterViewInit {
  title = "Office Depot";
  record: CardData;
  showSpinner: boolean = false;
  searchForm: FormGroup;
  items: object = [];
  pageSize: number = 15;
  columns: object = [];
  cardStatusForm: FormGroup;
  cardExpiredForm: FormGroup;
  statusList: Object = [];
  @ViewChild("ngSearchForm") ngSearchForm;
  constructor(
    private fb: FormBuilder,
    private officeDepotService: OfficeDepotService,
    private ref: ChangeDetectorRef,
    private alertService: AlertService,
    private datePipe: DatePipe,
    private authService: AuthService,
    private titleHelp: Title
  ) {
    this.getStatusCard();
  }

  ngOnInit() {
    this.buildSearchForm();
    this.buildCardStatusForm();
    this.setDefaultValue();
    this.buildCardExpiredForm();
    this.titleHelp.setTitle(this.title);
  }

  ngAfterViewInit() {
    this.columns = this.getColumnsCardHistory();
    this.ref.detectChanges();
  }

  private buildSearchForm = (): void => {
    this.searchForm = this.fb.group({
      cardNumber: [
        null,
        [
          Validators.required,
          Validators.minLength(13),
          Validators.maxLength(16),
          Validators.pattern("^-?[0-9]\\d*(\\.\\d{1,2})?$")
        ]
      ],
      startDate: [null, [Validators.required]],
      endDate: [null, [Validators.required]]
    });
  };

  private buildCardStatusForm = (): void => {
    this.cardStatusForm = this.fb.group({
      statusId: [null, [Validators.required]]
    });
  };

  private buildCardExpiredForm = (): void => {
    this.cardExpiredForm = this.fb.group({
      newDate: [null, [Validators.required]]
    });
  };

  get cardNumber() {
    return this.searchForm.get("cardNumber") as FormControl;
  }

  get startDate() {
    return this.searchForm.get("startDate") as FormControl;
  }

  get endDate() {
    return this.searchForm.get("endDate") as FormControl;
  }

  get statusId() {
    return this.cardStatusForm.get("statusId") as FormControl;
  }

  get newDate() {
    return this.cardExpiredForm.get("newDate") as FormControl;
  }

  private setDefaultValue = (): void => {
    this.searchForm.patchValue({
      cardNumber: environment.prefixOfficeDepotCard,
      startDate: new Date(new Date().setMonth(new Date().getMonth() - 1)),
      endDate: new Date()
    });
  };

  private setDefaultValueCardStatusForm = (): void => {
    if (this.record) {
      this.cardStatusForm.patchValue({
        statusId: this.record.IdStatusTarjeta.toString()
      });
    }
  };

  private setDefaultValueCardExpiredForm = (): void => {
    if (this.record) {
      this.cardExpiredForm.patchValue({
        newDate: new Date(
          this.datePipe.transform(this.record.FechaVigencia, "yyyy/MM/dd")
        )
      });
    }
  };

  public doSearch = (): void => {
    this.alertService.clear();
    this.showSpinner = true;
    this.officeDepotService.getCardData(this.cardNumber.value).subscribe(
      result => {
        if (result.Respuesta.CodigoRespuesta == 0) {
          this.record = result.DatosTarjeta as CardData;
          this.getCardHistory(result.DatosTarjeta.Tarjeta);
          this.setDefaultValueCardStatusForm();
          this.setDefaultValueCardExpiredForm();
        } else {
          this.showSpinner = false;
          this.alertService.error(result.Respuesta.Mensaje);
        }
      },
      error => {
        this.showSpinner = false;
        console.log(error);
      }
    );
  };

  public updateCardStatus = (): void => {
    this.showSpinner = true;
    this.alertService.clear();
    this.officeDepotService
      .updateCardStatus({
        tarjeta: this.record.Tarjeta,
        NuevoEstatus: this.statusId.value,
        UsuarioActualiza: this.authService.getAuthStatus().UsuarioPos
          .NombreUsuario
      })
      .subscribe(
        result => {
          this.showSpinner = false;
          if (result.CodigoRespuesta == 0) {
            this.alertService.success("Se Actualizó el Estatus Correctamente.");
          } else {
            this.alertService.error(result.Mensaje);
          }
        },
        error => {
          this.showSpinner = false;
          console.log(error);
        }
      );
  };

  public updateCardExpired = (): void => {
    this.showSpinner = true;
    this.alertService.clear();
    this.officeDepotService
      .updateCardExpired({
        tarjeta: this.record.Tarjeta,
        FechaVigencia: this.newDate.value,
        UsuarioActualiza: this.authService.getAuthStatus().UsuarioPos
          .NombreUsuario
      })
      .subscribe(
        result => {
          this.showSpinner = false;
          if (result.CodigoRespuesta == 0) {
            this.alertService.success(
              "Se Actualizó la Fecha de Vigencia Correctamente."
            );
          } else {
            this.alertService.error(result.Mensaje);
          }
        },
        error => {
          this.showSpinner = false;
          console.log(error);
        }
      );
  };

  public resetForm = () => {
    this.alertService.clear();
    this.record = null;
    this.items = [];
    this.ngSearchForm.resetForm();
    this.setDefaultValue();
  };

  private getCardHistory = (cardNumber: string): void => {
    let parameters = {
      tarjeta: cardNumber,
      fechaInicial:
        this.datePipe.transform(new Date(this.startDate.value), "yyyy/MM/dd") +
        " 00:00:00",
      fechaFinal:
        this.datePipe.transform(new Date(this.endDate.value), "yyyy/MM/dd") +
        " 23:59:59"
    };

    this.officeDepotService.getCardHistory(parameters).subscribe(
      result => {
        this.showSpinner = false;
        if (result.Respuesta.CodigoRespuesta == 0) {
          this.items = result.MovimientosOd;
        } else {
          this.items = [];
        }
      },
      error => {
        this.showSpinner = false;
        console.log(error);
      }
    );
  };

  private getStatusCard = (): void => {
    this.officeDepotService.getCardStatus().subscribe(
      result => {
        if (result.Respuesta.CodigoRespuesta == 0) {
          this.statusList = result.StatusTarjetas;
        } else {
          this.statusList = [];
        }
      },
      error => {
        console.log(error);
      }
    );
  };

  private getColumnsCardHistory = (): Object[] => {
    return [
      {
        name: "Autorización",
        prop: "Autorizacion",
        flexGrow: 1
      },
      {
        name: "Tarjeta",
        prop: "Tarjeta",
        flexGrow: 1
      },
      {
        name: "EAN",
        prop: "EAN",
        flexGrow: 1
      },
      {
        name: "Importe",
        prop: "Importe",
        flexGrow: 1
      },
      {
        name: "Servicio",
        prop: "Servicio",
        flexGrow: 1
      },
      {
        name: "Estatus",
        prop: "Estatus",
        flexGrow: 1
      },
      {
        name: "Fecha Registro",
        prop: "FechaRegistro",
        flexGrow: 1
      }
    ];
  };
}
