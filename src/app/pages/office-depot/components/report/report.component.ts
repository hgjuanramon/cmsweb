import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewChild,
  AfterViewInit
} from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { OfficeDepotService } from "../../services/office-depot.service";
import { AlertService } from "src/app/core/services/alert.service";
import { DatePipe } from "@angular/common";
import { Title } from "@angular/platform-browser";
import * as XLSX from "xlsx";

@Component({
  selector: "app-report",
  templateUrl: "./report.component.html",
  styleUrls: ["./report.component.scss"],
  providers: [DatePipe]
})
export class ReportComponent implements OnInit, AfterViewInit {
  title = "Reporte Tarejetas Vencidas";
  showSpinner: boolean = false;
  searchForm: FormGroup;
  items = [];
  pageSize: number = 15;
  columns: object = [];
  @ViewChild("ngSearchForm") ngSearchForm;
  constructor(
    private fb: FormBuilder,
    private officeDepotService: OfficeDepotService,
    private ref: ChangeDetectorRef,
    private alertService: AlertService,
    private datePipe: DatePipe,
    private titleHelp: Title
  ) {}

  ngOnInit() {
    this.buildSearchForm();
  }
  ngAfterViewInit(): void {
    this.titleHelp.setTitle(this.title);
    this.columns = this.getColumns();
    this.ref.detectChanges();
  }

  private buildSearchForm = (): void => {
    this.searchForm = this.fb.group({
      startDate: [null, [Validators.required]],
      endDate: [null, [Validators.required]]
    });
  };

  get startDate() {
    return this.searchForm.get("startDate") as FormControl;
  }

  get endDate() {
    return this.searchForm.get("endDate") as FormControl;
  }

  public doSearch = (): void => {
    this.alertService.clear();
    this.showSpinner = true;
    this.officeDepotService
      .getCardExpired({
        FechaInicio:
          this.datePipe.transform(
            new Date(this.startDate.value),
            "yyyy-MM-dd"
          ) + " 00:00:00",
        FechaFin:
          this.datePipe.transform(new Date(this.endDate.value), "yyyy-MM-dd") +
          " 23:59:59"
      })
      .subscribe(
        result => {
          this.showSpinner = false;
          if (result.Respuesta.CodigoRespuesta == 0) {
            this.items = result.DatosTarjetaVencidas;
          } else {
            this.alertService.error(result.Respuesta.Mensaje);
          }
        },
        error => {
          this.showSpinner = false;
          console.log(error);
        }
      );
  };

  private getColumns = (): Object[] => {
    return [
      {
        name: "Tarjeta",
        prop: "Tarjeta",
        flexGrow: 1
      },
      {
        name: "Estatus Tarjeta",
        prop: "EstatusTarjeta",
        flexGrow: 1
      },
      {
        name: "EAN",
        prop: "EAN",
        flexGrow: 1
      },
      {
        name: "Fecha Vigencia",
        prop: "FechaVigencia",
        flexGrow: 1
      },
      {
        name: "Saldo",
        prop: "Saldo",
        flexGrow: 1
      },
      {
        name: "Saldo Inicial",
        prop: "SaldoInicial",
        flexGrow: 1
      }
    ];
  };

  public resetForm = () => {
    this.alertService.clear();
    this.ngSearchForm.resetForm();
    this.items = [];
  };

  public download = () => {
    if (this.items) {
      this.downloadFile([...this.items], "Reporte Tarjetas Vencidas");
    }
  };

  private downloadFile = (
    items: Object[] = [],
    filename: string = "Reporte"
  ) => {
    event.preventDefault();
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(items);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Hoja 1");

    /* save to file */
    XLSX.writeFile(wb, filename + ".xlsx");
  };
}
