import { environment } from "src/environments/environment";

export class ApiOfficeDepotConstant {
  public static GET_CARD_STATUS_ENDPOINT = `${environment.urlOfficeDepot}/OfficeDepot/ObtieneEstatusTarjetas`;

  public static GET_CARD_DATA_ENDPOINT = `${environment.urlOfficeDepot}/OfficeDepot/ObtieneDatosTarjeta`;

  public static GET_CARD_HISTORY_ENDPOINT = `${environment.urlOfficeDepot}/OfficeDepot/ObtieneMovimientos`;

  public static GET_CARD_EXPIRED_ENDPOINT = `${environment.urlOfficeDepot}/OfficeDepot/ObtieneTarjetasVencidas`;

  public static UPDATE_CARD_STATUS_ENDPOINT = `${environment.urlOfficeDepot}/OfficeDepot/ActualizaEstatusTarjeta`;

  public static UPDATE_CARD_EXPIRED_ENDPOINT = `${environment.urlOfficeDepot}/OfficeDepot/ActualizaVigenciaTarjeta`;
}
