import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { Routes, RouterModule} from '@angular/router';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { ReportComponent } from './components/report/report.component';


const OfficeDepotRoutes : Routes = [
  {
    path:'',
    children:[
      {
        path:"",
        component: HomeComponent        
      },
      {
        path:"expired-card-report",
        component: ReportComponent        
      }
    ],
    canActivate:[AuthGuard]
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(OfficeDepotRoutes)
  ],
  exports: [RouterModule]
})
export class OfficeDepotRoutingModule { }
