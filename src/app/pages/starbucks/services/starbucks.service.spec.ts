import { TestBed } from '@angular/core/testing';

import { StarbucksService } from './starbucks.service';

describe('StarbucksService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StarbucksService = TestBed.get(StarbucksService);
    expect(service).toBeTruthy();
  });
});
