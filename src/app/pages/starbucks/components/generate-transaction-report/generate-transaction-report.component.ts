import { Component, OnInit, ViewChild } from "@angular/core";
import { StarbucksService } from "../../services/starbucks.service";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { DateHelper } from "src/app/core/helpers/date-helper";
import { AlertService } from "src/app/core/services/alert.service";
import { Title } from "@angular/platform-browser";
import { StarbucksMessageConstant } from "../../constants/starbucks-message.constant";

@Component({
  selector: "app-generate-transaction-report",
  templateUrl: "./generate-transaction-report.component.html",
  styleUrls: ["./generate-transaction-report.component.scss"]
})
export class GenerateTransactionReportComponent implements OnInit {
  title = "Generar Reporte de Transaciones";
  searchForm: FormGroup;
  years = [];
  months = [];
  days = [];
  spinnerVisible: boolean = false;
  @ViewChild("frmSearch") myNgForm;
  constructor(
    private fb: FormBuilder,
    private urlService: StarbucksService,
    private dateHelper: DateHelper,
    public alertService: AlertService,
    private titleService: Title
  ) {
    this.years = this.dateHelper.getYears();
    this.months = this.dateHelper.getMonths();
  }

  get year() {
    return this.searchForm.get("year") as FormControl;
  }

  get month() {
    return this.searchForm.get("month") as FormControl;
  }

  get day() {
    return this.searchForm.get("day") as FormControl;
  }

  ngOnInit() {
    this.buildForm();
    this.titleService.setTitle(this.title);
    this.setDefaultSearchForm();
  }

  private buildForm() {
    this.searchForm = this.fb.group({
      year: [null, [Validators.required]],
      month: [null, [Validators.required]],
      day: [null]
    });
  }

  private setDefaultSearchForm = () => {
    this.searchForm.patchValue({ year: new Date().getFullYear() });
  };

  public doGenerateReport = (request: FormGroup): void => {
    this.alertService.clear();
    this.spinnerVisible = true;
    this.urlService.generateTransactionReport(request.value).subscribe(
      response => {
        this.spinnerVisible = false;
        if (response.CodigoRespuesta != 0) {
          this.alertService.info(response.Mensaje);
        } else {
          this.alertService.info(
            StarbucksMessageConstant.MESSAGE_SUCCES_REPORT
          );
        }
      },
      error => {
        this.spinnerVisible = false;
        console.log(error);
      }
    );
  };

  public onChangeMonth = () => {
    if (this.month.value) {
      this.days = this.dateHelper.getDays(this.year.value, this.month.value);
    } else {
      this.days = [];
    }
  };

  public doReset = () => {
    this.alertService.clear();
    this.myNgForm.resetForm();
    this.setDefaultSearchForm();
  };
}
