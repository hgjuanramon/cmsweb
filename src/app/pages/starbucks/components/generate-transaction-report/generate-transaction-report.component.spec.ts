import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateTransactionReportComponent } from './generate-transaction-report.component';

describe('GenerateTransactionReportComponent', () => {
  let component: GenerateTransactionReportComponent;
  let fixture: ComponentFixture<GenerateTransactionReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateTransactionReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateTransactionReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
