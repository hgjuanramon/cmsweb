import { Response } from 'src/app/core/models/response';
import { Transaction } from '../entity/transaction';

export interface TransactionResponse{
    Respuesta: Response;
    Tx: Transaction[]
}