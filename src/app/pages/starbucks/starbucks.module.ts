import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionReportComponent } from './components/transaction-report/transaction-report.component';
import { GenerateTransactionReportComponent } from './components/generate-transaction-report/generate-transaction-report.component';
import { StarbucksRoutingModule } from './starbucks-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModule } from 'src/app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [TransactionReportComponent, GenerateTransactionReportComponent],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    StarbucksRoutingModule    
  ]
})
export class StarbucksModule { }
