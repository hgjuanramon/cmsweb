import { environment } from "src/environments/environment";

export class StarbucksConstant {
  public static GENERATE_TRANSACTION_REPORT = `${environment.urlService}/Starbucks/GeneraReporteAceptadasRechazadas`;

  public static GET_ALL_TRANSACTIONS = `${environment.urlService}/Starbucks/ObtenerTransacciones`;
}
