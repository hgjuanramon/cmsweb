import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from '../auth/auth.service';
import { Title } from '@angular/platform-browser';
import { AlertService } from 'src/app/core/services/alert.service';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent implements OnInit {

  title = "Recuperar Contraseña";
  recoverPasswordForm: FormGroup;
  submitted : boolean = false;
  isVisible : boolean = false;
  @ViewChild("ngRecoverPasswordForm") ngRecoverPasswordForm;
  constructor(private fb: FormBuilder, private authService: AuthService, private titleService: Title, private alertService: AlertService) { }

  ngOnInit() {
    this.authService.logout();
    this.buildRecoverPasswordForm();
    this.titleService.setTitle(this.title);
  }

  private buildRecoverPasswordForm(): void{
    this.recoverPasswordForm = this.fb.group({
      email: ['',[Validators.required, Validators.email]]
    });
  }

  public recoverPassword(submittedForm: FormGroup){
    
    this.alertService.clear();
    this.isVisible = true;
    this.authService.recoverPassword(submittedForm.value.email).subscribe(result => {
        this.isVisible = false;
        if(result.CodigoRespuesta == 0){         
          this.ngRecoverPasswordForm.resetForm();
          this.alertService.success(result.Mensaje);
        }else{
          this.alertService.error(result.Mensaje);
        } 
      }, error => {
        this.isVisible = false;
        console.log(error);
    });
  }
}
