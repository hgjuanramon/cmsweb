import { NgModule } from "@angular/core";
import { MaterialModule } from "../material.module";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "../app-routing.module";
import { SharedModule } from "../shared/shared.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { AuthService } from "./auth/auth.service";
import { AuthGuard } from "../core/guards/auth.guard";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { AlertService } from "../core/services/alert.service";
import { LoginComponent } from "./login/login.component";
import { LogoutComponent } from "./logout/logout.component";
import { RecoverPasswordComponent } from "./recover-password/recover-password.component";
import { ChangePasswordComponent } from "./change-password/change-password.component";

@NgModule({
  declarations: [
    LoginComponent,
    LogoutComponent,
    RecoverPasswordComponent,
    ChangePasswordComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedModule,
    NgxDatatableModule,
  ],
  providers: [AuthService, AuthGuard, AlertService],
})
export class PagesModule {}
