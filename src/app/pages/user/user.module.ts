import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserListComponent } from './components/user-list/user-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserRoutingModule } from './user-routing.module';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { DetailUserComponent } from './components/detail-user/detail-user.component';
import { SearchComponent } from './components/search/search.component';
import { AddUserComponent } from './components/add-user/add-user.component';

@NgModule({  
  imports: [
    CommonModule,
    FormsModule,  
    ReactiveFormsModule,
    UserRoutingModule,
    SharedModule,
    MaterialModule    
  ],
  declarations: [UserListComponent, EditUserComponent, DetailUserComponent, SearchComponent, AddUserComponent],
  entryComponents: [EditUserComponent,DetailUserComponent, AddUserComponent]
})
export class UserModule { }
