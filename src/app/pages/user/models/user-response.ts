import { Response } from "src/app/core/models/response";
import { User } from "./user";

export interface UserResponse {
  Respuesta: Response;
  UsuarioFW: User;
}

export interface UserListResponse {
  Respuesta: Response;
  UsuarioFW: User[];
}

export interface UserUpdateResponse {
  Respuesta: Response;
}
