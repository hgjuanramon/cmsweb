export interface UserSearchRequest{
    AgentNumber: number,
    Email: string,
    ProgramId: number
}