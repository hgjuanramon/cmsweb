export interface User{
    IdUsuario: number,
    Usuario: string,
    Nombre: string,
    Correo: string,
    Estatus: string
}