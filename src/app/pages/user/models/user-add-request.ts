export interface UserAddRequest{
    agentNumber: number;
    username: string;
    firstname: string;
    lastname: string;
    email: string;
}