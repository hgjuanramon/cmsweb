export interface UserRequest{
    id: number,
    name: string,
    email: string,
    status: number
    changeUserId: number
}