import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from './components/user-list/user-list.component';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { SearchComponent } from './components/search/search.component';

const userRoutes : Routes = [
  {
    path:'',
    children:[
      {
       path:"",
       component: UserListComponent 
      },
      {
        path:"edit",
        component: SearchComponent
       }
    ],
    canActivate:[AuthGuard]
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(userRoutes)
  ],
  exports:[RouterModule]
})
export class UserRoutingModule { }
