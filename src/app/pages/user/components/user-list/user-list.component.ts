import {
  Component,
  OnInit,
  ChangeDetectorRef,
  AfterViewChecked,
  ViewChild
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { Title } from "@angular/platform-browser";
import { UserService } from "../../services/user.service";
import { MatDialog } from "@angular/material/dialog";
import { UserSearchRequest } from "../../models/user-search-request";
import { DetailUserComponent } from "../detail-user/detail-user.component";
import { AlertService } from "src/app/core/services/alert.service";

@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.scss"]
})
export class UserListComponent implements OnInit, AfterViewChecked {
  title = "Consulta de Usuarios";
  programList;
  searchForm: FormGroup;
  errorForm = "";
  errorField = "";
  submitted: boolean = false;
  @ViewChild("f") myNgForm;
  constructor(
    private fb: FormBuilder,
    private titleService: Title,
    private userService: UserService,
    public dialog: MatDialog,
    private ref: ChangeDetectorRef,
    public alertService: AlertService
  ) {
    this.getPrograms();
  }

  ngOnInit() {
    this.buildSearchForm();
    this.titleService.setTitle(this.title);
    this.setDefaultSearchForm();
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  private buildSearchForm(): void {
    this.searchForm = this.fb.group({
      programId: ["", [Validators.required]],
      agentNumber: ["", [Validators.pattern("^-?[0-9]\\d*(\\.\\d{1,2})?$")]],
      email: ["", [Validators.email]]
    });
  }

  setDefaultSearchForm() {
    this.searchForm.patchValue({ programId: 73 });
  }

  get agentNumber() {
    return this.searchForm.get("agentNumber") as FormControl;
  }

  get email() {
    return this.searchForm.get("email") as FormControl;
  }

  public setChangeForm(): void {
    if (!this.email.value && !this.agentNumber.value) {
      this.agentNumber.setValidators([Validators.required]);
      this.email.setValidators([Validators.required]);
    }

    if (this.agentNumber.value) {
      this.agentNumber.setValidators([Validators.required]);
      this.email.clearValidators();
    }

    if (this.email.value) {
      this.email.setValidators([Validators.required, Validators.email]);
      this.agentNumber.clearValidators();
    }

    this.agentNumber.updateValueAndValidity();
    this.email.updateValueAndValidity();
  }

  public doSearch(request: FormGroup): void {
    this.submitted = true;
    this.setChangeForm();
    this.alertService.clear();
    if (this.searchForm.valid) {
      const parameters = {
        ProgramId: request.value.programId,
        Email: request.value.email,
        AgentNumber: request.value.agentNumber
      };
      this.userService.getUser(parameters).subscribe(
        result => {
          if (result.Respuesta.CodigoRespuesta == 0) {
            this.editUser(parameters);
          } else {
            this.alertService.success(result.Respuesta.Mensaje);
          }
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  public editUser(record: UserSearchRequest): void {
    const dialogRef = this.dialog.open(DetailUserComponent, {
      disableClose: true,
      width: "600px",
      panelClass: "edit-user-modal-dialog",
      data: {
        ProgramId: record.ProgramId,
        Email: record.Email,
        AgentNumber: record.AgentNumber
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.alertService.success(result.Mensaje);
      }
    });
  }

  public doReset() {
    this.myNgForm.resetForm();
    this.setDefaultSearchForm();
  }

  private getPrograms() {
    this.programList = this.userService.getPrograms();
  }
}
