import { Component, OnInit, Inject } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EditUserComponent } from '../edit-user/edit-user.component';
import { UserService } from '../../services/user.service';
import { UserRequest } from '../../models/user-request';

export interface DialogData{
  AgentNumber: number,
  Email: string,
  ProgramId: number
}

@Component({
  selector: 'app-detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.scss']
})
export class DetailUserComponent implements OnInit {

  statusList;
  editUserForm: FormGroup;
  userRequest: UserRequest;
  userId: number = 0;

  constructor(private userService: UserService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<EditUserComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData) {  
    this.userRetrieved();    
   }

  ngOnInit() {
   this.buildForm(); 
  }

  buildForm(): void{
    this.editUserForm = this.fb.group({
      username : ['',[Validators.required]],
      name : ['',[Validators.required]],
      email: ['', [Validators.required]],
      status: ['', [Validators.required]]
    });    
  }

  userRetrieved(): void{
    this.userService.getUser(this.data).subscribe(result => {
      this.userId = result.UsuarioFW.IdUsuario;
      this.editUserForm.patchValue({        
        username: result.UsuarioFW.Usuario,
        name: result.UsuarioFW.Nombre,
        email: result.UsuarioFW.Correo,
        status: result.UsuarioFW.Estatus
      });  
    }, error => {
      console.log(error);
    });
  }
}
