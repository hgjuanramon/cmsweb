import { Component, OnInit, Inject } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from "../../services/user.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { UserRequest } from "../../models/user-request";
import { AuthService } from "src/app/pages/auth/auth.service";
import { User } from "../../models/user";

export interface DialogData {
  record: User;
}

@Component({
  selector: "app-edit-user",
  templateUrl: "./edit-user.component.html",
  styleUrls: ["./edit-user.component.scss"]
})
export class EditUserComponent implements OnInit {
  errorText = "";
  statusList;
  editUserForm: FormGroup;
  userRequest: UserRequest;
  userId: number = 0;

  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<EditUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private authService: AuthService
  ) {
    this.getStatus();
  }

  ngOnInit() {
    this.buildForm();
    this.userRetrieved(this.data.record);
  }

  private buildForm(): void {
    this.editUserForm = this.fb.group({
      username: ["", [Validators.required]],
      name: ["", [Validators.required]],
      email: ["", [Validators.required]],
      status: ["", [Validators.required]]
    });
  }

  private userRetrieved(data: User): void {
    if (data) {
      this.userId = data.IdUsuario;
      this.editUserForm.patchValue({
        username: data.Usuario,
        name: data.Nombre,
        email: data.Correo,
        status: data.Estatus
      });
    }
  }

  public save(): void {
    this.errorText = "";
    if (this.editUserForm.valid) {
      const parameters = Object.assign(
        {},
        this.userRequest,
        this.editUserForm.value
      );
      parameters.id = this.userId;
      parameters.status = this.editUserForm.value.status == "Activo" ? 1 : 4;
      parameters.changeUserId = 1;
      this.userService.updateUser(parameters).subscribe(
        result => {
          if (result.Respuesta.CodigoRespuesta != 0) {
            this.errorText = result.Respuesta.Mensaje;
          } else {
            this.dialogRef.close({
              status: 0,
              message: "El Registro se Actualizó Correctamente."
            });
          }
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  private getStatus() {
    this.statusList = this.userService.getStatus();
  }
}
