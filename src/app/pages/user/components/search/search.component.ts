import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  TemplateRef,
  AfterViewInit
} from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { Title } from "@angular/platform-browser";
import { UserService } from "../../services/user.service";
import { MatDialog } from "@angular/material/dialog";
import { AlertService } from "src/app/core/services/alert.service";
import { EditUserComponent } from "../edit-user/edit-user.component";
import { AddUserComponent } from "../add-user/add-user.component";

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.scss"]
})
export class SearchComponent implements OnInit, AfterViewInit {
  title = "Consulta de Usuarios";
  programList;
  searchForm: FormGroup;
  submitted: boolean = false;
  items: object[] = [];
  public columns: Object[] = [];
  public pageOfSize: number = 15;
  @ViewChild("actionCellTemplate")
  actionCellTemplate: TemplateRef<any>;
  @ViewChild("f")
  myNgForm;
  showSpinner: boolean = false;
  constructor(
    private fb: FormBuilder,
    private titleService: Title,
    private userService: UserService,
    public dialog: MatDialog,
    private ref: ChangeDetectorRef,
    public alertService: AlertService
  ) {
    this.getPrograms();
  }

  ngOnInit() {
    this.buildSearchForm();
    this.titleService.setTitle(this.title);
    this.setDefaultSearchForm();
  }

  ngAfterViewInit(): void {
    this.getColumns();
    this.ref.detectChanges();
  }

  private buildSearchForm(): void {
    this.searchForm = this.fb.group({
      programId: ["", [Validators.required]],
      agentNumber: ["", [Validators.pattern("^-?[0-9]\\d*(\\.\\d{1,2})?$")]],
      email: ["", [Validators.email]]
    });
  }

  setDefaultSearchForm() {
    this.searchForm.patchValue({ programId: 73 });
  }

  get agentNumber() {
    return this.searchForm.get("agentNumber") as FormControl;
  }

  get email() {
    return this.searchForm.get("email") as FormControl;
  }

  public setChangeForm(): void {
    if (!this.email.value && !this.agentNumber.value) {
      this.agentNumber.setValidators([Validators.required]);
      this.email.setValidators([Validators.required]);
    }

    if (this.agentNumber.value) {
      this.agentNumber.setValidators([Validators.required]);
      this.email.clearValidators();
    }

    if (this.email.value) {
      this.email.setValidators([Validators.required, Validators.email]);
      this.agentNumber.clearValidators();
    }

    this.agentNumber.updateValueAndValidity();
    this.email.updateValueAndValidity();
  }

  public doSearch(request: FormGroup): void {
    this.setChangeForm();
    this.alertService.clear();
    if (this.searchForm.valid) {
      this.getData();
    }
  }
  public getData() {
    this.showSpinner = true;
    const parameters = {
      ProgramId: this.searchForm.value.programId,
      Email: this.searchForm.value.email,
      AgentNumber: this.searchForm.value.agentNumber
    };

    this.userService.getUsers(parameters).subscribe(
      result => {
        this.showSpinner = false;
        if (result.Respuesta.CodigoRespuesta == 0) {
          this.items = result.UsuarioFW;
        } else {
          this.items = [];
          this.alertService.error(result.Respuesta.Mensaje);
        }
      },
      error => {
        this.showSpinner = false;
        console.log(error);
      }
    );
  }

  public addUser(): void {
    this.alertService.clear();
    const dialogRef = this.dialog.open(AddUserComponent, {
      disableClose: true,
      width: "600px",
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.status == 0) {
          this.alertService.success(result.message);
        } else {
          this.alertService.error(result.message);
        }
      }
    });
  }

  public editUser(row): void {
    this.alertService.clear();
    const dialogRef = this.dialog.open(EditUserComponent, {
      disableClose: true,
      width: "600px",
      panelClass: "edit-user-modal-dialog",
      data: { record: row }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.status == 0) {
          this.alertService.success(result.message);
          this.getData();
        } else {
          this.alertService.error(result.message);
        }
      }
    });
  }

  public doReset() {
    this.columns = [];
    this.myNgForm.resetForm();
    this.setDefaultSearchForm();
  }

  private getPrograms() {
    this.programList = this.userService.getPrograms();
  }

  private getColumns = (): void => {
    this.columns = [
      {
        name: "ID",
        prop: "IdUsuario",
        flexGrow: 0.5
      },
      {
        name: "Usuario",
        prop: "Usuario",
        flexGrow: 1
      },
      {
        name: "Nombre",
        prop: "Nombre",
        flexGrow: 1
      },
      {
        name: "Email",
        prop: "Correo",
        flexGrow: 1
      },
      {
        name: "Estatus",
        prop: "Estatus",
        flexGrow: 0.5
      },
      {
        name: "Acciones",
        cellTemplate: this.actionCellTemplate,
        flexGrow: 1
      }
    ];
  };
}
