import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { AlertService } from 'src/app/core/services/alert.service';
import { UserAddRequest } from '../../models/user-add-request';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  errorText = "";
  user : UserAddRequest;
  addUserForm: FormGroup;
  constructor(private fb: FormBuilder, private urlApiService: UserService, private alertService: AlertService, private dialogRef: MatDialogRef<AddUserComponent>) { 

  }

  ngOnInit() {
    this.buildAddForm();
  }

  private buildAddForm(): void{
    this.addUserForm = this.fb.group({
      agentNumber: ["", [Validators.required, Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')]],
      username: ["", [Validators.required]],
      firstname: ["", [Validators.required]],
      lastname: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.email]]
    });
  } 

  public addUser(): void{
    this.errorText = "";
    let parameters = Object.assign({},this.user, this.addUserForm.value);
    this.urlApiService.insertUser(parameters).subscribe(result =>{
      if(result.CodigoRespuesta != 0){
        this.errorText = result.Mensaje;
      }else{
        this.dialogRef.close({status: 0, message: "El Registro se Guardo Correctamente."});
      }
    }, error => {
      console.log(error);
    })
  }
}
