import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { UserSearchRequest } from "../models/user-search-request";
import { Observable } from "rxjs";
import {
  UserResponse,
  UserUpdateResponse,
  UserListResponse
} from "../models/user-response";
import { environment } from "src/environments/environment";
import { Program } from "src/app/core/models/program";
import { UserRequest } from "../models/user-request";
import { Response } from "src/app/core/models/response";
import { UserAddRequest } from "../models/user-add-request";

@Injectable({
  providedIn: "root"
})
export class UserService {
  constructor(private httpClient: HttpClient) {}

  public getUser(request: UserSearchRequest): Observable<UserResponse> {
    return this.httpClient.post<UserResponse>(
      `${environment.urlService}/Fws/ObtieneUsuarioFWS`,
      {
        IdCompania: request.ProgramId,
        Agente: request.AgentNumber,
        correo: request.Email
      }
    );
  }

  public getUsers(request: UserSearchRequest): Observable<UserListResponse> {
    return this.httpClient.post<UserListResponse>(
      `${environment.urlService}/Fws/ObtieneUsuariosFWS`,
      {
        IdCompania: request.ProgramId,
        Agente: request.AgentNumber,
        correo: request.Email
      }
    );
  }

  public updateUser(request: UserRequest): Observable<UserUpdateResponse> {
    return this.httpClient.post<UserUpdateResponse>(
      `${environment.urlService}/Fws/ActualizaUsuario`,
      {
        IdUsuario: request.id,
        Nombre: request.name,
        Correo: request.email,
        UsuarioUpdate: request.changeUserId,
        IdEstatus: request.status
      }
    );
  }

  public insertUser(request: UserAddRequest): Observable<Response> {
    let parameters = {
      agente: request.agentNumber,
      usuario: request.username,
      nombre: request.firstname,
      apellidoPaterno: request.lastname,
      email: request.email
    };

    return this.httpClient.post<Response>(
      `${environment.urlService}/Fws/InsertaUsuario`,
      parameters
    );
  }

  public getPrograms(): Program[] {
    return [{ IdPrograma: 73, Nombre: "Construganas" }];
  }

  public getStatus(): Object[] {
    return [
      { Id: "Activo", Value: "Activo" },
      { Id: "Inactivo", Value: "Inactivo" }
    ];
  }
}
