import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgentListComponent } from './components/agent/agent-list/agent-list.component';
import { EditAgentComponent } from './components/agent/edit-agent/edit-agent.component';
import { ControlDeskRoutingModule } from './control-desk-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { GoalObjectiveComponent } from './components/load-file/goal-objective/goal-objective.component';
import { AddAgentComponent } from './components/agent/add-agent/add-agent.component';
import { AddAgentFullcargaComponent } from './components/agent/add-agent-fullcarga/add-agent-fullcarga.component';
import { BranchListComponent } from './components/branch/branch-list/branch-list.component';
import { EditBranchComponent } from './components/branch/edit-branch/edit-branch.component';
import { GenerateComponent } from './components/card/generate/generate.component';
import { DownloadCardLotComponent } from './components/card/download-card-lot/download-card-lot.component';
import { EditVpfComponent } from './components/agent/edit-vpf/edit-vpf.component';

@NgModule({
  declarations: [AgentListComponent, EditAgentComponent, GoalObjectiveComponent, AddAgentComponent, AddAgentFullcargaComponent, BranchListComponent, EditBranchComponent, GenerateComponent, DownloadCardLotComponent, EditVpfComponent],
  imports: [
    CommonModule,
    ControlDeskRoutingModule,
    FormsModule,  
    ReactiveFormsModule,
    SharedModule,
    MaterialModule,
    NgxDatatableModule
  ],
  entryComponents: [EditAgentComponent,AddAgentComponent, AddAgentFullcargaComponent, EditBranchComponent, EditVpfComponent]
})
export class ControlDeskModule { }
