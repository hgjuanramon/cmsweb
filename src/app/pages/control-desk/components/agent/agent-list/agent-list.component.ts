import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewChild,
  TemplateRef,
  AfterViewInit
} from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { Agent } from "src/app/pages/control-desk/models/entity/agent";
import { AgentService } from "src/app/pages/control-desk/services/agent.service";
import { AlertService } from "src/app/core/services/alert.service";
import { TableViewComponent } from "src/app/shared/table-view/table-view.component";
import { AuthService } from "src/app/pages/auth/auth.service";
import { MatDialog } from "@angular/material/dialog";
import { EditAgentComponent } from "../edit-agent/edit-agent.component";
import { AddAgentComponent } from "../add-agent/add-agent.component";
import { AddAgentFullcargaComponent } from "../add-agent-fullcarga/add-agent-fullcarga.component";
import { Title } from "@angular/platform-browser";
import * as XLSX from "xlsx";
import { EditVpfComponent } from "../edit-vpf/edit-vpf.component";

@Component({
  selector: "app-agent-list",
  templateUrl: "./agent-list.component.html",
  styleUrls: ["./agent-list.component.scss"]
})
export class AgentListComponent implements OnInit, AfterViewInit {
  title = "Lista de Agentes";
  selected = [];
  searchForm: FormGroup;
  items: Agent[] = [];
  public columns: Object[] = [];
  pageSize: number = 15;
  spinnerVisible: boolean = false;
  @ViewChild("tableView") tableView: TableViewComponent<any>;
  @ViewChild("actionCellTemplate")
  private actionCellTemplate: TemplateRef<any>;
  @ViewChild("checkCellTemplate")
  private checkCellTemplate: TemplateRef<any>;
  @ViewChild("headerTemplate")
  private headerTemplate: TemplateRef<any>;
  @ViewChild("ngSearchForm") ngSearchForm;
  constructor(
    private fb: FormBuilder,
    public ref: ChangeDetectorRef,
    private urlService: AgentService,
    public alertService: AlertService,
    private authService: AuthService,
    public dialog: MatDialog,
    private titleRef: Title
  ) {}

  ngOnInit() {
    this.buildForm();
    this.titleRef.setTitle(this.title);
  }

  ngAfterViewInit() {
    this.columns = this.getColumns();
    this.ref.detectChanges();
  }

  buildForm() {
    this.searchForm = this.fb.group({
      agentNumber: [
        "",
        [Validators.required, Validators.pattern("^-?[0-9]\\d*(\\.\\d{1,2})?$")]
      ]
    });
  }

  get agentNumber() {
    return this.searchForm.get("agentNumber") as FormControl;
  }

  public doSearch() {
    this.alertService.clear();
    this.getAllData(this.agentNumber.value);
  }

  public edit = (row): void => {
    this.alertService.clear();
    let dialogRef = this.dialog.open(EditAgentComponent, {
      width: "600px",
      disableClose: true,
      data: { record: row }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.alertService.success(result.message);
        this.getAllData(result.data.cardNumber);
      }
    });
  };

  public onChangeActive = (): void => {
    this.spinnerVisible = true;
    this.alertService.clear();
    if (this.selected.length > 0) {
      const username = this.authService.getAuthStatus().UsuarioPos
        .NombreUsuario;
      for (let i = 0; i < this.selected.length; i++) {
        const record = this.selected[i];
        this.urlService
          .updateAgent({
            id: record.IdUsuario,
            fullname: record.Nombres,
            email: record.Email,
            statusId: 1,
            userUpdate: username
          })
          .subscribe(result => {});
      }
    }

    this.selected = [];
    this.tableView.selected = [];
    this.spinnerVisible = false;
    this.getAllData(this.agentNumber.value);
    this.alertService.success("Se Activarón Correctamente los Registros.");
  };

  public onChangeInActive = (): void => {
    this.spinnerVisible = true;
    this.alertService.clear();
    if (this.selected.length > 0) {
      const username = this.authService.getAuthStatus().UsuarioPos
        .NombreUsuario;
      for (let i = 0; i < this.selected.length; i++) {
        const record = this.selected[i];
        this.urlService
          .updateAgent({
            id: record.IdUsuario,
            fullname: record.Nombres,
            email: record.Email,
            statusId: 2,
            userUpdate: username
          })
          .subscribe(result => {});
      }
    }

    this.selected = [];
    this.tableView.selected = [];
    this.spinnerVisible = false;
    this.getAllData(this.agentNumber.value);
    this.alertService.success("Se Desactivarón Correctamente los Registros.");
  };

  public doReset = (): void => {
    this.selected = [];
    this.alertService.clear();
    this.items = [];
    this.ngSearchForm.resetForm();
  };

  public addAgent = (): void => {
    this.alertService.clear();
    let dialogRef = this.dialog.open(AddAgentComponent, {
      width: "600px",
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.alertService.success(result.message);
        if (this.agentNumber.value) {
          this.getAllData(this.agentNumber.value);
        }
      }
    });
  };

  public addAgentFullcarga = (): void => {
    this.alertService.clear();
    let dialogRef = this.dialog.open(AddAgentFullcargaComponent, {
      width: "600px",
      disableClose: true,
      data: { agentNumber: this.agentNumber.value }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.alertService.success(result.message);
      }
    });
  };

  public selectEvent(selected) {
    this.selected = selected;
  }

  private getAllData = (agentNumber: number): void => {
    this.spinnerVisible = true;
    this.urlService.getAgentList({ agentNumber: agentNumber }).subscribe(
      result => {
        this.spinnerVisible = false;
        if (result.Respuesta.CodigoRespuesta == 0) {
          this.items = [...result.UsuariosPw];
        } else {
          this.items = [];
          this.alertService.error(result.Respuesta.Mensaje);
        }
      },
      error => {
        this.items = [];
        this.spinnerVisible = false;
        console.log(error);
      }
    );
  };

  private getColumns = (): Object[] => {
    return [
      {
        name: "",
        cellTemplate: this.checkCellTemplate,
        headerTemplate: this.headerTemplate
      },
      {
        name: "Comercio",
        prop: "Comercio",
        flexGrow: 0.5
      },
      {
        name: "Usuario",
        prop: "NombreUsuario",
        flexGrow: 1
      },
      {
        name: "Rol",
        prop: "Rol",
        flexGrow: 1
      },
      {
        name: "Nombre",
        prop: "Nombres",
        flexGrow: 1
      },
      {
        name: "Email",
        prop: "Email",
        flexGrow: 1
      },
      {
        name: "Fecha Alta",
        prop: "FechaAlta",
        flexGrow: 1
      },
      {
        name: "Fecha Cambio",
        prop: "FechaCambio",
        flexGrow: 1
      },
      {
        name: "Estatus",
        prop: "Estatus",
        flexGrow: 0.5
      },
      {
        name: "Acciones",
        cellTemplate: this.actionCellTemplate,
        flexGrow: 1.5
      }
    ];
  };

  public downloadUsersPosweb = (): void => {
    this.alertService.clear();
    this.spinnerVisible = true;
    this.urlService.getAllUserPosweb().subscribe(
      result => {
        this.spinnerVisible = false;
        if (result.Respuesta.CodigoRespuesta == 0) {
          this.downloadFile(result.UsuariosPosWeb, "usuario-posweb");
        } else {
          this.alertService.error(result.Respuesta.Mensaje);
        }
      },
      error => {
        this.spinnerVisible = false;
        console.log(error);
      }
    );
  };

  private downloadFile = (
    items: Object[] = [],
    filename: string = "Reporte"
  ): void => {
    event.preventDefault();
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(items);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Hoja 1");

    /* save to file */
    XLSX.writeFile(wb, filename + ".xlsx");
  };

  public variablePF = (row): void => {
    this.alertService.clear();
    let dialogRef = this.dialog.open(EditVpfComponent, {
      width: "600px",
      disableClose: true,
      data: { agentId: row.IdUsuario }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.Status == 0) {
        this.alertService.success(result.Message);
      }
    });
  };
}
