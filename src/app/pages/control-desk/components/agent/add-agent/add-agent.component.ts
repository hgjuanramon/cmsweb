import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AgentService } from 'src/app/pages/control-desk/services/agent.service';
import { MatDialogRef } from '@angular/material/dialog';
import { AlertService } from 'src/app/core/services/alert.service';
import { AuthService } from 'src/app/pages/auth/auth.service';
import { NewAgentModel } from 'src/app/pages/control-desk/models/request/new-agent';

@Component({
  selector: 'app-add-agent',
  templateUrl: './add-agent.component.html',
  styleUrls: ['./add-agent.component.scss']
})

export class AddAgentComponent implements OnInit {

  addAgentForm: FormGroup;
  newAgent: NewAgentModel;
  constructor(private fb: FormBuilder, private ref: ChangeDetectorRef, private urlAgentService: AgentService, private dialogRef: MatDialogRef<AddAgentComponent>, private alertService: AlertService, private authService: AuthService) { }

  ngOnInit() {
    this.buildForm();
  }

  ngAfterViewInit(){
    this.ref.detectChanges();
  }

  buildForm(){
    this.addAgentForm = this.fb.group({
      commerceNumber: [null, [Validators.required, Validators.maxLength(15)]],
      commerceName: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]]
    });
  }

  commerceNumber(){ return this.addAgentForm.get("commerceNumber") as FormControl; }

  email() { return this.addAgentForm.get("email") as FormControl; }

  commerceName() { return this.addAgentForm.get("commerceName") as FormControl; }

  insertAgent(){
    let parameters = Object.assign({}, this.newAgent, this.addAgentForm.value);
    parameters.username = this.authService.getAuthStatus().UsuarioPos.NombreUsuario;
    this.urlAgentService.insertAgent(parameters).subscribe(result => {
      if(result.CodigoRespuesta != 0){
        this.alertService.error(result.Mensaje);
      }else{
        this.dialogRef.close({status: 0, message: "Se Agregó Correctamente el Agente."});
      }
    }, error => {
      console.log(error);
    })
  } 
}
