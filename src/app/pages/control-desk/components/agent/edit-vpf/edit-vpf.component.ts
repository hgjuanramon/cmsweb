import { Component, OnInit, Inject } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { CatalogsService } from "../../../services/catalogs.service";
import { AuthService } from "src/app/pages/auth/auth.service";

export interface DialogData {
  agentId: number;
}

@Component({
  selector: "app-edit-vpf",
  templateUrl: "./edit-vpf.component.html",
  styleUrls: ["./edit-vpf.component.scss"]
})
export class EditVpfComponent implements OnInit {
  listVpf = [];
  vpfForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    public urlService: CatalogsService,
    public dialogRef: MatDialogRef<EditVpfComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public authService: AuthService
  ) {}

  ngOnInit() {
    this.buildForm();
    this.vpfRetrivied();
  }

  private vpfRetrivied(): void {
    this.urlService.getVpfByAgentId(this.data.agentId).subscribe(result => {
      if (result.Respuesta.CodigoRespuesta == 0) {
        this.vpfForm.patchValue({
          corporate: result.UsuarioProfundidad.find(
            x => x.Profundidad == "Corporativo"
          ).Valor,
          region: result.UsuarioProfundidad.find(x => x.Profundidad == "Region")
            .Valor,
          management: result.UsuarioProfundidad.find(
            x => x.Profundidad == "Gerencia"
          ).Valor,
          administrator:
            result.UsuarioProfundidad.find(
              x => x.Profundidad == "EsAdministrador"
            ).Valor == "1"
              ? true
              : false
        });
      }
    });
  }

  private buildForm() {
    this.vpfForm = this.fb.group({
      corporate: [null, [Validators.required]],
      region: [null, [Validators.required]],
      management: [null, [Validators.required]],
      administrator: [null]
    });
  }

  get corporate() {
    return this.vpfForm.get("corporate") as FormControl;
  }

  get region() {
    return this.vpfForm.get("region") as FormControl;
  }

  get management() {
    return this.vpfForm.get("management") as FormControl;
  }

  get administrator() {
    return this.vpfForm.get("administrator") as FormControl;
  }

  public saveData() {
    this.setPvf();
    var success = 1;
    for (var i = 0; i < this.listVpf.length; i++) {
      this.urlService.saveVpf(this.listVpf[i]).subscribe(result => {
        if (result.CodigoRespuesta == 0) {
          success++;
        }
      });
    }
    this.dialogRef.close({
      Status: 0,
      Message: "Se Guardarón Correctamente las Variables de Profundidad."
    });
  }

  private setPvf() {
    this.listVpf.push({
      userId: this.data.agentId,
      vpfId: 1,
      value: this.corporate.value,
      usernameAdd: this.authService.getAuthStatus().UsuarioPos.NombreUsuario
    });
    this.listVpf.push({
      userId: this.data.agentId,
      vpfId: 2,
      value: this.region.value,
      usernameAdd: this.authService.getAuthStatus().UsuarioPos.NombreUsuario
    });
    this.listVpf.push({
      userId: this.data.agentId,
      vpfId: 3,
      value: this.management.value,
      usernameAdd: this.authService.getAuthStatus().UsuarioPos.NombreUsuario
    });
    this.listVpf.push({
      userId: this.data.agentId,
      vpfId: 4,
      value: this.administrator.value == true ? 1 : 0,
      usernameAdd: this.authService.getAuthStatus().UsuarioPos.NombreUsuario
    });
  }
}
