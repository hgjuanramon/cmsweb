import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVpfComponent } from './edit-vpf.component';

describe('EditVpfComponent', () => {
  let component: EditVpfComponent;
  let fixture: ComponentFixture<EditVpfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditVpfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVpfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
