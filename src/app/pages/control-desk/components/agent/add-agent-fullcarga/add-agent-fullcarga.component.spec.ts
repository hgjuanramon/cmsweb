import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAgentFullcargaComponent } from './add-agent-fullcarga.component';

describe('AddAgentFullcargaComponent', () => {
  let component: AddAgentFullcargaComponent;
  let fixture: ComponentFixture<AddAgentFullcargaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAgentFullcargaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAgentFullcargaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
