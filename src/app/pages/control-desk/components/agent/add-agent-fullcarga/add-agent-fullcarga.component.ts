import { Component, OnInit, ChangeDetectorRef, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AgentService } from "src/app/pages/control-desk/services/agent.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { AlertService } from "src/app/core/services/alert.service";
import { AuthService } from "src/app/pages/auth/auth.service";
import { AgentFullcargaRequest } from "src/app/pages/control-desk/models/request/agent-fullcarga";
import { FullcargaPersistence } from "src/app/pages/control-desk/persistence/fullcarga.persistence";
import { FullcargaCommerce } from "src/app/pages/control-desk/models/entity/fullcarga-comerce";
import { environment } from "src/environments/environment";

export interface DataDialog {
  agentNumber: number;
}

@Component({
  selector: "app-add-agent-fullcarga",
  templateUrl: "./add-agent-fullcarga.component.html",
  styleUrls: ["./add-agent-fullcarga.component.scss"]
})
export class AddAgentFullcargaComponent implements OnInit {
  agentFullcarga: AgentFullcargaRequest;
  agentFullcargaForm: FormGroup;
  record: FullcargaCommerce;
  constructor(
    private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private urlAgentService: AgentService,
    private dialogRef: MatDialogRef<AddAgentFullcargaComponent>,
    private alertService: AlertService,
    private authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public data: DataDialog
  ) {
    this.retrivied(data.agentNumber);
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.agentFullcargaForm = this.fb.group({
      commerceNumber: [null, [Validators.required]],
      chainNumber: [null, [Validators.required]],
      terminalNumber: [
        null,
        [Validators.required, Validators.minLength(3), Validators.maxLength(20)]
      ],
      password: [
        null,
        [Validators.required, Validators.minLength(6), Validators.maxLength(10)]
      ],
      PasswordForEncrypt: [
        null,
        [Validators.required, Validators.minLength(6), Validators.maxLength(10)]
      ]
    });
  }

  retrivied(commerce: number) {
    this.urlAgentService.getFullcargaByCommerce(commerce).subscribe(result => {
      if (result.Respuesta.CodigoRespuesta == 0) {
        this.record = result.FullCarga;
        this.agentFullcargaForm.patchValue({
          commerceNumber: commerce,
          chainNumber: result.FullCarga.Cadena,
          terminalNumber: result.FullCarga.Terminal,
          password: result.FullCarga.Password,
          PasswordForEncrypt: result.FullCarga.ClaveEncripcion
        });
      }
    });
  }

  addAgentFullcarga() {
    let paramters = Object.assign(
      {},
      this.agentFullcarga,
      this.agentFullcargaForm.value
    );
    paramters.username = FullcargaPersistence.getUsername(
      paramters.chainNumber
    );

    paramters.version = environment.fullcargaVersion;
    paramters.userAdd = this.authService.getAuthStatus().UsuarioPos.NombreUsuario;

    this.urlAgentService.insertAgentFullcarga(paramters).subscribe(
      result => {
        if (result.CodigoRespuesta != 0) {
          this.alertService.error(result.Mensaje);
        } else {
          this.dialogRef.close({
            status: 0,
            message: "Se Agregó Correctamente el Agente a Fullcarga."
          });
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
