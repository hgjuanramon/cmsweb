import { Component, OnInit, Inject } from "@angular/core";
import { AgentService } from "src/app/pages/control-desk/services/agent.service";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Agent } from "src/app/pages/control-desk/models/entity/agent";
import { AlertService } from "src/app/core/services/alert.service";
import { AuthService } from "src/app/pages/auth/auth.service";

export interface DialogData {
  record: Agent;
}

@Component({
  selector: "app-edit-agent",
  templateUrl: "./edit-agent.component.html",
  styleUrls: ["./edit-agent.component.scss"]
})
export class EditAgentComponent implements OnInit {
  statusList: Object[] = [];
  agentForm: FormGroup;
  constructor(
    private urlService: AgentService,
    private fb: FormBuilder,
    public alertService: AlertService,
    public dialogRef: MatDialogRef<EditAgentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private authService: AuthService
  ) {
    this.statusList = this.urlService.getStatus();
  }

  ngOnInit() {
    this.buildForm();
    this.agentRetrivied(this.data.record);
  }

  buildForm() {
    this.agentForm = this.fb.group({
      fullname: null,
      status: ["", Validators.required],
      email: [null, [Validators.required, Validators.email]]
    });
  }

  get fullname() {
    return this.agentForm.get("fullname") as FormControl;
  }

  get email() {
    return this.agentForm.get("email") as FormControl;
  }

  get status() {
    return this.agentForm.get("status") as FormControl;
  }

  agentRetrivied(record: Agent) {
    if (record) {
      this.agentForm.patchValue({
        fullname: record.Nombres,
        email: record.Email,
        status: record.Estatus == "Activo" ? 1 : 2
      });
    }
  }

  update() {
    this.alertService.clear();
    this.urlService
      .updateAgent({
        id: this.data.record.IdUsuario,
        fullname: this.fullname.value,
        statusId: this.status.value,
        userUpdate: this.authService.getAuthStatus().UsuarioPos.NombreUsuario,
        email: this.email.value
      })
      .subscribe(
        result => {
          if (result.CodigoRespuesta != 0) {
            this.alertService.error(result.Mensaje);
          } else {
            this.dialogRef.close({
              status: 0,
              message: "Se Actualizó Correctamente los Datos.",
              data: { cardNumber: this.data.record.Comercio }
            });
          }
        },
        error => {
          console.log(error);
        }
      );
  }
}
