import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  ChangeDetectorRef,
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import * as XLSX from "xlsx";
import { GoalObjectivePersistence } from "src/app/pages/control-desk/persistence/goal-objective-persistence";
import { LoadFileService } from "src/app/pages/control-desk/services/load-file.service";
import { AlertService } from "src/app/core/services/alert.service";
import { Goal } from "src/app/pages/control-desk/models/entity/goal";
import { ObjectiveRequest } from "src/app/pages/control-desk/models/request/objective-request";
import { Title } from "@angular/platform-browser";

@Component({
  selector: "app-goal-objective",
  templateUrl: "./goal-objective.component.html",
  styleUrls: ["./goal-objective.component.scss"],
})
export class GoalObjectiveComponent implements OnInit, AfterViewInit {
  fileName: string = "";
  file: File = null;
  jsonData: [];
  extFile = ".xls,.xlsx";
  loadForm: FormGroup;
  spinnerVisible = false;
  loading = false;
  @ViewChild("ngLoadForm") ngLoadForm;
  errorList = [];
  title = "Carga de Archivos";
  constructor(
    private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private urlService: LoadFileService,
    private alertService: AlertService,
    private titleTool: Title
  ) {}

  ngOnInit() {
    this.buildForm();
  }

  ngAfterViewInit() {
    this.titleTool.setTitle(this.title);
    this.ref.detectChanges();
  }

  buildForm() {
    this.loadForm = this.fb.group({
      fileName: ["", [Validators.required]],
    });
  }

  public onFileChange(ev): void {
    let workBook = null;
    const reader = new FileReader();
    var file = ev.target.files[0];
    this.fileName = file.name;
    this.loading = true;
    reader.onload = (event) => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: "binary" });
      this.jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet, { header: 1 });
        this.loading = false;
        return initial;
      }, {});
    };
    reader.readAsBinaryString(file);
  }

  public loadFile(): void {
    this.errorList = [];
    this.spinnerVisible = true;
    this.alertService.clear();

    if (this.jsonData["Hoja1"]) {
      if (this.fileName.includes("PTLOY-MTAONLINE")) {
        console.log("Archivo Metas");
        let request = GoalObjectivePersistence.getGoals(this.jsonData["Hoja1"]);
        this.saveGoals(request);
      } else if (this.fileName.includes("PTLOY-MTA-CARGA")) {
        console.log("Archivo Objetivos");
        const records = this.jsonData["Hoja1"];
        let request = GoalObjectivePersistence.getObjectives(records);
        let header = {
          anio: records[0][0],
          mes: records[0][1],
          idProducto: records[0][2],
        };

        this.saveObjectives({
          header: header,
          detail: request,
        } as ObjectiveRequest);
      } else {
        this.spinnerVisible = false;
        this.alertService.error("Archivo Incorrecto");
        this.ngLoadForm.resetForm();
      }

      this.ngLoadForm.resetForm();
    } else {
      this.spinnerVisible = false;
      this.alertService.error(
        "El Archivo No Tiene el Nombre Correcto de la Hoja."
      );
      this.ngLoadForm.resetForm();
    }
  }

  public doReset() {
    this.errorList = [];
    this.jsonData = [];
    this.alertService.clear();
    this.ngLoadForm.resetForm();
  }

  private saveGoals(parameters: Goal[]) {
    this.urlService.saveGoals(parameters).subscribe(
      (result) => {
        this.spinnerVisible = false;
        if (result.CodigoRespuesta == 0) {
          this.alertService.success("Se Cargo Correctamente el Archivo.");
          this.errorList = result.Mensajes;
        } else {
          this.alertService.error(result.Mensajes[0]);
        }

        this.ngLoadForm.resetForm();
      },
      (error) => {
        this.spinnerVisible = false;
        console.log(error);
        this.ngLoadForm.resetForm();
      }
    );
  }

  private saveObjectives(parameters: ObjectiveRequest) {
    this.urlService.saveObjectives(parameters).subscribe(
      (result) => {
        this.spinnerVisible = false;
        if (result.CodigoRespuesta == 0) {
          this.alertService.success("Se Cargo Correctamente el Archivo.");
          this.errorList = result.Mensajes;
        } else {
          this.alertService.error(result.Mensajes[0]);
        }

        this.ngLoadForm.resetForm();
      },
      (error) => {
        this.spinnerVisible = false;
        console.log(error);
        this.ngLoadForm.resetForm();
      }
    );
  }

  public downloadFile(event) {
    event.preventDefault();
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(
      GoalObjectivePersistence.getErrors(this.errorList)
    );
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Hoja1");

    /* save to file */
    XLSX.writeFile(wb, "lista-errores.xlsx");
  }
}
