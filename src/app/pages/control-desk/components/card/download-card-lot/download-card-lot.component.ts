import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CardService } from '../../../services/card.service';
import { LotRequest } from '../../../models/request/lot-request';
import { AlertService } from 'src/app/core/services/alert.service';

@Component({
  selector: 'app-download-card-lot',
  templateUrl: './download-card-lot.component.html',
  styleUrls: ['./download-card-lot.component.scss']
})
export class DownloadCardLotComponent implements OnInit {

  parameters : LotRequest;
  downloadForm: FormGroup
  programList = [];
  typeLotList = [];  
  segmentList = [];
  lotList = [];
  filename = "";
  @ViewChild("ngDownloadForm") ngDownloadForm;
  constructor(private fb: FormBuilder, private urlApiService: CardService, private alertService: AlertService) { 
    this.getPrograma();
    this.getTypeFile();
  }

  ngOnInit() {
    this.buildFileForm();
  }

  private buildFileForm(): void{
    this.downloadForm = this.fb.group({
      programId: [null, [Validators.required]],
      typeLotId: [null, [Validators.required]],
      lotId: [null, [Validators.required]]
    })
  }

  get lotId(){ return this.downloadForm.get('lotId') as FormControl }

  public save(): void{
    this.alertService.clear();
    this.urlApiService.getLotFileFTP(this.filename.replace(".txt","_Confirmacion.txt")).subscribe(result => {
      if(result.Respuesta.CodigoRespuesta == 0){        
        let listFile = result.Archivo.join("\r\n");
        this.saveTextAsFile(listFile, this.filename.replace(".txt","_Confirmacion.txt"));

        this.urlApiService.getLotFileFTP(this.filename.replace(".txt","_Impresion.txt")).subscribe(result => {
          if(result.Respuesta.CodigoRespuesta == 0){
            this.alertService.success("Se Descargo Correctamente el Archivo de Lote.");
            let listFile = result.Archivo.join("\r\n");
            this.saveTextAsFile(listFile, this.filename.replace(".txt","_Impresion.txt"));
          }else{
            this.alertService.error(result.Respuesta.Mensaje);
          }
        });

      }else{
        this.alertService.error(result.Respuesta.Mensaje);
      }
    });
  }

  public changeTypeLot(){
    this.getLots();
  }

  changeLot(event:any){
    this.filename =  event.source.selected.viewValue;   
  }


  public reset(): void{
    this.ngDownloadForm.resetForm();
  }

  private getPrograma(): void{
    this.urlApiService.getPrograms().subscribe(result => {
      if(result.Respuesta.CodigoRespuesta != 0){
        this.programList = [];
      }else{
        this.programList = result.Programas;
      }
    });
  }

  private getTypeFile(): void{
    this.urlApiService.getTypeLot().subscribe(result =>{
      if(result.Respuesta.CodigoRespuesta != 0){
        this.typeLotList = [];
      }else{
        this.typeLotList = result.TipoLotes.filter(c => c.IdTipoLote == 2);
      }
    });    
  }

  private getLots(): void{
    let request = Object.assign({},this.parameters, this.downloadForm.value);
    this.urlApiService.getLots(request).subscribe(result => {
      if(result.Respuesta.CodigoRespuesta != 0){
        this.lotList = [];
      }else{
        this.lotList = result.LotesGenerados;
      }
    });
  }

  public saveTextAsFile (data, filename){

    if(!data) {
      console.error('Console.save: No data')
      return;
    }

    if(!filename) filename = 'File.txt'

    var blob = new Blob([data], {type: 'text/plain'}),
        e    = document.createEvent('MouseEvents'),
        a    = document.createElement('a')
    // FOR IE:

    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob, filename);
    }
    else{
      var e = document.createEvent('MouseEvents'),
          a = document.createElement('a');

      a.download = filename;
      a.href = window.URL.createObjectURL(blob);
      a.dataset.downloadurl = ['text/plain', a.download, a.href].join(':');
      e.initEvent('click', true, false);
      a.dispatchEvent(e);
    }
  }
}
