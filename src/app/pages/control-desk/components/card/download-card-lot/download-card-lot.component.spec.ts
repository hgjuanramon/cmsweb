import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadCardLotComponent } from './download-card-lot.component';

describe('DownloadCardLotComponent', () => {
  let component: DownloadCardLotComponent;
  let fixture: ComponentFixture<DownloadCardLotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadCardLotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadCardLotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
