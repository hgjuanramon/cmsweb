import { Component, OnInit, ViewChild, LOCALE_ID } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CardService } from '../../../services/card.service';
import { environment } from 'src/environments/environment';
import { DatePipe, registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { AlertService } from 'src/app/core/services/alert.service';
import { LotDataRequest } from '../../../models/request/lot-data-request';
import { AuthService } from 'src/app/pages/auth/auth.service';

registerLocaleData(es, 'es');

@Component({
  selector: 'app-generate',
  templateUrl: './generate.component.html',
  styleUrls: ['./generate.component.scss'],
  providers:[
    DatePipe,
    {provide: LOCALE_ID, useValue: "es-ES" }
  ]
})
export class GenerateComponent implements OnInit {

  public lotData: LotDataRequest;
  fileForm: FormGroup
  programList = [];
  typeLotList = [];  
  segmentList = [];
  statusList = [];
  spinnerVisible : boolean = false;
  @ViewChild("ngFileForm") ngFileForm;
  constructor(private fb: FormBuilder, private urlApiService: CardService, public datePipe: DatePipe, private alertService: AlertService, private authService: AuthService) { 
    this.getPrograma();
    this.getSegments();
    this.getTypeFile();
    this.getStatus();
  }

  ngOnInit() {
    this.buildFileForm();
  }

  private buildFileForm(): void{
    this.fileForm = this.fb.group({
      programId: [null, [Validators.required]],
      typeLotId: [null, [Validators.required]],
      nameCard: [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9\\s]+$')]],
      description: [null, [Validators.required]],
      groupId: [null, [Validators.required]],
      amountCard: [null, [Validators.required, Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')]],
      statusCardId: [null, [Validators.required]],
      isTest: [null]
    })
  }

  get programId(){return this.fileForm.get("programId") as FormControl }
  
  get typeLotId(){return this.fileForm.get("typeLotId") as FormControl }

  get nameCard(){return this.fileForm.get("nameCard") as FormControl }

  get description(){return this.fileForm.get("description") as FormControl }

  get groupId(){return this.fileForm.get("groupId") as FormControl }

  get amountCard(){return this.fileForm.get("amountCard") as FormControl }

  get statusCardId(){return this.fileForm.get("statusCardId") as FormControl }

  public generateFile(): void{
    this.alertService.clear();
    this.spinnerVisible = true;

    let filename = this.generateNameFile();
    let listStr = [];
    listStr.push(this.getHeadFile());
    listStr.push(this.getBodyFile(filename));
    listStr.push(this.getFooter());
  
    this.urlApiService.generateLotFile({filename: filename+".txt", listStr: listStr}).subscribe(result => {
      if(result.CodigoRespuesta == 0){
        console.log("Archivo Enviado a FTP Correctamente.");
        this.saveLot(filename);
      }else{
        this.spinnerVisible = false;
        this.alertService.error(result.Mensaje);
      }
    }, error  => {
      this.spinnerVisible = false;
      console.log(error);
    });
  }

  private saveLot(filename : string){
    let parametersLot  = Object.assign({}, this.lotData, this.fileForm.value);
    parametersLot.filename = filename;
    parametersLot.serviceId = 0; 
    parametersLot.username = this.authService.getAuthStatus().UsuarioPos.NombreUsuario;
    this.urlApiService.saveLot(parametersLot).subscribe(result => {
      this.spinnerVisible = false;
      if(result.CodigoRespuesta == 0){
        this.alertService.success("Se Generó Correctamente el Archivo de Lote.");
        this.ngFileForm.resetForm();
      }else{                
        this.alertService.error(result.Mensaje);
      }
    }, error => { 
      this.spinnerVisible = false;
      console.log(error);
    });
  }

  public reset(): void{
    this.ngFileForm.resetForm();
  }

  private getPrograma(): void{
    this.urlApiService.getPrograms().subscribe(result => {
      if(result.Respuesta.CodigoRespuesta != 0){
        this.programList = [];
      }else{
        this.programList = result.Programas;
      }
    });
  }

  private getTypeFile(): void{
    this.urlApiService.getTypeLot().subscribe(result =>{
      if(result.Respuesta.CodigoRespuesta != 0){
        this.typeLotList = [];
      }else{
        this.typeLotList = result.TipoLotes.filter(c => c.IdTipoLote == 2);
      }
    });    
  }

  private getSegments(): void{
    this.urlApiService.getSegments().subscribe(result =>{      
      if(result.Respuesta.CodigoRespuesta != 0){

        this.segmentList = [];
      }else{
        this.segmentList = result.Segmentos;
      }
    });
  }

  private getStatus(): void{
    this.urlApiService.getStatus().subscribe(result => {
      if(result.Respuesta.CodigoRespuesta != 0){
        this.statusList = [];
      }else{
        this.statusList = result.EstatusAlta;
      }
    });
  }

  private generateNameFile(){
    let dateNow = (this.datePipe.transform(new Date(),"yyyyMMMddHHmmss")).replace(".","");
    return environment.prefixFile.alta + this.padLeft(this.programId.value) + dateNow;
  }

  private padLeft(value: string){
    var str = "" + value
    var pad = "000"
    return pad.substring(0, pad.length - str.length) + str
  }

  private getHeadFile(){
    return "H|Grupo|Numero de tarjetas a generar|Nombre a imprimir en la Tarjeta|Nombre del Lote";
  }

  private getBodyFile(filename: string){
    let nameCardValue = (this.nameCard.value.toUpperCase()).replace("Ñ","N");
    let regexInt = /^[0-9]+$/;
    let regexAlfa = /^[a-zA-Z0-9\s]+$/;
    if((!regexInt.test(this.groupId.value) || !regexInt.test(this.amountCard.value) ? true : !regexAlfa.test(nameCardValue))){
      console.log("Los datos del cuerpo (D) no contiene el formatoCorrecto")
      return false;
    }

    let valueForm = Object.assign({}, this.fileForm.value);
    return "D|" + valueForm.groupId + "|" + valueForm.amountCard+ "|" + nameCardValue + "|" + filename;
  }

  private getFooter(){
    return "T|1|" + this.amountCard.value;
  }
}
