import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { BranchService } from 'src/app/pages/control-desk/services/branch.service';
import { Branch } from 'src/app/pages/control-desk/models/entity/branch';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlertService } from 'src/app/core/services/alert.service';
import { AuthService } from 'src/app/pages/auth/auth.service';
import { Management } from 'src/app/pages/control-desk/models/entity/management';

export interface DataDialog  {
  record: Branch
} 

@Component({
  selector: 'app-edit-branch',
  templateUrl: './edit-branch.component.html',
  styleUrls: ['./edit-branch.component.scss']
})
export class EditBranchComponent implements OnInit {

  branchEditForm: FormGroup;
  record : Branch;
  managementList : Management[];
  constructor(private fb: FormBuilder, 
    private urlApiService: BranchService,
    private authService: AuthService,
    private dialogRef : MatDialogRef<EditBranchComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DataDialog,
    private alertService: AlertService) { 
      this.getManagements();
    }

  ngOnInit() {
    this.buildEditForm();
    this.branchRetrivied(this.data.record);
  }

  buildEditForm(){
    this.branchEditForm = this.fb.group({
      agentNumber: [null, [Validators.required]],
      name: [null, [Validators.required]],
      nivelId: [null, [Validators.required]]
    });
  }

  get name(){ return this.branchEditForm.get("name") as FormControl }

  get nivelId() { return this.branchEditForm.get("nivelId") as FormControl }

  branchRetrivied(record : Branch){
    this.branchEditForm.patchValue({
      agentNumber: record.Comercio,
      name: record.Nombre,
      nivelId: record.IdNivelSucursalPrograma
    });
  }

  getManagements(){
    this.urlApiService.getManagements().subscribe(result => {
      if(result.Respuesta.CodigoRespuesta != 0 ){
        this.managementList = []
      }else{
        this.managementList = result.Sucursales;
      }
    });
  }
  update(){
    this.alertService.clear();
    this.urlApiService.editBranch({BranchId: this.data.record.ID_Sucursal, Name: this.name.value, Nivel: this.nivelId.value, Username: this.authService.getAuthStatus().UsuarioPos.NombreUsuario  }).subscribe( result =>{
      if(result.CodigoRespuesta != 0){
        this.alertService.error(result.Mensaje);
      }else{
        this.dialogRef.close({status: 0, message: "Se Actualizó Correctamente los Datos.", data:{ cardNumber: this.data.record.Comercio}});
      }
    },error => {
      console.log(error);
    })
  }
}
