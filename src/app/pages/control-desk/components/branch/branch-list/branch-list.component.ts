import {
  Component,
  OnInit,
  ViewChild,
  TemplateRef,
  AfterViewInit,
  ChangeDetectorRef,
} from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { BranchService } from "src/app/pages/control-desk/services/branch.service";
import { Branch } from "src/app/pages/control-desk/models/entity/branch";
import { AlertService } from "src/app/core/services/alert.service";
import { MatDialog } from "@angular/material/dialog";
import { EditBranchComponent } from "../edit-branch/edit-branch.component";
import { Title } from "@angular/platform-browser";
import { AuthService } from "src/app/pages/auth/auth.service";

@Component({
  selector: "app-branch-list",
  templateUrl: "./branch-list.component.html",
  styleUrls: ["./branch-list.component.scss"],
})
export class BranchListComponent implements OnInit, AfterViewInit {
  title = "Sucursales por Comercio";
  items: Branch[] = [];
  pageSize = 15;
  public columns: Object[] = [];
  searchBranchForm: FormGroup;
  @ViewChild("tableView") tableView: TemplateRef<any>;
  @ViewChild("actionCellTemplate")
  private actionCellTemplate: TemplateRef<any>;
  @ViewChild("ngSearchForm") ngSearchForm;
  spinnerVisible = false;
  constructor(
    private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private urlApiService: BranchService,
    private alertService: AlertService,
    private dialog: MatDialog,
    private titleService: Title,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.buildSeachForm();
    this.titleService.setTitle(this.title);
  }

  ngAfterViewInit(): void {
    this.getColumns();
    this.ref.detectChanges();
  }

  private buildSeachForm() {
    this.searchBranchForm = this.fb.group({
      commerceNumber: [
        null,
        [
          Validators.required,
          ,
          Validators.pattern("^-?[0-9]\\d*(\\.\\d{1,2})?$"),
        ],
      ],
    });
  }

  get commerceNumber() {
    return this.searchBranchForm.get("commerceNumber") as FormControl;
  }

  public doSearch = (): void => {
    this.spinnerVisible = true;
    this.alertService.clear();
    this.getData();
  };

  public editBranch = (row): void => {
    this.alertService.clear();
    let dialogRef = this.dialog.open(EditBranchComponent, {
      width: "600px",
      disableClose: true,
      data: { record: row },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        if (result.status == 0) {
          this.alertService.success(result.message);
          this.getData();
        } else {
          this.alertService.error(result.message);
        }
      }
    });
  };

  public inactiveBranch(row) {
    this.alertService.clear();
    this.urlApiService
      .inactiveBranch({
        Comercio: row.Comercio,
        IdEstatus: 0,
        UsuarioCambio: this.authService.getAuthStatus().UsuarioPos
          .NombreUsuario,
      })
      .subscribe(
        (result) => {
          if (result.CodigoRespuesta == 0) {
            this.alertService.success(result.Mensaje);
            this.getData();
          } else {
            this.alertService.error(result.Mensaje);
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

  public doReset = (): void => {
    this.alertService.clear();
    this.items = [];
    this.ngSearchForm.resetForm();
  };

  private getData = () => {
    this.urlApiService
      .getBranchByCommerce({
        commerce: this.commerceNumber.value,
      })
      .subscribe(
        (result) => {
          if (result.Respuesta.CodigoRespuesta == 0) {
            this.items = result.Sucursales;
          } else {
            this.items = [];
            this.alertService.error(result.Respuesta.Mensaje);
          }
        },
        (error) => {
          console.log(error);
          this.spinnerVisible = false;
        }
      );
  };

  private getColumns() {
    this.columns = [
      {
        name: "Comercio",
        prop: "Comercio",
        flexGrow: 1,
      },
      {
        name: "Nombre",
        prop: "Nombre",
        flexGrow: 1,
      },
      {
        name: "Sucursal",
        prop: "Sucursal",
        flexGrow: 1,
      },
      {
        name: "Nivel",
        prop: "IdNivelSucursalPrograma",
        flexGrow: 0.5,
      },
      {
        name: "Estatus",
        prop: "Estatus",
        flexGrow: 0.5,
        pipe: { transform: this.statusPipe },
      },
      {
        name: "Sincronizado",
        prop: "Sincronizado",
        flexGrow: 0.5,
        pipe: { transform: this.synchronizePipe },
      },
      {
        name: "Acciones",
        cellTemplate: this.actionCellTemplate,
        flexGrow: 1,
      },
    ];
  }

  statusPipe(value: any, ...args: any[]) {
    return value == 1 ? "Activo" : "Inactivo";
  }

  synchronizePipe(value: any, ...args: any[]) {
    return value == 1 ? "Si" : "No";
  }
}
