import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AgentListComponent } from './components/agent/agent-list/agent-list.component';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { GoalObjectiveComponent } from './components/load-file/goal-objective/goal-objective.component';
import { BranchListComponent } from './components/branch/branch-list/branch-list.component';
import { GenerateComponent } from './components/card/generate/generate.component';
import { DownloadCardLotComponent } from './components/card/download-card-lot/download-card-lot.component';

const controlDeskRoutes : Routes = [
  {
    path: "",
    children: [
      {
        path: "list-agent",
        component: AgentListComponent
      },
      {
        path: "add-agent",
        component: AgentListComponent
      },
      {
        path: "load-file",
        component: GoalObjectiveComponent
      },
      {
        path: "list-branch",
        component: BranchListComponent
      },
      {
        path: "lot-of-cards",
        component: GenerateComponent
      },
      {
        path: "download-lot-of-cards",
        component: DownloadCardLotComponent
      }
    ],
    canActivate: [AuthGuard]
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(controlDeskRoutes)
  ],
  exports: [RouterModule]
})
export class ControlDeskRoutingModule { }
