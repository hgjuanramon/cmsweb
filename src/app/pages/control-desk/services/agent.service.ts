import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AgentResponse } from "../models/response/agent-response";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import { SearchRequest } from "../models/request/search-request";
import { Response } from "src/app/core/models/response";
import { AgentRequest } from "../models/request/agent.request";
import { NewAgentModel } from "../models/request/new-agent";
import { AgentFullcargaRequest } from "../models/request/agent-fullcarga";
import { UserPoswebResponse } from "../models/response/user-posweb-response";
import { FullcargaCommerceResponse } from "../models/response/fullcarga-commerce-response";

@Injectable({
  providedIn: "root"
})
export class AgentService {
  constructor(private httpClient: HttpClient) {}

  public getAgentList(request: SearchRequest): Observable<AgentResponse> {
    return this.httpClient.post<AgentResponse>(
      `${environment.urlService}/Operacion/ObtieneUsuariosPorAgente`,
      { Valor: request.agentNumber }
    );
  }

  public updateAgent(request: AgentRequest): Observable<Response> {
    let parameters = {
      IdUsuario: request.id,
      Nombre: request.fullname,
      Correo: request.email,
      Estatus: request.statusId,
      UsuarioCambio: request.userUpdate
    };

    return this.httpClient.post<Response>(
      `${environment.urlService}/Operacion/ActualizaUsuarioPosWeb`,
      parameters
    );
  }

  public insertAgent(request: NewAgentModel): Observable<Response> {
    let parameters = {
      IdComercio: request.commerceNumber,
      Comercio: request.commerceName,
      email: request.email,
      UsuarioAlta: request.username
    };

    return this.httpClient.post<Response>(
      `${environment.urlService}/Operacion/UsuarioInsertaGerente`,
      parameters
    );
  }

  public insertAgentFullcarga(
    request: AgentFullcargaRequest
  ): Observable<Response> {
    let parameters = {
      Cadena: request.chainNumber,
      Comercio: request.commerceNumber,
      Terminal: request.terminalNumber,
      ClaveEncripcion: request.PasswordForEncrypt,
      Usuario: request.username,
      Version: request.version,
      Password: request.password,
      UsuarioAlta: request.username
    };

    return this.httpClient.post<Response>(
      `${environment.urlService}/Operacion/InsertaDatosFullCarga`,
      parameters
    );
  }

  public getStatus(): Object[] {
    return [
      {
        id: 1,
        value: "Activo"
      },
      {
        id: 2,
        value: "Inactivo"
      }
    ];
  }

  public getAllUserPosweb(): Observable<UserPoswebResponse> {
    return this.httpClient.post<UserPoswebResponse>(
      `${environment.urlService}/Operacion/ObtieneUsuariosPosWeb`,
      {}
    );
  }

  public getFullcargaByCommerce(
    commerce: number
  ): Observable<FullcargaCommerceResponse> {
    return this.httpClient.post<FullcargaCommerceResponse>(
      `${environment.urlService}/Operacion/ObtienePosComercio`,
      { Valor: commerce }
    );
  }
}
