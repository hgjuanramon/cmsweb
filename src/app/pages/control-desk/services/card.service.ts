import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProgramaResponse } from '../models/response/programa-response';
import { environment } from 'src/environments/environment';
import { SegmentResponse } from '../models/response/segment-response';
import { TypeLotResponse } from '../models/response/type-lot-response';
import { HighStatusResponse } from '../models/response/High-status-response';
import { LotRequest } from '../models/request/lot-request';
import { LotGenerateResponse } from '../models/response/lot-generate-response';
import { GenerateFileRequest } from '../models/request/generate-file-request';
import { Response } from 'src/app/core/models/response';
import { LotDataRequest } from '../models/request/lot-data-request';
import { FileDownloadResponse } from '../models/response/file-download-response';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  constructor(private httpClient: HttpClient) { }

  public getPrograms(): Observable<ProgramaResponse>{
    return this.httpClient.post<ProgramaResponse>(`${environment.urlService}/AdminLotes/ObtieneProgramas`,{})
  }
  
  public getSegments(): Observable<SegmentResponse>{
    return this.httpClient.post<SegmentResponse>(`${environment.urlService}/AdminLotes/ObtieneSegmentos`,{Valor:4})
  }
  
  public getTypeLot(): Observable<TypeLotResponse>{
    return this.httpClient.post<TypeLotResponse>(`${environment.urlService}/AdminLotes/ObtieneTipoLote`,{})
  }

  public getStatus(): Observable<HighStatusResponse>{
    return this.httpClient.post<HighStatusResponse>(`${environment.urlService}/AdminLotes/ObtieneEstatusAlta`,{});
  }

  public getLots(request: LotRequest): Observable<LotGenerateResponse>{
    let parameters = {
      idTipoLote: request.typeLotId,
      estatusTipoLote: request.statusId,
      emisor: request.programId
    }
    return this.httpClient.post<LotGenerateResponse>(`${environment.urlService}/AdminLotes/ObtieneLotesGenerados`, parameters);
  }

  public generateLotFile(request: GenerateFileRequest): Observable<Response>{
    let parameters = {
      nombreArchivo: request.filename,
      Archivo: request.listStr
    }

    return this.httpClient.post<Response>(`${environment.urlService}/AdminLotes/EnvioLote`, parameters);
  }

  public saveLot(request: LotDataRequest): Observable<Response>{
    let parameters = {
      nombreLote: request.filename,
      idEstatusTarjeta: request.statusCardId,
      idGrupo: request.groupId,
      idServicio: request.serviceId,
      idEmisor: request.programId,
      idTipoLote: request.typeLotId,
      descripcion: request.description,
      idBeneficio: request.benefitId,
      usuarioCambio: request.username,
      esprueba: request.isTest
    }

    return this.httpClient.post<Response>(`${environment.urlService}/AdminLotes/CreaEncabezadoLote`, parameters);
  }

  getLotFileFTP(filename: string): Observable<FileDownloadResponse>{
    return this.httpClient.post<FileDownloadResponse>(`${environment.urlService}/AdminLotes/DescargaLote`,{Cadena: filename});
  }
}
