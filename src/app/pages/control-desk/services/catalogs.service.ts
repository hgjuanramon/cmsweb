import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { VpfRequest } from '../models/request/vpf-request';
import { Response } from 'src/app/core/models/response';
import { UserVpfResponse } from '../models/response/user-vpf.response';

@Injectable({
  providedIn: 'root'
})
export class CatalogsService {

  constructor(private httpClient: HttpClient) { 

  }

  public getVpfByAgentId(agentId: number): Observable<UserVpfResponse>{
    return this.httpClient.post<UserVpfResponse>(`${environment.urlService}/ReportsPosweb/ObtieneConfiguracion`,{ Valor: agentId});
  }

  public saveVpf(request: VpfRequest): Observable<Response>{
    let parameters = {
      idUsuario: request.userId,
      idProfundidad: request.vpfId,
      valor: request.value,
      usuarioAlta: request.usernameAdd      
    }

    return this.httpClient.post<Response>(`${environment.urlService}/ReportsPosweb/AddUsuarioProfudidad`, parameters);
  }
}
