import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { BranchResponse } from "../models/response/branch-response";
import { environment } from "src/environments/environment";
import { BranchSearchRequest } from "../models/request/branch-search-request.model";
import { ManagementResponse } from "../models/response/management-response";
import { Response } from "src/app/core/models/response";
import { BranchRequest } from "../models/request/branch-request";
import { InactiveBranchRequest } from "../models/request/inactive-branch.request";

@Injectable({
  providedIn: "root",
})
export class BranchService {
  constructor(private httpClient: HttpClient) {}

  public getBranchByCommerce(
    request: BranchSearchRequest
  ): Observable<BranchResponse> {
    return this.httpClient.post<BranchResponse>(
      `${environment.urlService}/Operacion/ObtieneSucursales`,
      { Valor: request.commerce }
    );
  }

  public getManagements(): Observable<ManagementResponse> {
    return this.httpClient.post<ManagementResponse>(
      `${environment.urlService}/Operacion/ObtieneSucursalCemex`,
      {}
    );
  }

  public editBranch(request: BranchRequest): Observable<Response> {
    let parameters = {
      IdSucursal: request.BranchId,
      Nombre: request.Name,
      Usuario: request.Username,
      Nivel: request.Nivel,
    };

    return this.httpClient.post<Response>(
      `${environment.urlService}/Operacion/ActualizaSucursal`,
      parameters
    );
  }

  public inactiveBranch(request: InactiveBranchRequest): Observable<Response> {
    return this.httpClient.post<Response>(
      `${environment.urlService}/Operacion/DesactivaComercio`,
      request
    );
  }
}
