import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Goal } from '../models/entity/goal';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ObjectiveRequest } from '../models/request/objective-request';
import { Response } from '../models/response/response';

@Injectable({
  providedIn: 'root'
})
export class LoadFileService {

  constructor(private httpClient: HttpClient) { }

  public saveGoals(request: Goal[]): Observable<Response> {
    return this.httpClient.post<Response>(`${environment.urlService}/Operacion/CargaMetasSucursal`,{ Metas:request });
  }

  public saveObjectives(request: ObjectiveRequest): Observable<Response> {
    debugger;
    return this.httpClient.post<Response>(`${environment.urlService}/Operacion/CargaObjetivosOperador`, request);
  }
}
