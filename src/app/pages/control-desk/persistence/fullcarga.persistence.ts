import { Injectable } from '@angular/core';

@Injectable()
export class FullcargaPersistence {

    static getUsername(chainNumber: number){
        let username = "";
        switch(chainNumber){
            case 8:
                username = "2D2";
                break;
            case 784:
                username = "CEMEX";
                break;
            case 423:
                username = "PROMEXMA";
                break;
            case 794:
                username = "MMGDCEMEX";
                break;    
            default:
                 username = "CEMEX";
                break;
        }

        return username;
    }
}