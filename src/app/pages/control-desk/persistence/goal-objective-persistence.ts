import { Goal } from '../models/entity/goal';
import { Objective } from '../models/entity/objective';
import { Injectable } from '@angular/core';
import { ErrorList } from '../models/entity/error-list';

@Injectable()
export class GoalObjectivePersistence {

    static getGoals(records : Object[]): Goal[] {
        let data = []
        for(let i = 0; i < records.length; i++){
            let record = records[i];            
            if(i == 0)
                continue;

            data.push({
                Anio: record[0],
                Mes: record[1],
                Tipo: record[2],
                Comercio: record[3],
                Meta: record[4],
                MetaPuntos: record[5],
                Tope: record[6],
                TopePuntos: record[7]
              } as Goal);
        }
        
        return data as Goal[];
    }

    static getObjectives(records: Object[]): Objective[] {
        let data = []
        for(let i =0; i < records.length; i++){
            let record = records[i];
            if(i == 0)
                continue
            data.push({idOperador: record[0], cantidad: record[4]} as Objective);
        }

        return data as Objective[];
    }

    static getErrors(records: Object[]): ErrorList[] {
        let data = []
        for(let i =0; i < records.length; i++){
            let record = records[i];
            data.push({Mensaje: record} as ErrorList);
        }

        return data as ErrorList[];
    }    
}