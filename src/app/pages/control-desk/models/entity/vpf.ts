export interface Vpf{
    IdProfundidad: number;
    Nombre: string;
    Descripcion: string;
    Agrupador: string;
    Valor: string;
}