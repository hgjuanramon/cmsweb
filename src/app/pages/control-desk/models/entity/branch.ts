export interface Branch {
  ID_Sucursal: number;
  Comercio: string;
  Nombre: string;
  Sucursal: string;
  IdNivelSucursalPrograma: number;
  Estatus: string;
  Sincronizado: string;
}
