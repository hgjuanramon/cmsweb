export interface Agent {
    IdUsuario: number;
    NombreUsuario: string;
    Comercio: number,
    Rol: string;
    Nombres: string;
    Email: string;
    UsuarioAlta: string;
    FechaAlta: string;
    UsuarioCambio: string;
    FechaCambio: string;
    ID_Sucursal: string;
    Estatus: string;
}