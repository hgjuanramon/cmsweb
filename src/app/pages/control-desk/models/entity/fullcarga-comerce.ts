export interface FullcargaCommerce {
    Terminal: string;
    Usuario: string,
    ClaveEncripcion: string;
    Version: number;
    Password: string;
    Cadena: number;
}