export interface Goal{
    Anio: string;
    Mes: number;
    Tipo: string;
    Comercio: string;
    Meta: string;
    MetaPuntos: string;
    Tope: string;
    TopePuntos: string;
}