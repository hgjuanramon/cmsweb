import { Response } from 'src/app/core/models/response';

export interface FileDownloadResponse{
    Respuesta: Response;
    Archivo: string[]
}