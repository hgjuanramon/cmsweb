import { Response } from 'src/app/core/models/response';

export interface UserPoswebResponse{
    Respuesta: Response;
    UsuariosPosWeb: Object[]
}