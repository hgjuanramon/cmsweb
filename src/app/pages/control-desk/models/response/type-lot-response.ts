import { Response } from 'src/app/core/models/response';
import { TypeLot } from '../entity/type-lot';

export interface TypeLotResponse{
    Respuesta: Response;
    TipoLotes: TypeLot[]
}