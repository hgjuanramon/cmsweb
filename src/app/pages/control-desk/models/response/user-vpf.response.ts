import { Response } from 'src/app/core/models/response';
import { UserVpf } from '../entity/user-vpf';

export interface UserVpfResponse{
    Respuesta: Response;
    UsuarioProfundidad: UserVpf[]
}