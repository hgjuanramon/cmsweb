import { Response } from 'src/app/core/models/response';
import { FullcargaCommerce } from '../entity/fullcarga-comerce';

export interface FullcargaCommerceResponse{
    Respuesta: Response;
    FullCarga: FullcargaCommerce
}