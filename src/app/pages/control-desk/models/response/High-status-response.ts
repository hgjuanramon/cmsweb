import { Response } from 'src/app/core/models/response';
import { HighStatus } from '../entity/high-status';

export interface HighStatusResponse {
    Respuesta: Response;
    EstatusAlta: HighStatus[]
}