import { Response } from 'src/app/core/models/response';
import { Branch } from '../entity/branch';

export interface BranchResponse{
    Respuesta: Response;
    Sucursales: Branch[]; 
}