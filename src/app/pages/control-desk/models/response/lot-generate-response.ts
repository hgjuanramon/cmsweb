import { Response } from 'src/app/core/models/response';
import { LotGenerate } from '../entity/lot-generate';

export interface LotGenerateResponse{
    Respuesta: Response;
    LotesGenerados: LotGenerate[]
}