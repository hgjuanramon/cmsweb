import { Response } from 'src/app/core/models/response';
import { Management } from '../entity/management';

export interface ManagementResponse{
    Respuesta: Response;
    Sucursales: Management[];
}