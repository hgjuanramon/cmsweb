import { Response } from 'src/app/core/models/response';
import { Vpf } from '../entity/vpf';

export interface VpfResponse{
    Respuesta: Response;
    UsuarioProfundidad: Vpf[]
}