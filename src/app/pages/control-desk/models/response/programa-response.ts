import { Program } from '../entity/program';
import { Response } from 'src/app/core/models/response';

export interface ProgramaResponse {
    Respuesta: Response;
    Programas: Program[]
}