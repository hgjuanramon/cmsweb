import { Response } from 'src/app/core/models/response';
import { Segment } from '../entity/segment';

export interface SegmentResponse {
    Respuesta: Response;
    Segmentos: Segment[]
}