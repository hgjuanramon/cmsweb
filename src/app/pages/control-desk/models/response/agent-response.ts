import { Response } from 'src/app/core/models/response';
import { Agent } from '../entity/agent';

export interface AgentResponse {
    Respuesta: Response;
    UsuariosPw: Agent[]
}