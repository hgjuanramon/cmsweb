export interface InactiveBranchRequest {
  Comercio: number;
  IdEstatus: number;
  UsuarioCambio: string;
}
