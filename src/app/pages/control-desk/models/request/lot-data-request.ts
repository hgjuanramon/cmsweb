export interface LotDataRequest{
    filename: string;
    statusCardId: number;
    groupId: number;
    serviceId: number;
    programId: number;
    typeLotId: number;
    description: string;
    benefitId: number;
    isTest: boolean;
    username: string;
}