export interface AgentRequest {
  id: number;
  fullname: string;
  email: string;
  statusId: number;
  userUpdate: string;
}
