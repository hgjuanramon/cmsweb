export interface NewAgentModel {
    commerceNumber: string;
    commerceName: string;
    email: string;
    username: string;
}