import { Header } from '../entity/header';
import { Objective } from '../entity/objective';

export interface ObjectiveRequest{
    header: Header;
    detail: Objective[]
}