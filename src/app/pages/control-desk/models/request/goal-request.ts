export interface GoalRequest {
    Anio: number;
    Mes: number;
    Tipo: string;
    Comercio: string;
    Meta: string;
    MetaPuntos: string;
    Tope: string;
    TopePuntos: string;
}