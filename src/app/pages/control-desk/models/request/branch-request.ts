export interface BranchRequest{
    BranchId: number;
    Name: String;
    Username: string;
    Nivel: number;
}