export interface VpfRequest{
    userId: number;
    vpfId: number;
    value: string;
    usernameAdd: string;
}