export interface AgentFullcargaRequest {
    commerceNumber: number;
    chainNumber: number;
    terminalNumber: string;
    password: string;
    username: string;
    keyEncryption: string;
    PasswordForEncrypt: string;
    version: number;
    userAdd: string;
}