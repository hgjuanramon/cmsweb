export interface LotRequest{
    typeLotId: number;
    statusId: number;
    programId: number;
}