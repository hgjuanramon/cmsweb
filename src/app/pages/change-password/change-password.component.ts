import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "../auth/auth.service";
import { Title } from "@angular/platform-browser";
import { AlertService } from "src/app/core/services/alert.service";
import { ActivatedRoute, Router } from "@angular/router";
import { MustMatch } from "src/app/core/validators/must-match.validator";

@Component({
  selector: "app-change-password",
  templateUrl: "./change-password.component.html",
  styleUrls: ["./change-password.component.scss"]
})
export class ChangePasswordComponent implements OnInit {
  title = "Cambiar Contraseña";
  changePasswordForm: FormGroup;
  submitted: boolean = false;
  isVisible: boolean = false;
  showPanel: boolean = true;
  @ViewChild("ngChangePasswordForm") ngChangePasswordForm;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private alertService: AlertService
  ) {
    this.validateToken();
  }

  ngOnInit() {
    this.authService.logout();
    this.buildChangePasswordForm();
    this.titleService.setTitle(this.title);
  }

  private buildChangePasswordForm = (): void => {
    this.changePasswordForm = this.fb.group(
      {
        password: [
          null,
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(20)
          ]
        ],
        confirmPassword: [
          null,
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(20)
          ]
        ]
      },
      {
        validators: MustMatch("password", "confirmPassword")
      }
    );
  };

  public changePassword = (submittedForm: FormGroup): void => {
    this.alertService.clear();
    this.isVisible = true;
    this.authService.validateToken(this.route.snapshot.params.token).subscribe(
      resultToken => {
        if (resultToken.CodigoRespuesta == 0) {
          this.authService
            .changePassword(
              this.route.snapshot.params.token,
              submittedForm.value.password
            )
            .subscribe(
              result => {
                this.isVisible = false;
                if (result.CodigoRespuesta == 0) {
                  this.ngChangePasswordForm.resetForm();
                  this.showPanel = false;
                } else {
                  this.alertService.error(result.Mensaje);
                }
              },
              error => {
                this.isVisible = false;
                console.log(error);
              }
            );
        } else {
          this.isVisible = false;
          this.alertService.error(resultToken.Mensaje);
        }
      },
      error => {
        this.isVisible = false;
        console.log(error);
      }
    );
  };

  private validateToken() {
    this.authService.validateToken(this.route.snapshot.params.token).subscribe(
      result => {
        if (result.CodigoRespuesta != 0) {
          this.router.navigate(["/login"]);
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
