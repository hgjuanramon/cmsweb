import { Response } from "src/app/core/models/response";
import { User } from "../entity/user";

export interface UserResponse {
  Respuesta: Response;
  UsuariosInterno: User[];
}
