import { Response } from "src/app/core/models/response";
import { Variable } from "../entity/variable";

export interface VariableResponse {
  Respuesta: Response;
  Profundidad: Variable[];
}
