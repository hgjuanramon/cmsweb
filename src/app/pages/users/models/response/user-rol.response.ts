import { Response } from "src/app/core/models/response";
import { UserRol } from "../entity/user-rol";

export interface UserRolResponse {
  Respuesta: Response;
  RolesUsuariosInternos: UserRol[];
}
