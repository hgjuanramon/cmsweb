import { Response } from "src/app/core/models/response";
import { Rol } from "../entity/rol";

export interface RolResponse {
  Respuesta: Response;
  Roles: Rol[];
}
