export interface User {
  IdusuarioInterno: number;
  Nombre: string;
  NombreUsuario: string;
  Email: string;
  EstatusUsuario: string;
}
