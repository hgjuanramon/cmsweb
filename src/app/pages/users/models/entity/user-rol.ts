export interface UserRol {
  IdRol: number;
  Rol: string;
  Descripcion: string;
  TipoRol: string;
}
