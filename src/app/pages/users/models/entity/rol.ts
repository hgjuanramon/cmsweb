export interface Rol {
  IdRol: string;
  Rol: string;
  Nombre?: string;
  Activo?: boolean;
}
