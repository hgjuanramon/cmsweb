export interface UserRequest {
  Nombre: string;
  ApellidoPaterno: string;
  ApellidoMaterno: string;
  Email: string;
  UsuarioAlta: string;
}
