export interface VariableRequest {
  idUsuario: number;
  idProfundidad: number;
  valor: string;
  usuarioAlta: string;
}
