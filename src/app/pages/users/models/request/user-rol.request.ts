export interface UserRolRequest {
  IdUsuarioInterno: number;
  IdRol: number;
  Accion: number;
}
