import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { UserListComponent } from "./components/user-list/user-list.component";
import { AddUserComponent } from "./components/add-user/add-user.component";
import { SharedModule } from "src/app/shared/shared.module";
import { MaterialModule } from "src/app/material.module";
import { UserRoutingModule } from "./user-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { UserRolComponent } from "./components/user-rol/user-rol.component";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { AddVariableComponent } from "./components/add-variable/add-variable.component";

@NgModule({
  declarations: [
    UserListComponent,
    AddUserComponent,
    UserRolComponent,
    AddVariableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MaterialModule,
    UserRoutingModule,
    DragDropModule
  ],
  entryComponents: [AddUserComponent, UserRolComponent, AddVariableComponent]
})
export class UsersModule {}
