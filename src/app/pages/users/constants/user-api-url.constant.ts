import { environment } from "src/environments/environment";

export class UserApiUrlConstant {
  public static GET_USERS_ENDPOINT = `${environment.urlService}/Admin/ObtieneUsuariosInterno`;

  public static ADD_USER_ENDPOINT = `${environment.urlService}/Admin/InsertaUsuarioInterno`;

  public static GET_ROLES_ENDPOINT = `${environment.urlService}/Admin/ObtieneRoles`;

  public static GET_ROLES_CAMPAIGN_ENDPOINT = `${environment.urlService}/Admin/ObtieneRolesCampaign`;

  public static GET_USER_ROLES_ENDPOINT = `${environment.urlService}/Admin/ObtieneRolesUsuario`;

  public static SAVE_USER_ROL = `${environment.urlService}/Admin/ActualizaUsuarioRol`;

  public static SAVE_USER_ROL_CAMPAIGN = `${environment.urlService}/Admin/ActualizaUsuarioRolCampaign`;

  public static GET_VARIABLE_BY_USER = `${environment.urlService}/Admin/ObtieneProfundidadInterno`;

  public static UPDATE_VARIABLE_BY_USER = `${environment.urlService}/Admin/UsuarioInternoProfundidadAdd`;
}
