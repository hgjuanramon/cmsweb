import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  AfterViewInit,
  TemplateRef
} from "@angular/core";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import { UserService } from "../../services/user.service";
import { AlertService } from "src/app/core/services/alert.service";
import { Title } from "@angular/platform-browser";
import { MatDialog } from "@angular/material/dialog";
import { AddUserComponent } from "../add-user/add-user.component";
import { UserRolComponent } from "../user-rol/user-rol.component";
import { AddVariableComponent } from "../add-variable/add-variable.component";

@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.scss"]
})
export class UserListComponent implements OnInit, AfterViewInit {
  title: string = "Usuarios";
  items: object[] = [];
  public columns: Object[] = [];
  public pageOfSize: number = 15;
  showSpinner: boolean = false;
  searchForm: FormGroup;
  @ViewChild("ngSearchForm") ngSearchForm;
  @ViewChild("actionCellTemplate")
  actionCellTemplate: TemplateRef<any>;
  constructor(
    private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private userService: UserService,
    private alertService: AlertService,
    private titleTabPage: Title,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.buildSearchForm();
    this.titleTabPage.setTitle(this.title);
  }

  ngAfterViewInit(): void {
    this.getColumns();
    this.ref.detectChanges();
  }

  private buildSearchForm = (): void => {
    this.searchForm = this.fb.group({
      fulltext: [null]
    });
  };

  get fulltext() {
    return this.searchForm.get("fulltext") as FormControl;
  }

  public doSearch = (): void => {
    this.alertService.clear();
    this.showSpinner = true;
    this.userService.getUsers({ Cadena: this.fulltext.value }).subscribe(
      result => {
        this.showSpinner = false;
        if (result.Respuesta.CodigoRespuesta == 0) {
          this.items = result.UsuariosInterno;
        } else {
          this.alertService.error(result.Respuesta.Mensaje);
        }
      },
      error => {
        this.showSpinner = false;
        console.log(error);
      }
    );
  };

  public addUser = () => {
    let dialogRef = this.dialog.open(AddUserComponent, {
      width: "600px",
      disableClose: true,
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.CodigoRespuesta == 0) {
        this.alertService.success(result.Mensaje);
      }
    });
  };

  public addRol(row) {
    let dialogRef = this.dialog.open(UserRolComponent, {
      width: "800px",
      disableClose: true,
      data: row
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.CodigoRespuesta == 0) {
        this.alertService.success(result.Mensaje);
      }
    });
  }

  public addVariable(row) {
    let dialogRef = this.dialog.open(AddVariableComponent, {
      width: "600px",
      disableClose: true,
      data: row
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.CodigoRespuesta == 0) {
        this.alertService.success(result.Mensaje);
      }
    });
  }

  public doReset = (): void => {
    this.alertService.clear();
    this.items = [];
    this.ngSearchForm.resetForm();
  };

  private getColumns = (): void => {
    this.columns = [
      {
        name: "ID",
        prop: "IdusuarioInterno",
        flexGrow: 1
      },
      {
        name: "Nombre",
        prop: "Nombre",
        flexGrow: 1
      },
      {
        name: "Nombre Usuario",
        prop: "NombreUsuario",
        flexGrow: 1
      },
      {
        name: "Email",
        prop: "Email",
        flexGrow: 1
      },
      {
        name: "Estatus",
        prop: "EstatusUsuario",
        flexGrow: 1
      },
      {
        name: "Acciones",
        cellTemplate: this.actionCellTemplate,
        flexGrow: 1
      }
    ];
  };
}
