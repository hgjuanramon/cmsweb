import { Component, OnInit, Inject } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { AddUserComponent } from "src/app/pages/user/components/add-user/add-user.component";
import { User } from "../../models/entity/user";
import { AuthService } from "src/app/pages/auth/auth.service";
import { CdkDragDrop, transferArrayItem } from "@angular/cdk/drag-drop";
import { UserService } from "../../services/user.service";
import { UserRolRequest } from "../../models/request/user-rol.request";
import { RolItem } from "../../models/entity/rol-item";

@Component({
  selector: "app-user-rol",
  templateUrl: "./user-rol.component.html",
  styleUrls: ["./user-rol.component.scss"]
})
export class UserRolComponent implements OnInit {
  public roles: RolItem[] = [];
  public userRoles: RolItem[] = [];
  public rolesCampaign: RolItem[] = [];
  public userRolesCampaign: RolItem[] = [];
  public userRol: UserRolRequest;
  public titleUser = "";
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private dialogRef: MatDialogRef<AddUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User,
    private authService: AuthService
  ) {
    this.getRolesByUser(1);
    this.getRolesByUser(2);
  }

  ngOnInit() {
    this.getRoles(1);
    this.getRoles(2);
    this.titleUser = this.data.Nombre;
  }

  drop(event: CdkDragDrop<string[]>, option: number = 1, typeRol: number = 1) {
    if (event.previousContainer !== event.container) {
      if (option == 2) {
        let item =
          typeRol == 1
            ? this.roles[event.previousIndex]
            : this.rolesCampaign[event.previousIndex];
        let parameters = Object.assign({}, this.userRol);
        parameters.IdUsuarioInterno = this.data.IdusuarioInterno;
        parameters.IdRol = item.id;
        parameters.Accion = 1;
        this.saveUserRol(parameters, typeRol);
      } else {
        let item =
          typeRol == 1
            ? this.userRoles[event.previousIndex]
            : this.userRolesCampaign[event.previousIndex];
        let parameters = Object.assign({}, this.userRol);
        parameters.IdUsuarioInterno = this.data.IdusuarioInterno;
        parameters.IdRol = item.id;
        parameters.Accion = 2;
        this.saveUserRol(parameters, typeRol);
      }

      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }

  private getRoles = (typeRol: number = 1): void => {
    this.userService.getRoles(typeRol).subscribe(result => {
      if (result.Respuesta.CodigoRespuesta == 0) {
        result.Roles.forEach(element => {
          if (!this.validateRol(+element.IdRol, typeRol)) {
            if (typeRol == 1) {
              this.roles.push({ id: +element.IdRol, name: element.Rol });
            } else {
              this.rolesCampaign.push({
                id: +element.IdRol,
                name: element.Rol
              });
            }
          }
        });
      } else {
        this.roles = [];
      }
    });
  };

  private getRolesByUser = (typeRol: number = 1): void => {
    this.userService
      .getRolesByUser(this.data.IdusuarioInterno)
      .subscribe(result => {
        if (result.Respuesta.CodigoRespuesta == 0) {
          if (result.RolesUsuariosInternos) {
            result.RolesUsuariosInternos.forEach(element => {
              if (typeRol == 1 && element.TipoRol == "LoyaltyHelp") {
                this.userRoles.push({ id: element.IdRol, name: element.Rol });
              } else if (typeRol == 2 && element.TipoRol == "Campaign") {
                this.userRolesCampaign.push({
                  id: element.IdRol,
                  name: element.Rol
                });
              }
            });
          }
        } else {
          this.userRoles = [];
        }
      });
  };

  private saveUserRol = (parameters: UserRolRequest, typeRol: number) => {
    this.userService.saveUserRol(parameters, typeRol).subscribe(result => {
      if (result.CodigoRespuesta == 0) {
        console.log("Guardo correctamente");
      } else {
        console.log("Guardo correctamente");
      }
    });
  };

  private validateRol(rolId: number, typeRol: number = 1) {
    var records = typeRol == 1 ? this.userRoles : this.userRolesCampaign;
    var status = false;
    for (var i = 0; i < records.length; i++) {
      if (records[i].id == rolId) {
        status = true;
        break;
      }
    }

    return status;
  }
}
