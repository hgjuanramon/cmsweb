import { Component, OnInit, Inject } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { AuthService } from "src/app/pages/auth/auth.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { UserService } from "../../services/user.service";
import { VariableRequest } from "../../models/request/variable.request";
import { UserMessagesConstant } from "../../constants/user-messages.constant";
import { User } from "../../models/entity/user";
import { AlertService } from "src/app/core/services/alert.service";

@Component({
  selector: "app-add-variable",
  templateUrl: "./add-variable.component.html",
  styleUrls: ["./add-variable.component.scss"]
})
export class AddVariableComponent implements OnInit {
  variableForm: FormGroup;
  messageError = "";
  record: VariableRequest;
  user;
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private dialogRef: MatDialogRef<AddVariableComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User,
    private authService: AuthService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.buildUserForm();
    this.getVariablesByUser();
  }

  private buildUserForm() {
    this.variableForm = this.fb.group({
      Programa: [null, [Validators.required]]
    });
  }

  get Programa() {
    return this.variableForm.get("Programa") as FormControl;
  }

  public saveVariable = (): void => {
    this.alertService.clear();
    this.messageError = "";
    let parameters = Object.assign({}, this.record);
    parameters.idUsuario = this.data.IdusuarioInterno;
    parameters.idProfundidad = 5;
    parameters.valor = this.Programa.value;
    parameters.usuarioAlta = this.authService.getAuthStatus().UsuarioPos.NombreUsuario;

    this.userService.updateVariableByUser(parameters).subscribe(result => {
      if (result.CodigoRespuesta == 0) {
        result.Mensaje = UserMessagesConstant.MESSAGE_VARIABLE_SUCCESS_ADD;
        this.dialogRef.close(result);
      } else {
        this.messageError = result.Mensaje;
      }
    });
  };

  private getVariablesByUser() {
    this.userService.getVariableByUser(this.data.IdusuarioInterno).subscribe(
      result => {
        if (result.Respuesta) {
          if (result.Profundidad) {
            result.Profundidad.forEach(element => {
              this.variableForm
                .get(element.Profundidad)
                .setValue(element.Valor);
            });
          }
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
