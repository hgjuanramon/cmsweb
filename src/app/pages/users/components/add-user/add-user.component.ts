import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UserService } from "../../services/user.service";
import { MatDialogRef } from "@angular/material/dialog";
import { AuthService } from "src/app/pages/auth/auth.service";
import { UserRequest } from "../../models/request/user.request";
import { UserMessagesConstant } from "../../constants/user-messages.constant";

@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.scss"]
})
export class AddUserComponent implements OnInit {
  userForm: FormGroup;
  user: UserRequest;
  messageError = "";
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private dialogRef: MatDialogRef<AddUserComponent>,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.buildUserForm();
  }

  private buildUserForm = () => {
    this.userForm = this.fb.group({
      Nombre: [null, [Validators.required]],
      ApellidoPaterno: [null, [Validators.required]],
      ApellidoMaterno: [null],
      Email: [null, [Validators.required, Validators.email]]
    });
  };

  public saveUser = (): void => {
    this.messageError = "";
    let parameters = Object.assign({}, this.user, this.userForm.value);
    parameters.UsuarioAlta = this.authService.getAuthStatus().UsuarioPos.NombreUsuario;
    this.userService.saveUser(parameters).subscribe(result => {
      if (result.CodigoRespuesta == 0) {
        result.Mensaje = UserMessagesConstant.SUCCESS_ADD;
        this.dialogRef.close(result);
      } else {
        this.messageError = result.Mensaje;
      }
    });
  };
}
