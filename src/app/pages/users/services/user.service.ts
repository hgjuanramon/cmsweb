import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { SearchUserRequest } from "../models/request/search-user.request";
import { UserResponse } from "../models/response/user.response";
import { UserRequest } from "../models/request/user.request";
import { Response } from "src/app/core/models/response";
import { RolResponse } from "../models/response/rol.response";
import { UserApiUrlConstant } from "../constants/user-api-url.constant";
import { UserRolResponse } from "../models/response/user-rol.response";
import { UserRolRequest } from "../models/request/user-rol.request";
import { VariableResponse } from "../models/response/variable.response";
import { VariableRequest } from "../models/request/variable.request";

@Injectable({
  providedIn: "root"
})
export class UserService {
  constructor(private httpClient: HttpClient) {}

  public getUsers = (request: SearchUserRequest): Observable<UserResponse> => {
    return this.httpClient.post<UserResponse>(
      UserApiUrlConstant.GET_USERS_ENDPOINT,
      request
    );
  };

  public saveUser = (request: UserRequest): Observable<Response> => {
    return this.httpClient.post<Response>(
      UserApiUrlConstant.ADD_USER_ENDPOINT,
      request
    );
  };

  public getRoles = (type: number): Observable<RolResponse> => {
    return this.httpClient.post<RolResponse>(
      type == 1
        ? UserApiUrlConstant.GET_ROLES_ENDPOINT
        : UserApiUrlConstant.GET_ROLES_CAMPAIGN_ENDPOINT,
      {}
    );
  };

  public getRolesByUser = (userId: number): Observable<UserRolResponse> => {
    return this.httpClient.post<UserRolResponse>(
      UserApiUrlConstant.GET_USER_ROLES_ENDPOINT,
      { Valor: userId }
    );
  };

  public saveUserRol = (
    request: UserRolRequest,
    typeRol: number
  ): Observable<Response> => {
    return this.httpClient.post<Response>(
      typeRol == 1
        ? UserApiUrlConstant.SAVE_USER_ROL
        : UserApiUrlConstant.SAVE_USER_ROL_CAMPAIGN,
      request
    );
  };

  public getVariableByUser = (userId: number): Observable<VariableResponse> => {
    return this.httpClient.post<VariableResponse>(
      UserApiUrlConstant.GET_VARIABLE_BY_USER,
      { Valor: userId }
    );
  };

  public updateVariableByUser = (
    request: VariableRequest
  ): Observable<Response> => {
    return this.httpClient.post<Response>(
      UserApiUrlConstant.UPDATE_VARIABLE_BY_USER,
      request
    );
  };
}
