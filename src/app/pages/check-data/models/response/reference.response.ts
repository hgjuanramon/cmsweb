import { Response } from 'src/app/core/models/response';
import { Reference } from '../entity/reference';

export interface ReferenceResponse{
    Respuesta: Response;
    Referencias: Reference[];
}