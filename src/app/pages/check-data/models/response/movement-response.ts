import { Response } from 'src/app/core/models/response';
import { Movement } from '../entity/movement';

export interface MovementResponse{
    Respuesta: Response;
    Movimientos: Movement[];
}