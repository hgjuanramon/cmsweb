import { Response } from 'src/app/core/models/response';
import { Gender } from '../entity/gender';

export interface GenderResponse{
    Respuesta: Response;
    Sexo: Gender[]
}