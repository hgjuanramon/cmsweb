import { Response } from 'src/app/core/models/response';
import { Balance } from '../entity/balance';

export interface BalanceResponse{
    Respuesta: Response;
    Beneficios: Balance[]
}