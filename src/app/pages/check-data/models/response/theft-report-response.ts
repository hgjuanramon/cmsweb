import { Response } from 'src/app/core/models/response';
import { TheftReport } from '../entity/theft-report';

export interface TheftReportResponse{
    Respuesta: Response,
    ReporteRobo: TheftReport
}