import { Response } from 'src/app/core/models/response';
import { ReasonReport } from '../entity/reason-report';

export interface ReasonReportResponse{
    Respuesta: Response;
    MotivosReportes: ReasonReport[]
}