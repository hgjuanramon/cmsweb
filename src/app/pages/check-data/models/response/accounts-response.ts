import { Response } from 'src/app/core/models/response';
import { Personal } from '../entity/personal';

export interface AccountResponse{
    Respuesta: Response;
    CuentaHabiente: Personal;
}

export interface AccountsResponse{
    Respuesta: Response;
    DatosCuentaHabiente: Personal[];
}