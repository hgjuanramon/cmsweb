export interface Movement{
    Tarjeta: string;
    EstatusTarjeta: string;
    Autorizacion: string;
    Comercio: string;
    ConceptoOperacion: string;
    ID_Operacion: string;
    Importe: string;
    Cargo: string;
    Abono: string;
    Beneficio: string;
    Descuento: string;
    FechaRegistro: string;
    CargoAbono: string;
    SaldoFinal: string;
}
