export interface Personal{
    IDP: number;
    Tarjeta: string;
    EstatusTarjeta: string;
    NombreImpreso: string;
    DelegacionMunicipio: string;
    Colonia: string;
    Nombre: string;
    PrimerApellido: string;
    SegundoApellido: string;
    FechaNacimiento: string;
    Correo: string;
    Telefono: string;
    ID_Grupo: number;
    DescripcionGrupo: string;
    RazonSocial: string;
    RFC: string;
    ID_RegimenFiscal: number;
    DescripcionRegimenFiscal: string;
    Sexo: number;
    RecibirSMS: boolean;
    FechaRegistro: String;
}