export interface TheftReport {
    NumeroReporte: string;
    TarjetaNueva: string;
    Resultado: string;
}