export interface Reference{
    IdCuentareferencia: string;
    IdCuenta: string;
    ReferenciaTarjeta: string;
    FechaRegistro: string;
    Activo: number;
    IdEmisor: number;
}