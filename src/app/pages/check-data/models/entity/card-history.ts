export interface CardHistory {
    Tarjeta: string;
    EstatusTarjeta: string;
    FechaReporteBaja: string;
    Motivo: string;
    DescripcionMotivo: string;
    NombreReporta: string;
    UsuarioReporta: string; 
}