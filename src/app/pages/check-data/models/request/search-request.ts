export interface searchRequest{
    typeOption ?: number;
    startDate ?: string;
    endDate?: string;
    cardNumber ?: string;
    idp?: string;
    firstName?: string;
    lastName?: string;
    secondName?: string;
    phoneNumber?: string;
    city?: number;
    maxNumMovs?: number;
}