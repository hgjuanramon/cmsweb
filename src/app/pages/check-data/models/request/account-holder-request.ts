export interface AccountHolderRequest{
    Tarjeta: string;      
    Nombre: string;
    PrimerApellido: string;
    SegundoApellido: string;
    FechaNacimiento: string;
    Email: string;
    Telefono: string;
    RazonSocial: string;
    RFC: string;
    IdRegimenFiscal: number;
    IDSexo: number;
    RecibirSMS: boolean;
    UsuarioCambio?: String;
}