export interface TransferBalanceRequest{
    fullnameReport: string;
    username: string;
    reasonId: number;
    observations: string;
    oldCardNumber: string;
    newCarNumber: string;
}