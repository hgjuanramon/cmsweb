export interface TheftReportRequest{
    idp: number;
    fullnameReport: string;
    username: string;
    reasonId: number;
    observations: string;
}