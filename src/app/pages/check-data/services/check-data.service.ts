import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import {
  AccountResponse,
  AccountsResponse
} from "../models/response/accounts-response";
import { searchRequest } from "../models/request/search-request";
import { BalanceResponse } from "../models/response/balance-response";
import { MovementResponse } from "../models/response/movement-response";
import { CardHistoryResponse } from "../models/response/card-history-response";
import { cardRequest } from "../models/request/card-request";
import { Response } from "src/app/core/models/response";
import { GenderResponse } from "../models/response/gender-response";
import { AccountHolderRequest } from "../models/request/account-holder-request";
import { TransferBalanceRequest } from "../models/request/transfer-balance-request";
import { ReasonReportResponse } from "../models/response/reason-report-response";
import { ReferenceResponse } from "../models/response/reference.response";
import { DeactivateReferenceRequest } from "../models/request/deactivate-reference.request";
import { ApiLoyaltyConstant } from "../constant/api-loyalty.constant";

@Injectable({
  providedIn: "root"
})
export class CheckDataService {
  constructor(private httClient: HttpClient) {}

  public getAccount = (
    parameter: searchRequest
  ): Observable<AccountResponse> => {
    return this.httClient.post<AccountResponse>(
      ApiLoyaltyConstant.GET_ACCOUNT,
      { Cadena: parameter.cardNumber }
    );
  };

  public getCardHistory = (
    parameter: searchRequest
  ): Observable<CardHistoryResponse> => {
    return this.httClient.post<CardHistoryResponse>(
      ApiLoyaltyConstant.GET_CARD_HISTORY,
      { Cadena: parameter.cardNumber }
    );
  };

  public getBalances = (
    parameter: searchRequest
  ): Observable<BalanceResponse> => {
    return this.httClient.post<BalanceResponse>(
      ApiLoyaltyConstant.GET_BALANCES,
      { Cadena: parameter.cardNumber }
    );
  };

  public getMovements = (
    parameter: searchRequest
  ): Observable<MovementResponse> => {
    return this.httClient.post<MovementResponse>(
      ApiLoyaltyConstant.GET_MOVEMENTS,
      {
        Tarjeta: parameter.cardNumber,
        FechaInicial: parameter.startDate,
        FechaFinal: parameter.endDate,
        NumMaxMovs: parameter.maxNumMovs
      }
    );
  };

  public getUsersAcount = (
    parameter: searchRequest
  ): Observable<AccountsResponse> => {
    let request = {
      TipoConsulta: 0,
      Nombre: null,
      ApellidoPaterno: null,
      ApellidoMaterno: null,
      Telefono: "",
      IDP: null,
      Tarjeta: ""
    };

    switch (parameter.typeOption) {
      case 1:
        request.TipoConsulta = 1;
        request.Nombre = parameter.firstName;
        request.ApellidoPaterno = parameter.lastName;
        request.ApellidoMaterno = parameter.secondName;
        request.Telefono = parameter.phoneNumber;
        break;
      case 3:
        request.TipoConsulta = 0;
        request.IDP = parameter.idp;
        request.Tarjeta = null;
        break;
    }

    return this.httClient.post<AccountsResponse>(
      ApiLoyaltyConstant.GET_USERS_ACCOUNT,
      request
    );
  };

  public updateCard = (request: cardRequest): Observable<Response> => {
    return this.httClient.post<Response>(ApiLoyaltyConstant.UPDATE_CARD, {
      Tarjeta: request.cardNumber,
      NuevoEstatus: request.status,
      UsuarioActualiza: request.username
    });
  };

  public getOptionSearch = (): Object[] => {
    return [
      {
        id: 1,
        value: "Datos Personales"
      },
      {
        id: 2,
        value: "Número de Tarjeta"
      },
      {
        id: 3,
        value: "IDP"
      }
    ];
  };

  public getCities = (): Object[] => {
    return [
      {
        id: 1,
        value: "Ciudad de México"
      },
      {
        id: 2,
        value: "Hidalgo"
      },
      {
        id: 3,
        value: "Veracruz"
      },
      {
        id: 4,
        value: "Monterrey"
      }
    ];
  };

  public getStatus = (): Object[] => {
    return [
      {
        id: 0,
        value: "ACTIVA"
      },
      {
        id: 4,
        value: "INACTIVA"
      },
      {
        id: 6,
        value: "CANCELADA"
      }
    ];
  };

  public getGender = (): Observable<GenderResponse> => {
    return this.httClient.post<GenderResponse>(
      ApiLoyaltyConstant.GET_GENDER,
      {}
    );
  };

  public UpdateAccountHolder = (
    request: AccountHolderRequest
  ): Observable<Response> => {
    return this.httClient.post<Response>(
      ApiLoyaltyConstant.UPDATE_ACCOUNT_HOLDER,
      request
    );
  };

  public transferBalance = (
    request: TransferBalanceRequest
  ): Observable<Response> => {
    const parameters = {
      TarjetaActual: request.oldCardNumber,
      TarjetaNueva: request.newCarNumber,
      UsuarioCambio: request.username,
      NombreReporta: request.fullnameReport,
      IdMotivoReporte: request.reasonId,
      IdOrigenTarjetaReposicion: 0,
      Observaciones: request.observations
    };
    return this.httClient.post<Response>(
      ApiLoyaltyConstant.TRANSFER_BALANCE,
      parameters
    );
  };

  public getReasonReport = (): Observable<ReasonReportResponse> => {
    return this.httClient.post<ReasonReportResponse>(
      ApiLoyaltyConstant.GET_REASON_REPORT,
      {}
    );
  };

  public getReferencesByCard = (
    cardNumber: string
  ): Observable<ReferenceResponse> => {
    return this.httClient.post<ReferenceResponse>(
      ApiLoyaltyConstant.GET_REFERENCES_BY_CARD,
      { Cadena: cardNumber }
    );
  };

  public deactiveReference = (
    request: DeactivateReferenceRequest
  ): Observable<Response> => {
    return this.httClient.post<Response>(
      ApiLoyaltyConstant.DEACTIVE_REFERENCE,
      request
    );
  };
}
