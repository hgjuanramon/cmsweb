import { TestBed } from '@angular/core/testing';

import { CheckDataService } from './check-data.service';

describe('CheckDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CheckDataService = TestBed.get(CheckDataService);
    expect(service).toBeTruthy();
  });
});
