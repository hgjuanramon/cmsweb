import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { DataGeneralComponent } from './components/data-general/data-general.component';

const checkDataRoutes : Routes = [
  {
    path:'',
    component: DataGeneralComponent,
    children:[
      {
        path:"check-data",
        component: DataGeneralComponent
      }
    ],
    canActivate:[AuthGuard]
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(checkDataRoutes)
  ],
  exports: [RouterModule]
})
export class CheckDataRoutingModule { }
