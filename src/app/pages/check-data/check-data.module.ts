import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModule } from 'src/app/material.module';
import { DataGeneralComponent } from './components/data-general/data-general.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CheckDataRoutingModule } from './check-data-routing.module';
import { PersonalInfoComponent, DialogReference } from './components/personal-info/personal-info.component';
import { BalanceComponent } from './components/balance/balance.component';
import { DetailSearchComponent } from './components/detail-search/detail-search.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CardHistoryComponent } from './components/card-history/card-history.component';
import { EditCardComponent, DialogOverviewDialog } from './components/edit-card/edit-card.component';
import { EditAccountHolderComponent } from './components/edit-account-holder/edit-account-holder.component';
import { ReportCardComponent } from './components/report-card/report-card.component';
import { MovementComponent } from './components/movement/movement.component';

@NgModule({  
  imports: [
    CommonModule,
    FormsModule,  
    ReactiveFormsModule,
    CheckDataRoutingModule,
    SharedModule,
    MaterialModule,
    NgxDatatableModule
  ],
  declarations: [DataGeneralComponent, PersonalInfoComponent, BalanceComponent,MovementComponent, DetailSearchComponent, CardHistoryComponent, EditCardComponent, EditAccountHolderComponent, ReportCardComponent, DialogOverviewDialog, DialogReference],
  entryComponents:[DetailSearchComponent, EditAccountHolderComponent, DialogOverviewDialog, DialogReference]
})
export class CheckDataModule { }
