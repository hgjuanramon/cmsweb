import { environment } from "src/environments/environment";

export class ApiLoyaltyConstant {
  public static GET_ACCOUNT = `${environment.urlService}/CallCenter/ObtieneDatosCuentahabiente`;

  public static GET_CARD_HISTORY = `${environment.urlService}/CallCenter/ObtieneHistorico`;

  public static GET_BALANCES = `${environment.urlService}/CallCenter/ObtieneSaldos`;

  public static GET_MOVEMENTS = `${environment.urlService}/CallCenter/ObtieneMovimientos`;

  public static GET_USERS_ACCOUNT = `${environment.urlService}/CallCenter/BusquedaDatosCuentaHabiente`;

  public static UPDATE_CARD = `${environment.urlService}/CallCenter/ActualizaEstatusTarjeta`;

  public static GET_GENDER = `${environment.urlService}/CallCenter/ObtieneCatalogoSexo`;

  public static UPDATE_ACCOUNT_HOLDER = `${environment.urlService}/CallCenter/ActualizaDatos`;

  public static TRANSFER_BALANCE = `${environment.urlService}/CallCenter/TraspasarSaldo`;

  public static GET_REASON_REPORT = `${environment.urlService}/CallCenter/ObtieneMotivosRobo`;

  public static GET_REFERENCES_BY_CARD = `${environment.urlService}/Operacion/ObtieneReferenciaTarjeta`;

  public static DEACTIVE_REFERENCE = `${environment.urlService}/Operacion/DesactivaCuentasReferencia`;
}
