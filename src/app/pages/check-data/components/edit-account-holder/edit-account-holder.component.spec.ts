import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAccountHolderComponent } from './edit-account-holder.component';

describe('EditAccountHolderComponent', () => {
  let component: EditAccountHolderComponent;
  let fixture: ComponentFixture<EditAccountHolderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAccountHolderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAccountHolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
