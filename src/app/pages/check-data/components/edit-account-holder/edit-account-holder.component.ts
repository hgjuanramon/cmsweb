import { Component, OnInit, ChangeDetectorRef, Inject } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { CheckDataService } from "../../services/check-data.service";
import { AlertService } from "src/app/core/services/alert.service";
import { Personal } from "../../models/entity/personal";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { AuthService } from "src/app/pages/auth/auth.service";
import { DatePipe } from "@angular/common";
import { MessagesLoyaltyConstant } from "../../constant/messages-loyalty.constant";

export interface DialogData {
  cardNumber: string;
}

@Component({
  selector: "app-edit-account-holder",
  templateUrl: "./edit-account-holder.component.html",
  styleUrls: ["./edit-account-holder.component.scss"],
  providers: [DatePipe]
})
export class EditAccountHolderComponent implements OnInit {
  editAccountForm: FormGroup;
  genderList: Object[] = [];
  record: Personal;
  constructor(
    private ref: ChangeDetectorRef,
    private fb: FormBuilder,
    private urlService: CheckDataService,
    public alertService: AlertService,
    public dialogRef: MatDialogRef<EditAccountHolderComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public authService: AuthService,
    private datePipe: DatePipe
  ) {
    this.getGender();
    this.acountHolderRetrivied(data.cardNumber);
  }

  ngOnInit() {
    this.buildForm();
  }

  ngAfterViewInit() {
    this.ref.detectChanges();
  }

  private buildForm = (): void => {
    this.editAccountForm = this.fb.group({
      cardNumber: [
        "",
        [
          Validators.required,
          Validators.minLength(16),
          Validators.maxLength(16)
        ]
      ],
      firstName: ["", [Validators.required]],
      lastName: ["", [Validators.required]],
      secondName: [null],
      email: [null],
      birthday: ["", [Validators.required]],
      phoneNumber: ["", [Validators.required]],
      genderId: ["", [Validators.required]],
      businessName: [null],
      rfc: [null],
      fiscalId: [null],
      receiveSMS: ["", [Validators.required]],
      username: [null]
    });
  };

  get firstName() {
    return this.editAccountForm.get("firstName") as FormControl;
  }

  get lastName() {
    return this.editAccountForm.get("lastName") as FormControl;
  }

  get secondName() {
    return this.editAccountForm.get("secondName") as FormControl;
  }

  get email() {
    return this.editAccountForm.get("email") as FormControl;
  }

  get phoneNumber() {
    return this.editAccountForm.get("phoneNumber") as FormControl;
  }

  get birthday() {
    return this.editAccountForm.get("birthday") as FormControl;
  }

  get genderId() {
    return this.editAccountForm.get("genderId") as FormControl;
  }

  get businessName() {
    return this.editAccountForm.get("businessName") as FormControl;
  }

  get rfc() {
    return this.editAccountForm.get("rfc") as FormControl;
  }

  get fiscalId() {
    return this.editAccountForm.get("fiscalId") as FormControl;
  }

  get receiveSMS() {
    return this.editAccountForm.get("receiveSMS") as FormControl;
  }

  private acountHolderRetrivied = (cardNumber: string): void => {
    this.urlService.getAccount({ cardNumber: cardNumber }).subscribe(
      result => {
        if (result.Respuesta.CodigoRespuesta == 0) {
          this.record = result.CuentaHabiente;
          this.editAccountForm.patchValue({
            cardNumber: result.CuentaHabiente.Tarjeta,
            firstName: result.CuentaHabiente.Nombre,
            lastName: result.CuentaHabiente.PrimerApellido,
            secondName: result.CuentaHabiente.SegundoApellido,
            email: result.CuentaHabiente.Correo,
            birthday: new Date(result.CuentaHabiente.FechaNacimiento),
            phoneNumber: result.CuentaHabiente.Telefono,
            genderId: result.CuentaHabiente.Sexo,
            businessName: result.CuentaHabiente.RazonSocial,
            rfc: result.CuentaHabiente.RFC,
            fiscalId: result.CuentaHabiente.ID_RegimenFiscal,
            receiveSMS: result.CuentaHabiente.RecibirSMS
          });
        }
      },
      error => {
        console.log(error);
      }
    );
  };

  private getGender = (): void => {
    this.urlService.getGender().subscribe(response => {
      if (response.Respuesta.CodigoRespuesta == 0) {
        this.genderList = response.Sexo;
      } else {
        this.genderList = [];
      }
    });
  };

  public saveAccount = (): void => {
    const request = {
      Tarjeta: this.data.cardNumber,
      Nombre: this.firstName.value,
      PrimerApellido: this.lastName.value,
      SegundoApellido: this.secondName.value,
      FechaNacimiento: this.datePipe.transform(
        new Date(this.birthday.value),
        "yyyy/MM/dd"
      ),
      Email: this.email.value,
      Telefono: this.phoneNumber.value,
      RazonSocial: this.businessName.value,
      RFC: this.rfc.value,
      IdRegimenFiscal: this.fiscalId.value,
      IDSexo: this.genderId.value,
      RecibirSMS: this.receiveSMS.value,
      UsuarioCambio: this.authService.getAuthStatus().UsuarioPos.NombreUsuario
    };

    this.urlService.UpdateAccountHolder(request).subscribe(
      response => {
        if (response.CodigoRespuesta == 0) {
          response.Mensaje =
            MessagesLoyaltyConstant.MESSAGE_UPDATE_ACCOUNT_HOLDER_SUCCESS;
        }

        this.dialogRef.close(response);
      },
      error => {
        console.log(error);
      }
    );
  };
}
