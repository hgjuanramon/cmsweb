import {
  Component,
  OnInit,
  Input,
  AfterViewInit,
  ChangeDetectorRef,
  ViewChild,
  TemplateRef,
  Inject
} from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Personal } from "../../models/entity/personal";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { EditAccountHolderComponent } from "../edit-account-holder/edit-account-holder.component";
import { AlertService } from "src/app/core/services/alert.service";
import { DatePipe } from "@angular/common";
import { CheckDataService } from "../../services/check-data.service";

export interface DialogData {
  status: number;
  title: string;
  message: string;
}
@Component({
  selector: "app-personal-info",
  templateUrl: "./personal-info.component.html",
  styleUrls: ["./personal-info.component.scss"],
  providers: [DatePipe]
})
export class PersonalInfoComponent implements OnInit, AfterViewInit {
  @Input() record: Personal;
  dataForm: FormGroup;
  items = [];
  pageSize: number = 20;
  public columns: Object[] = [];
  spinnerVisible = false;
  @ViewChild("actionCellTemplate")
  private actionCellTemplate: TemplateRef<any>;
  constructor(
    private fb: FormBuilder,
    private checkDataService: CheckDataService,
    public dialog: MatDialog,
    private ref: ChangeDetectorRef,
    public alertService: AlertService,
    public datePipe: DatePipe
  ) {}

  ngAfterViewInit(): void {
    this.columns = this.getColumns();
    this.ref.detectChanges();
    this.formRetrieved();
    this.getReferences();
  }

  ngOnInit() {
    this.buildForm();
  }

  private formRetrieved = (): void => {
    if (this.record != null) {
      this.dataForm.patchValue({
        cardNumber: this.record.Tarjeta,
        firstName: this.record.Nombre,
        lastName: this.record.PrimerApellido,
        secondName: this.record.SegundoApellido,
        statusCard: this.record.EstatusTarjeta,
        birthdate: this.datePipe.transform(
          new Date(this.record.FechaNacimiento),
          "dd/MM/yyyy"
        ),
        email: this.record.Correo,
        phoneNumber: this.record.Telefono,
        segmentName: this.record.DescripcionGrupo,
        dateAdd: this.record.FechaRegistro
      });
    }
  };

  private buildForm() {
    this.dataForm = this.fb.group({
      cardNumber: ["", [Validators.required]],
      firstName: ["", [Validators.required]],
      lastName: ["", [Validators.required]],
      secondName: ["", []],
      statusCard: ["", [Validators.required]],
      birthdate: ["", [Validators.required]],
      email: ["", [Validators.required]],
      phoneNumber: ["", [Validators.required]],
      segmentName: ["", [Validators.required]],
      dateAdd: null
    });
  }

  public onShowDialog = (): void => {
    this.alertService.clear();
    const dialogRef = this.dialog.open(EditAccountHolderComponent, {
      width: "600px",
      disableClose: true,
      data: { cardNumber: this.record.Tarjeta }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.CodigoRespuesta == 0) {
        this.alertService.success(result.Mensaje);
      } else {
        this.alertService.error(result.Mensaje);
      }
    });
  };

  public deactivateReference = (row): void => {
    this.spinnerVisible = true;
    this.checkDataService
      .deactiveReference({
        Tarjeta: this.record.Tarjeta,
        Referencia: row.ReferenciaTarjeta
      })
      .subscribe(
        result => {
          this.spinnerVisible = false;
          if (result.CodigoRespuesta == 0) {
            this.getReferences();
            this.onModalDialog(
              0,
              "Mensaje Correcto",
              "Se Desactivó la Referencia Correctamente."
            );
          } else {
            this.onModalDialog(
              result.CodigoRespuesta,
              "Mensaje Error",
              result.Mensaje
            );
          }
        },
        error => {
          console.log(error);
        }
      );
  };

  private onModalDialog(status: number, title: string, message: string): void {
    const dialogRef = this.dialog.open(DialogReference, {
      disableClose: true,
      width: "450px",
      data: { status: status, title: title, message: message }
    });

    dialogRef.afterClosed().subscribe(result => {});
  }

  private getColumns(): object[] {
    return [
      {
        name: "Referencia Tarjeta",
        prop: "ReferenciaTarjeta",
        flexGrow: 1
      },
      {
        name: "Fecha Registro",
        prop: "FechaRegistro",
        flexGrow: 1
      },
      {
        name: "Acciones",
        cellTemplate: this.actionCellTemplate,
        flexGrow: 1
      }
    ];
  }

  private getReferences() {
    this.checkDataService
      .getReferencesByCard(this.record.Tarjeta)
      .subscribe(result => {
        if (result.Respuesta.CodigoRespuesta == 0) {
          this.items = result.Referencias;
        } else {
          this.items = [];
        }
      });
  }
}

@Component({
  selector: "dialog-reference",
  templateUrl: "dialog-reference.html"
})
export class DialogReference {
  status: number;
  title: string = "";
  message: string = "";
  constructor(
    public dialogRef: MatDialogRef<DialogReference>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.status = data.status;
    this.title = data.title;
    this.message = data.message;
  }

  onClick(): void {
    this.dialogRef.close({
      status: this.status
    });
  }
}
