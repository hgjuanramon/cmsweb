import { Component, OnInit, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { CheckDataService } from '../../services/check-data.service';

@Component({
  selector: 'app-card-history',
  templateUrl: './card-history.component.html',
  styleUrls: ['./card-history.component.scss']
})
export class CardHistoryComponent implements OnInit, AfterViewInit {

  @Input() cardNumber: string = "";
  items = [];
  pageSize: number = 20;
  public columns : Object[] = [];
  constructor(private apiUrl: CheckDataService, private ref: ChangeDetectorRef) { }

  ngOnInit() {
    this.getData();
  }

  ngAfterViewInit(): void{
    this.columns = this.getColumns();
    this.ref.detectChanges();
  }

  private getData(){
    
    this.apiUrl.getCardHistory({cardNumber: this.cardNumber}).subscribe(result => {      
      if(result.Respuesta.CodigoRespuesta != 0){
        this.items = [];
      }else{
        this.items = result.Historico;
      }
    }, error =>{
      console.log(error);
    })
  }

  private getColumns(): object[]{
    return [
      {
        name:"Tarjeta",
        prop: "Tarjeta",
        flexGrow: 1
      },{
        name:"Estatus",
        prop: "EstatusTarjeta",
        flexGrow: 1
      },{
        name:"Fecha Reporte",
        prop: "FechaReporteBaja",
        flexGrow: 1
      },{
        name:"Motivo",
        prop: "Motivo",
        flexGrow: 1
      },{
        name:"Nombre Reporta",
        prop: "NombreReporta",
        flexGrow: 1
      },{
        name:"Usuario Reporta",
        prop: "UsuarioReporta",
        flexGrow: 1
      }      
    ]
  }
}
