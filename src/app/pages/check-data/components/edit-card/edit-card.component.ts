import {
  Component,
  OnInit,
  ChangeDetectorRef,
  Inject,
  Input
} from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CheckDataService } from "../../services/check-data.service";
import { Personal } from "../../models/entity/personal";
import { AuthService } from "src/app/pages/auth/auth.service";
import { AlertService } from "src/app/core/services/alert.service";
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { MessagesLoyaltyConstant } from "../../constant/messages-loyalty.constant";

export interface DialogData {
  status: number;
  title: string;
  message: string;
}

@Component({
  selector: "app-edit-card",
  templateUrl: "./edit-card.component.html",
  styleUrls: ["./edit-card.component.scss"]
})
export class EditCardComponent implements OnInit {
  @Input() cardNumber: string = "";
  editCardForm: FormGroup;
  statusList: Object[] = [];
  record: Personal;
  spinnerVisible = false;
  constructor(
    private apiUrl: CheckDataService,
    private authService: AuthService,
    private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    public alertService: AlertService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.buildForm();
    this.cardRetrivied(this.cardNumber);
  }

  private buildForm(): void {
    this.editCardForm = this.fb.group({
      cardNumber: [
        "",
        Validators.compose([
          Validators.required,
          Validators.maxLength(16),
          Validators.minLength(16)
        ])
      ],
      status: ["", Validators.required]
    });
  }

  ngAfterViewInit(): void {
    this.statusList = this.apiUrl.getStatus();
    this.ref.detectChanges();
  }

  private cardRetrivied = (cardNumber: string): void => {
    this.apiUrl.getAccount({ cardNumber: cardNumber }).subscribe(
      result => {
        if (result.Respuesta.CodigoRespuesta == 0) {
          this.record = result.CuentaHabiente;
          this.editCardForm.patchValue({
            cardNumber: result.CuentaHabiente.Tarjeta,
            status: this.getStatus(result.CuentaHabiente.EstatusTarjeta)
          });
        }
      },
      error => {
        console.log(error);
      }
    );
  };

  private onModalDialog(status: number, title: string, message: string): void {
    const dialogRef = this.dialog.open(DialogOverviewDialog, {
      width: "450px",
      data: { status: status, title: title, message: message }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.status == 0) {
          window.location.reload();
        }
      }
    });
  }

  public doSave = (formData: FormGroup): void => {
    this.alertService.clear();
    this.spinnerVisible = true;
    this.apiUrl
      .updateCard({
        cardNumber: this.record.Tarjeta,
        status: formData.value.status,
        username: this.authService.getAuthStatus().UsuarioPos.NombreUsuario
      })
      .subscribe(
        result => {
          this.spinnerVisible = false;
          if (result.CodigoRespuesta == 0) {
            this.onModalDialog(
              0,
              "Mensaje Correcto",
              MessagesLoyaltyConstant.MESSAGE_STATUS_SUCCESS
            );
          } else {
            this.onModalDialog(
              result.CodigoRespuesta,
              "Mensaje Error",
              result.Mensaje
            );
          }
        },
        error => {
          this.spinnerVisible = false;
          console.log(error);
        }
      );
  };

  getStatus = (status: string): number => {
    let result: number = 0;
    switch (status) {
      case "INACTIVA":
        result = 4;
        break;
      case "CANCELADA":
        result = 6;
        break;
      default:
        result = 0;
        break;
    }
    return result;
  };
}

@Component({
  selector: "dialog-overview-dialog",
  templateUrl: "dialog-overview-dialog.html"
})
export class DialogOverviewDialog {
  status: number;
  title: string = "";
  message: string = "";
  constructor(
    public dialogRef: MatDialogRef<DialogOverviewDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.status = data.status;
    this.title = data.title;
    this.message = data.message;
  }

  onClick(): void {
    this.dialogRef.close({
      status: this.status
    });
  }
}
