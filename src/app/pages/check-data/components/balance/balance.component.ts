import { Component, OnInit, Input, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { CheckDataService } from '../../services/check-data.service';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.scss']
})
export class BalanceComponent implements OnInit, AfterViewInit {
  
  @Input() cardNumber : string= "";
  items = [];
  public columns: Object[] = [];
  pageSize = 10;
  constructor(private ref: ChangeDetectorRef, private apiUrl: CheckDataService) { }

  ngOnInit() {
    this.retriviedBalance();
  }

  ngAfterViewInit(): void {
    this.columns = this.getColumns();
    this.ref.detectChanges();    
  }

  private retriviedBalance(): void{    
    this.apiUrl.getBalances({ cardNumber: this.cardNumber}).subscribe(
      result => {        
        if(result.Respuesta.CodigoRespuesta == 0){
          this.items = result.Beneficios;
        }else{
          this.items = [];
        }
      }
    ,error => { console.log(error)});
  }

  private getColumns(): Object[]{
    return [
      {
        name: "Beneficio",
        prop: "Descripcion",
        flexGrow: 1
      },{
        name: "Saldo",
        prop: "Saldo",      
        flexGrow: 1
      }
    ]
  }
}
