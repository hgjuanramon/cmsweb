import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { searchRequest } from '../../models/request/search-request';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CheckDataService } from '../../services/check-data.service';
import { SelectionType } from '@swimlane/ngx-datatable';

export interface DialogData {
  paramaters: searchRequest;
}

@Component({
  selector: 'app-detail-search',
  templateUrl: './detail-search.component.html',
  styleUrls: ['./detail-search.component.scss']
})

export class DetailSearchComponent implements OnInit {

  items : object[] = []
  public columns: Object[] = [];
  selected = [];
  SelectionType = SelectionType;
  isSearch: boolean = false;
  constructor(private service: CheckDataService, private ref: ChangeDetectorRef,public dialogRef: MatDialogRef<DetailSearchComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.usersRetrieved(data.paramaters as searchRequest);
   }

  ngOnInit() {

  }

  ngAfterViewInit(): void {
   this.columns = this.getColumns();
   this.ref.detectChanges();
  }

  usersRetrieved(data: searchRequest): void{
    this.isSearch = true;
    this.service.getUsersAcount(data).subscribe(response => {
      this.isSearch = false;
      if(response.DatosCuentaHabiente.length == 0 || response.Respuesta.CodigoRespuesta != 0 ){
        this.dialogRef.close({status: false, message : "Sin resultado de busqueda." });
      }else if(response.DatosCuentaHabiente.length == 1){
        this.dialogRef.close({status: true, data :  response.DatosCuentaHabiente[0].Tarjeta});
      }else{
        this.items = response.DatosCuentaHabiente 
      }      
      
    }, error => {
      this.isSearch = false;
      console.log(error);
    });
  }

  private getColumns(): Object[]{
    return [
      {
        name: "ID",
        prop: "IDP",
        flexGrow: 0.5
      },{
        name: "Tarjeta",
        prop: "Tarjeta",      
      },{
        name: "Nombre Completo",
        prop: "NombreCompleto",      
      },{
        name: "Estatus",
        prop: "TipoTarjeta",      
      }
    ]
  }

  onSelect({selected}) {
    let row = selected[0];
    let response = {status: false, data : null }

    if(row != null){
      response.status = true;
      response.data = row.Tarjeta;
    }

    this.dialogRef.close(response);   
  }
}
