import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataGeneralComponent } from './data-general.component';

describe('DataGeneralComponent', () => {
  let component: DataGeneralComponent;
  let fixture: ComponentFixture<DataGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
