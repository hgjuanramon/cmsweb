import { Component, OnInit, ChangeDetectorRef, ViewChild } from "@angular/core";
import { CheckDataService } from "../../services/check-data.service";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { searchRequest } from "../../models/request/search-request";

import { MatDialog } from "@angular/material/dialog";
import { DetailSearchComponent } from "../detail-search/detail-search.component";
import { Title } from "@angular/platform-browser";
import { AlertService } from "src/app/core/services/alert.service";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-data-general",
  templateUrl: "./data-general.component.html",
  styleUrls: ["./data-general.component.scss"]
})
export class DataGeneralComponent implements OnInit {
  @ViewChild("f") myNgForm;
  title = "Consulta Beneficiario";
  optionSearch = [];
  optionCities = [];
  public searchForm: FormGroup;
  submitted = false;
  searchItem: searchRequest;
  personal;
  isResult: boolean = false;
  spinnerVisible: boolean = false;
  constructor(
    private fb: FormBuilder,
    private titleTab: Title,
    private ref: ChangeDetectorRef,
    private serviceUrl: CheckDataService,
    public dialog: MatDialog,
    private alertService: AlertService,
    private router: Router
  ) {
    this.optionSearch = this.serviceUrl.getOptionSearch();
    this.optionCities = this.serviceUrl.getCities();
  }

  ngOnInit() {
    this.buildForm();
    this.setDefaultValues();
    this.titleTab.setTitle(this.title);
  }

  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }

  private buildForm() {
    this.searchForm = this.fb.group({
      typeOption: ["", [Validators.required]],
      startDate: ["", [Validators.required]],
      endDate: ["", [Validators.required]],
      cardNumber: [null, [Validators.pattern("^-?[0-9]\\d*(\\.\\d{1,2})?$")]],
      idp: [null],
      firstName: [null],
      lastName: [null],
      secondName: [null],
      phoneNumber: [
        "",
        [
          Validators.minLength(10),
          Validators.maxLength(10),
          Validators.pattern("^-?[0-9]\\d*(\\.\\d{1,2})?$")
        ]
      ],
      city: [null]
    });

    this.setTypeForm();
  }

  private setTypeForm() {
    this.searchForm.get("typeOption").valueChanges.subscribe(checked => {
      //debugger;
      if (checked) {
        if (checked === 1) {
          this.idp.clearValidators();
          this.cardNumber.clearValidators();
        }

        if (checked == 2) {
          this.cardNumber.setValidators([
            Validators.required,
            Validators.pattern("^-?[0-9]\\d*(\\.\\d{1,2})?$"),
            Validators.minLength(16),
            Validators.maxLength(16)
          ]);
          this.idp.clearValidators();
        }

        if (checked == 3) {
          this.idp.setValidators([Validators.required]);
          this.cardNumber.clearValidators();
        }
      }

      this.firstName.updateValueAndValidity();
      this.lastName.updateValueAndValidity();
      this.cardNumber.updateValueAndValidity();
      this.idp.updateValueAndValidity();
    });
  }

  private setDefaultValues() {
    this.searchForm.patchValue({
      typeOption: 2,
      startDate: new Date(new Date().setMonth(new Date().getMonth() - 1)),
      endDate: new Date(),
      cardNumber: environment.prefixCard
    });
  }

  get cardNumber() {
    return this.searchForm.get("cardNumber") as FormControl;
  }

  get idp() {
    return this.searchForm.get("idp") as FormControl;
  }

  get firstName() {
    return this.searchForm.get("firstName") as FormControl;
  }

  get lastName() {
    return this.searchForm.get("lastName") as FormControl;
  }

  public doSearch(request: FormGroup) {
    this.submitted = true;
    this.isResult = false;
    this.personal = {};
    this.alertService.clear();
    if (this.searchForm.invalid) {
      return;
    }

    if (this.searchForm.valid) {
      if (request.value.typeOption == 1 || request.value.typeOption == 3) {
        const dialogRef = this.dialog.open(DetailSearchComponent, {
          width: "800px",
          disableClose: true,
          data: { paramaters: request.value }
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result.status == true) {
            this.getData(request, result.data);
          } else {
            this.isResult = false;
            this.personal = {};
            this.alertService.success(result.message);
          }
        });
      } else {
        this.getData(request);
      }
    }
  }

  private getData = (request: FormGroup, cardNumber: string = ""): void => {
    this.spinnerVisible = true;
    let parameters = Object.assign(
      {},
      {
        cardNumber: "",
        startDate: request.value.startDate,
        endDate: request.value.endDate,
        maxNumMovs: 50
      }
    );
    parameters.cardNumber = cardNumber ? cardNumber : request.value.cardNumber;
    this.serviceUrl.getAccount(parameters).subscribe(
      result => {
        this.spinnerVisible = false;
        if (result.Respuesta.CodigoRespuesta != 0) {
          this.personal = {};
          this.isResult = false;
          this.alertService.success(result.Respuesta.Mensaje);
        } else {
          this.personal = result.CuentaHabiente;
          this.isResult = true;
          this.searchItem = parameters;
        }
      },
      error => {
        console.log(error);
        this.spinnerVisible = false;
      }
    );
  };

  public isVisible(param: number): boolean {
    if (this.searchForm.value.typeOption == param) return true;

    return false;
  }

  public doReset() {
    this.submitted = false;
    this.isResult = false;
    this.myNgForm.resetForm();
    this.setDefaultValues();
  }
}
