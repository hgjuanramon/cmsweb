import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { searchRequest } from '../../models/request/search-request';
import { CheckDataService } from '../../services/check-data.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-movement',
  templateUrl: './movement.component.html',
  styleUrls: ['./movement.component.scss'],
  providers: [DatePipe]
})
export class MovementComponent implements OnInit {

  @Input() cardNumber : string= "";
  @Input() searchItems: searchRequest;
  items = [];
  public columns: Object[] = [];
  pageSize = 10;
  constructor(private ref: ChangeDetectorRef, private apiUrl: CheckDataService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.retrievedMovements();
  }

  ngAfterViewInit(): void {
    this.columns = this.getColumns();
    this.ref.detectChanges();    
  }

  private retrievedMovements(): void{ 
    let request = { 
      cardNumber: this.cardNumber, 
      startDate: this.datePipe.transform(this.searchItems.startDate.toString(),"yyyy-MM-dd") + " 00:00:00",
      endDate:  this.datePipe.transform(this.searchItems.endDate,"yyyy-MM-dd") + " 23:59:59",
      maxNumMovs: this.searchItems.maxNumMovs 
    }

    this.apiUrl.getMovements(request).subscribe(
      result => {
        if(result.Respuesta.CodigoRespuesta == 0){
          this.items = result.Movimientos;
        }else{
          this.items = [];
        }
      }
    , error => { console.log(error)});
  }

  private getColumns(): Object[]{
    return [
      {
        name: "Fecha Registro",
        prop: "FechaRegistro",
        flexGrow: 0.5
      },{
        name: "Autorizacion",
        prop: "Autorizacion",
        flexGrow: 0.5
      },{
        name: "Comercio",
        prop: "Comercio",
        flexGrow: 0.5
      },{
        name: "Concepto",
        prop: "ConceptoOperacion",
        flexGrow: 0.5
      },{
        name: "Importe",
        prop: "Importe",
        flexGrow: 0.5,
      },{
        name: "Cargo/Abono",
        prop: "CargoAbono",
        flexGrow: 0.5
      },{
        name: "Saldo Final",
        prop: "SaldoFinal",
        flexGrow: 0.5,
      },{
        name: "Beneficio",
        prop: "Beneficio",
        flexGrow: 0.5,
      },{
        name: "Campana",
        prop: "Campana",
        flexGrow: 1
      }
    ]
  }
}
