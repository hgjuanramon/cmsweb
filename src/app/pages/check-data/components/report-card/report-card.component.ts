import { Component, OnInit, Input, ViewChild } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import { CheckDataService } from "../../services/check-data.service";
import { AuthService } from "src/app/pages/auth/auth.service";
import { Personal } from "../../models/entity/personal";
import { AlertService } from "src/app/core/services/alert.service";

@Component({
  selector: "app-report-card",
  templateUrl: "./report-card.component.html",
  styleUrls: ["./report-card.component.scss"]
})
export class ReportCardComponent implements OnInit {
  @ViewChild("formCardReport") myNgForm;
  @Input() cardNumber: string = "";
  cardReportForm: FormGroup;
  record: Personal;
  spinnerVisible: boolean = false;
  reasons = [];
  constructor(
    private fb: FormBuilder,
    private urlService: CheckDataService,
    private authService: AuthService,
    public alertService: AlertService
  ) {
    this.getReasonReport();
  }

  ngOnInit() {
    this.buildForm();
    this.dataRetrieved(this.cardNumber);
  }

  private buildForm(): void {
    this.cardReportForm = this.fb.group({
      reasonId: ["", [Validators.required]],
      oldCardNumber: [
        "",
        [
          Validators.required,
          Validators.minLength(16),
          Validators.maxLength(16)
        ]
      ],
      fullname: [null],
      fullnameReport: ["", [Validators.required]],
      newCardNumber: [
        "",
        [
          Validators.required,
          Validators.minLength(16),
          Validators.maxLength(16)
        ]
      ],
      observationReport: ["", [Validators.required]]
    });
  }

  get fullnameReport() {
    return this.cardReportForm.get("fullnameReport") as FormControl;
  }

  get reasonId() {
    return this.cardReportForm.get("reasonId") as FormControl;
  }

  get observationReport() {
    return this.cardReportForm.get("observationReport") as FormControl;
  }

  get oldCardNumber() {
    return this.cardReportForm.get("oldCardNumber") as FormControl;
  }

  get newCardNumber() {
    return this.cardReportForm.get("newCardNumber") as FormControl;
  }

  private dataRetrieved = (cardNumber: string): void => {
    this.urlService.getAccount({ cardNumber: cardNumber }).subscribe(
      result => {
        if (result.Respuesta.CodigoRespuesta == 0) {
          this.record = result.CuentaHabiente;
          this.cardReportForm.patchValue({
            oldCardNumber: result.CuentaHabiente.Tarjeta,
            fullname:
              result.CuentaHabiente.Nombre +
              " " +
              result.CuentaHabiente.PrimerApellido +
              " " +
              result.CuentaHabiente.SegundoApellido
          });
        }
      },
      error => {
        console.log(error);
      }
    );
  };

  private getReasonReport() {
    this.urlService.getReasonReport().subscribe(result => {
      if (result.Respuesta.CodigoRespuesta == 0) {
        this.reasons = result.MotivosReportes.filter(x => x.IdMotivo != 0);
      } else {
        this.reasons = [];
      }
    });
  }

  public saveReportCard(): void {
    this.spinnerVisible = true;
    const request = {
      fullnameReport: this.fullnameReport.value,
      reasonId: this.reasonId.value,
      observations: this.observationReport.value,
      oldCardNumber: this.record.Tarjeta,
      newCarNumber: this.newCardNumber.value,
      username: this.authService.getAuthStatus().UsuarioPos.NombreUsuario
    };

    this.alertService.clear();
    this.urlService.transferBalance(request).subscribe(
      result => {
        this.spinnerVisible = false;
        if (result.CodigoRespuesta == 0) {
          this.alertService.success(
            "Se Realizó el Transpaso de Saldo Correctamente."
          );
          this.myNgForm.resetForm();
          this.dataRetrieved(request.oldCardNumber);
        } else {
          console.log(result);
          this.myNgForm.resetForm();
          this.dataRetrieved(request.oldCardNumber);
          this.alertService.error(result.Mensaje);
        }
      },
      error => {
        this.spinnerVisible = false;
        console.log(error);
      }
    );
  }

  doReset = (): void => {
    this.myNgForm.resetForm();
    this.dataRetrieved(this.cardNumber);
  };
}
