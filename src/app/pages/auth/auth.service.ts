import { Injectable } from '@angular/core';
import { Response } from 'src/app/core/models/response';
import { Observable, BehaviorSubject, throwError as observableThrowError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map }  from 'rxjs/operators';
import { transformError } from 'src/app/core/commons/common';
import { CacheService } from '../../core/services/cache.service';
import { UserRolResponse } from 'src/app/core/models/navbar';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})

export class AuthService extends CacheService{

  private readonly authProvider: (email: string, password: string) => Observable<IServerAuthResponse>;
  authStatus = new BehaviorSubject<IAuthStatus>(this.getItem('authStatus') || defaultAuthStatus);

  constructor(private httpClient: HttpClient) { 
    super();
    this.authStatus.subscribe(authStatus => {
      this.setItem('authStatus' , authStatus)
    });
    this.authProvider = this.userAuthProvider;
  }

  private userAuthProvider(email: string, password: string): Observable<IServerAuthResponse>{
    return this.httpClient.post<IServerAuthResponse>(`${environment.urlService}/LoginApi/Post`,{correo: email, password: password}, httpOptions);
  }

  public recoverPassword(email: string): Observable<Response>{
    return this.httpClient.post<Response>(`${environment.urlService}/Admin/RecuperaPassword`,{Cadena: email});
  }

  public validateToken(token: string): Observable<Response>{
    return this.httpClient.post<Response>(`${environment.urlService}/Admin/EsTokenValido`,{}, {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'TokenComunicacion': token
      })
    });
  }

  public changePassword(token: string, newPassword: string): Observable<Response>{
    return this.httpClient.post<Response>(`${environment.urlService}/Admin/ResetPassword`, {NewPassword: newPassword}, {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'TokenComunicacion': token
      })
    });
  }

  login(email: string , password: string): Observable<IAuthStatus>{
    this.logout();
    const loginResponse = this.authProvider(email,password).pipe(
      map(value => {
        if(value.Respuesta.CodigoRespuesta == 0){
          this.setToken(value.UsuarioPos.NombreUsuario);
        }
        return value as IAuthStatus;
      }),
      catchError(transformError)
    );

    loginResponse.subscribe(
      res => {
        this.authStatus.next(res);
      },
      err => {
        this.logout();
        return observableThrowError(err);
      }
    )

    return loginResponse;
  }

  logout(){
    this.clearToken();
    this.authStatus.next(defaultAuthStatus);
  }

  private setToken(jwt: string){
    this.setItem("jwt",jwt);
  }

  getToken():string{
    return this.getItem("jwt") || "";
  }

  private clearToken(){
    this.removeItem("jwt");
  }

  getAuthStatus(): IAuthStatus{
    return this.getItem("authStatus");
  }

  getMenus(): Observable<UserRolResponse>{
    var user = this.getAuthStatus();
    return this.httpClient.post<UserRolResponse>(`${environment.urlService}/Admin/ObtieneRolesPermisos`,{Valor: user.UsuarioPos.IdusuarioInterno}, httpOptions);
  }
}

export interface IAuthStatus {
  Respuesta: Response;
  UsuarioPos: User;
}

export interface User {
    IdusuarioInterno: number;
    Nombre: string;
    NombreUsuario: string;
    Email: string;
    IdEstatus: boolean;
    Contrasenia: string;
}

export interface Rol {
    IdRol: number,
    Nombre: string,
    Descripcion: string
}

interface IServerAuthResponse {
  Respuesta: Response;
  UsuarioPos: User;  
}

const defaultAuthStatus: IAuthStatus = { Respuesta: null, UsuarioPos: null};