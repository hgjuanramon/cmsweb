import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "src/app/core/guards/auth.guard";
import { UserListComponent } from "./components/user-list/user-list.component";

const userRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "",
        component: UserListComponent
      }
    ],
    canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(userRoutes)],
  exports: [RouterModule]
})
export class UserPosRoutingModule {}
