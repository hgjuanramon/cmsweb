import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AddUserComponent } from "./components/add-user/add-user.component";
import { EditUserComponent } from "./components/edit-user/edit-user.component";
import { AddUserVariablesComponent } from "./components/add-user-variables/add-user-variables.component";
import { UserListComponent } from "./components/user-list/user-list.component";
import { MaterialModule } from "src/app/material.module";
import { SharedModule } from "src/app/shared/shared.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { UserPosRoutingModule } from "./user-pos-routing.module";

@NgModule({
  declarations: [
    UserListComponent,
    AddUserComponent,
    EditUserComponent,
    AddUserVariablesComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MaterialModule,
    UserPosRoutingModule
  ],
  entryComponents: [
    AddUserComponent,
    EditUserComponent,
    AddUserVariablesComponent
  ]
})
export class UserPosModule {}
