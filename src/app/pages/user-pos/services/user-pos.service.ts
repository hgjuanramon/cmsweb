import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { UserPosResponse } from "../models/response/user-pos-.response";
import { ApiUserPosConstant } from "../constants/api-user-pos-constant";
import { UserSearchRequest } from "../models/request/user-search.request";
import { UserPosRequest } from "../models/request/user-pos.request";
import { Response } from "src/app/core/models/response";
import { UserVariableResponse } from "../models/response/user-variable.response";
import { VariableResponse } from "../models/response/variable.response";
import { UserVariable } from "../models/request/user-variable.request";

@Injectable({
  providedIn: "root"
})
export class UserPosService {
  constructor(private httpClient: HttpClient) {}

  public getUsers(request: UserSearchRequest): Observable<UserPosResponse> {
    return this.httpClient.post<UserPosResponse>(
      ApiUserPosConstant.GET_ALL_USER,
      request
    );
  }

  public saveUser(request: UserPosRequest): Observable<Response> {
    return this.httpClient.post<Response>(
      ApiUserPosConstant.SAVE_USER,
      request
    );
  }

  public getVariables(): Observable<VariableResponse> {
    return this.httpClient.post<VariableResponse>(
      ApiUserPosConstant.GET_VARIABLES,
      { Cadena: "POSWEB.Reportes" }
    );
  }

  public getVpfByAgentId(agentId: number): Observable<UserVariableResponse> {
    return this.httpClient.post<UserVariableResponse>(
      ApiUserPosConstant.GET_USER_VARIABLE,
      { Valor: agentId }
    );
  }

  public saveVariableUser(request: UserVariable): Observable<Response> {
    return this.httpClient.post<Response>(
      ApiUserPosConstant.SAVE_VARIABLES_USER,
      request
    );
  }
}
