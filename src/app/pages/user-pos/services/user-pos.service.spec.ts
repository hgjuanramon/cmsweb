import { TestBed } from '@angular/core/testing';

import { UserPosService } from './user-pos.service';

describe('UserPosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserPosService = TestBed.get(UserPosService);
    expect(service).toBeTruthy();
  });
});
