import { Component, OnInit } from "@angular/core";
import { Validators, FormGroup, FormBuilder } from "@angular/forms";
import { UserPosRequest } from "../../models/request/user-pos.request";
import { UserPosService } from "../../services/user-pos.service";
import { MatDialogRef } from "@angular/material/dialog";
import { AuthService } from "src/app/pages/auth/auth.service";
import { UserPosMessageConstant } from "../../constants/user-pos-message.constant";

@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.scss"]
})
export class AddUserComponent implements OnInit {
  userForm: FormGroup;
  user: UserPosRequest;
  messageError = "";
  constructor(
    private fb: FormBuilder,
    private userService: UserPosService,
    private dialogRef: MatDialogRef<AddUserComponent>,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.buildUserForm();
  }

  private buildUserForm = () => {
    this.userForm = this.fb.group({
      Nombre: [null, [Validators.required]],
      ApellidoPaterno: [null, [Validators.required]],
      Email: [null, [Validators.required, Validators.email]]
    });
  };

  public saveUser = (): void => {
    this.messageError = "";
    let parameters = Object.assign({}, this.user, this.userForm.value);
    parameters.UsuarioAlta = this.authService.getAuthStatus().UsuarioPos.NombreUsuario;
    this.userService.saveUser(parameters).subscribe(result => {
      if (result.CodigoRespuesta == 0) {
        result.Mensaje = UserPosMessageConstant.SUCCESS_ADD;
        this.dialogRef.close(result);
      } else {
        this.messageError = result.Mensaje;
      }
    });
  };
}
