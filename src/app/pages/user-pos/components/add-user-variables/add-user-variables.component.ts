import {
  Component,
  OnInit,
  Inject,
  ChangeDetectorRef,
  AfterViewInit,
} from "@angular/core";
import { UserPosService } from "../../services/user-pos.service";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { AuthService } from "src/app/pages/auth/auth.service";
import { UserPosMessageConstant } from "../../constants/user-pos-message.constant";

export interface DialogData {
  agentId: number;
}

@Component({
  selector: "app-add-user-variables",
  templateUrl: "./add-user-variables.component.html",
  styleUrls: ["./add-user-variables.component.scss"],
})
export class AddUserVariablesComponent implements OnInit, AfterViewInit {
  public variableList: Object[] = [];
  userVariableForm: FormGroup;
  tooltipCorporate: string = "";
  tooltipRegion: string = "";
  tooltipGerencia: string = "";
  constructor(
    private ref: ChangeDetectorRef,
    private userPosService: UserPosService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<AddUserVariablesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public authService: AuthService
  ) {}

  ngOnInit() {
    this.buildUserVariableForm();
    this.setValueUserVariable(this.data.agentId);
    this.setTooltip();
  }

  ngAfterViewInit(): void {
    this.ref.detectChanges();
  }

  buildUserVariableForm(): void {
    this.userVariableForm = this.fb.group({
      Corporativo: [null, [Validators.required]],
      Region: [null, [Validators.required]],
      Gerencia: [null, [Validators.required]],
      Cadena: [null, [Validators.required]],
      EsAdministrador: [null],
    });
  }

  get Corporativo() {
    return this.userVariableForm.get("Corporativo") as FormControl;
  }

  get Region() {
    return this.userVariableForm.get("Region") as FormControl;
  }

  get Gerencia() {
    return this.userVariableForm.get("Gerencia") as FormControl;
  }

  public saveVariable() {
    this.userPosService.getVariables().subscribe(
      (result) => {
        debugger;
        if (result.Respuesta.CodigoRespuesta == 0) {
          if (result.Profundidad.length > 0) {
            for (var i = 0; i < result.Profundidad.length; i++) {
              var record = result.Profundidad[i];

              var request = {
                idProfundidad: parseInt(record.IdProfundidad.toString()),
                idUsuario: this.data.agentId,
                valor: this.userVariableForm.get(record.Nombre).value,
                usuarioAlta: this.authService.getAuthStatus().UsuarioPos
                  .NombreUsuario,
              };
              if (record.Nombre == "EsAdministrador") {
                request.valor =
                  this.userVariableForm.get(record.Nombre).value == true
                    ? 1
                    : 0;
              }

              this.userPosService.saveVariableUser(request).subscribe(
                (result) => {},
                (error) => {
                  console.log(error);
                }
              );
            }
          }
        }

        this.dialogRef.close({
          CodigoRespuesta: 0,
          Mensaje: UserPosMessageConstant.MESSAGE_VARIABLE_SUCCESS_ADD,
        });
      },
      (error) => {
        console.log(error);
      }
    );
  }

  private setValueUserVariable(userId: number) {
    this.userPosService.getVpfByAgentId(userId).subscribe((result) => {
      if (result.Respuesta.CodigoRespuesta == 0) {
        if (result.UsuarioProfundidad) {
          for (var i = 0; i < result.UsuarioProfundidad.length; i++) {
            var record = result.UsuarioProfundidad[i];
            var field = this.userVariableForm.get(record.Profundidad);
            if (record.Profundidad == "EsAdministrador") {
              var valueField = record.Valor == "1" ? true : false;
              field.setValue(valueField);
            } else {
              field.setValue(record.Valor);
            }
          }
        }
      }
    });
  }

  setTooltip() {
    this.tooltipCorporate = this.getCorporativo().join("\n");
    this.tooltipRegion = this.getRegion().join("\n");
    this.tooltipGerencia = this.getGerencia().join("\n");
  }

  setTooltipManager() {
    this.tooltipGerencia = this.getGerencia(parseInt(this.Region.value)).join(
      "\n"
    );
  }

  getCorporativo(): Object[] {
    return ["* TODAS", "2 VP COMERCIAL MEXICO"];
  }

  getRegion(): Object[] {
    return [
      "* TODAS",
      "3 COMERCIAL CENTRO",
      "4 COMERCIAL NORESTE",
      "5 COMERCIAL PACÍFICO",
      "6 COMERCIAL SURESTE",
      "7 Sin Región",
    ];
  }

  getGerencia(regionId = 0): Object[] {
    let managerList = ["* TODAS"];

    switch (regionId) {
      case 3:
        managerList.push("8 METRO DF");
        managerList.push("9 GUERRERO - MORELOS");
        managerList.push("10 ESTADO DE MEXICO");
        managerList.push("11 QUERETARO - HIDALGO");
        managerList.push("12 MICHOACAN");
        break;
      case 4:
        managerList.push("14 GOLFO NORTE-SLP (HUASTECA)");
        managerList.push("15 GUANAJUATO");
        managerList.push("16 COAHUILA-DURANGO");
        managerList.push("17 NUEVO LEON-TAMAULIPAS");
        managerList.push("18 AGS-ZAC-SLP");
        break;
      case 5:
        managerList.push("19 LAS CALIFORNIAS");
        managerList.push("20 GDL-ALTOS");
        managerList.push("21 SINALOA");
        managerList.push("22 SONORA");
        managerList.push("23 JAL-COL-NAY");
        break;
      case 6:
        managerList.push("24 PENINSULA");
        managerList.push("25 SUR OAXACA");
        managerList.push("26 SUR CHIAPAS");
        managerList.push("27 PUEBLA - TLAXCALA");
        managerList.push("28 SUR VER-TAB");
        break;
      case 7:
        managerList.push("13 SLP");
        managerList.push("29 DF NORTE");
        managerList.push("30 Sin Gerencia");
        managerList.push("31 BAJA CALIF. NTE");
        break;
      default:
        break;
    }

    return managerList;
  }
}
