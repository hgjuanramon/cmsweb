import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUserVariablesComponent } from './add-user-variables.component';

describe('AddUserVariablesComponent', () => {
  let component: AddUserVariablesComponent;
  let fixture: ComponentFixture<AddUserVariablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUserVariablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserVariablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
