import {
  Component,
  OnInit,
  ChangeDetectorRef,
  TemplateRef,
  ViewChild
} from "@angular/core";
import { AddUserVariablesComponent } from "../add-user-variables/add-user-variables.component";
import { AddUserComponent } from "../add-user/add-user.component";
import { FormControl, FormBuilder, FormGroup } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { Title } from "@angular/platform-browser";
import { AlertService } from "src/app/core/services/alert.service";
import { UserPosService } from "../../services/user-pos.service";

@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.scss"]
})
export class UserListComponent implements OnInit {
  title: string = "Usuarios";
  items: object[] = [];
  public columns: Object[] = [];
  public pageOfSize: number = 15;
  showSpinner: boolean = false;
  searchForm: FormGroup;
  @ViewChild("ngSearchForm") ngSearchForm;
  @ViewChild("actionCellTemplate")
  actionCellTemplate: TemplateRef<any>;
  constructor(
    private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private userService: UserPosService,
    private alertService: AlertService,
    private titleTabPage: Title,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.buildSearchForm();
    this.titleTabPage.setTitle(this.title);
  }

  ngAfterViewInit(): void {
    this.getColumns();
    this.ref.detectChanges();
  }

  private buildSearchForm = (): void => {
    this.searchForm = this.fb.group({
      fulltext: [null]
    });
  };

  get fulltext() {
    return this.searchForm.get("fulltext") as FormControl;
  }

  public doSearch = (): void => {
    this.alertService.clear();
    this.showSpinner = true;
    this.userService.getUsers({ Cadena: this.fulltext.value }).subscribe(
      result => {
        this.showSpinner = false;
        if (result.Respuesta.CodigoRespuesta == 0) {
          this.items = result.UsuariosAdminPosWeb;
        } else {
          this.alertService.error(result.Respuesta.Mensaje);
        }
      },
      error => {
        this.showSpinner = false;
        console.log(error);
      }
    );
  };

  public addUser = () => {
    let dialogRef = this.dialog.open(AddUserComponent, {
      width: "600px",
      disableClose: true,
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.CodigoRespuesta == 0) {
        this.alertService.success(result.Mensaje);
      }
    });
  };

  public addVariable(row) {
    let dialogRef = this.dialog.open(AddUserVariablesComponent, {
      width: "600px",
      disableClose: true,
      data: { agentId: row.IdUsuario }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.CodigoRespuesta == 0) {
        this.alertService.success(result.Mensaje);
      }
    });
  }

  public doReset = (): void => {
    this.alertService.clear();
    this.items = [];
    this.ngSearchForm.resetForm();
  };

  private getColumns = (): void => {
    this.columns = [
      {
        name: "ID",
        prop: "IdUsuario",
        flexGrow: 1
      },
      {
        name: "Usuario",
        prop: "NombreUsuario",
        flexGrow: 1
      },
      {
        name: "Nombres",
        prop: "Nombres",
        flexGrow: 1
      },
      {
        name: "Email",
        prop: "Email",
        flexGrow: 1
      },
      {
        name: "Estatus",
        prop: "Estatus",
        flexGrow: 1
      },
      {
        name: "Acciones",
        cellTemplate: this.actionCellTemplate,
        flexGrow: 1
      }
    ];
  };
}
