export interface VariablePF {
  IdProfundidad: Number;
  Nombre: string;
  Descripcion: string;
  Agrupador: string;
}
