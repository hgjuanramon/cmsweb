export interface Variable {
  IdProfundidad: number;
  Profundidad: string;
  Valor: string;
}
