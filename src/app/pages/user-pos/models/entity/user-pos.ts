export interface UserPos {
  IdUsuario: number;
  NombreUsuario: string;
  Comercio: string;
  Rol: string;
  Nombres: string;
  Email: string;
  UsuarioAlta: string;
  FechaAlta: string;
  UsuarioCambio: string;
  FechaCambio: string;
  ID_Sucursal: number;
  Estatus: string;
}
