import { Response } from "src/app/core/models/response";
import { UserPos } from "../entity/user-pos";

export interface UserPosResponse {
  Respuesta: Response;
  UsuariosAdminPosWeb: UserPos[];
}
