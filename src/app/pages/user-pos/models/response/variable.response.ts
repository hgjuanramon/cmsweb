import { Response } from "src/app/core/models/response";
import { VariablePF } from "../entity/variablePF";

export interface VariableResponse {
  Respuesta: Response;
  Profundidad: VariablePF[];
}
