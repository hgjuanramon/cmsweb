export interface UserVariable {
  idUsuario: number;
  idProfundidad: number;
  valor: string;
  usuarioAlta: string;
}
