export interface UserPosRequest {
  Nombre: string;
  ApellidoPaterno: string;
  Email: string;
  UsuarioAlta: string;
}
