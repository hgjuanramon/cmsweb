import { environment } from "src/environments/environment";

export class ApiUserPosConstant {
  public static GET_ALL_USER = `${environment.urlService}/Admin/ObtieneUsuarioByComodin`;
  public static SAVE_USER = `${environment.urlService}/Admin/InsertaUsuarioPosWeb`;
  public static GET_USER_VARIABLE = `${environment.urlService}/ReportsPosweb/ObtieneConfiguracion`;
  public static GET_VARIABLES = `${environment.urlService}/ReportsPosweb/ObtieneProfundidad`;
  public static SAVE_VARIABLES_USER = `${environment.urlService}/ReportsPosweb/AddUsuarioProfudidad`;
}
