import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from 'src/app/core/guards/auth.guard';

const homeRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',      
        component: HomeComponent
      }
    ],
    canActivate:[AuthGuard],
    
  }
]
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(homeRoutes)
  ],
  exports:[RouterModule]
})
export class HomeRoutingModule { }
