import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "src/app/core/guards/auth.guard";
import { AnalyticTransactionComponent } from "./components/analytic-transaction/analytic-transaction.component";

const campaignRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "analytics-transaction",
        component: AnalyticTransactionComponent,
      },
    ],
    canActivate: [AuthGuard],
  },
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(campaignRoutes)],
  exports: [RouterModule],
})
export class CampaignManagerRoutingModule {}
