import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import { ApiCampaignConstant } from "../constants/api-campaign.constant";
import { Observable } from "rxjs";
import { Response } from "src/app/core/models/response";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/catch";
import { AnalyticTransactionResponse } from "../models/response/analytic-transaction.response";

@Injectable({
  providedIn: "root",
})
export class CampaignManagerService {
  constructor(private httlClient: HttpClient) {}

  public getAnalyticTransaction(): Observable<AnalyticTransactionResponse> {
    return this.httlClient
      .get<AnalyticTransactionResponse>(ApiCampaignConstant.GET_TRANSACTIONS, {
        headers: new HttpHeaders({
          'Content-Type': "text/xml",
          'accept': "text/xml",
          'Access-Control-Allow-Origin': '*'
        }),
      })
      .catch(this.handleError);
  }

  public handleError(err: HttpErrorResponse) {
    console.log(err.message);
    return Observable.throw(err.message);
  }
}
