export interface AnalyticTransaction {
  idBusqueda: number;
  idTipoBusqueda: number;
  esGrupo: boolean;
  idAgrupacion: number;
  name: string;
  descripcion: string;
  recalcular: boolean;
  todosCualquiera: boolean;
  creaAgrupacion: boolean;
  valorTop: string;
  esActualizacion: boolean;
  usuarioAlta: string;
  fechaAlta: string;
  usuarioCambio: string;
  fechaCambio: string;
  esSilverPop: boolean;
  fechaProceso: string;
  proceso: boolean;
  criterios: string;
  criteriosSeleccionados: string;
}
