import { AnalyticTransaction } from "../entities/analytic-transaction";

export interface AnalyticTransactionResponse {
  codigoRespuesta: number;
  mensaje: string;
  busquedaLista: AnalyticTransaction[];
}
