import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalyticTransactionComponent } from './analytic-transaction.component';

describe('AnalyticTransactionComponent', () => {
  let component: AnalyticTransactionComponent;
  let fixture: ComponentFixture<AnalyticTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalyticTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyticTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
