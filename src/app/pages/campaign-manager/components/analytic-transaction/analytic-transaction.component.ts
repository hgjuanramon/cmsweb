import {
  Component,
  OnInit,
  ViewChild,
  TemplateRef,
  AfterViewInit,
  ChangeDetectorRef,
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { CampaignManagerService } from "../../services/campaign-manager.service";
import { AlertService } from "src/app/core/services/alert.service";

@Component({
  selector: "app-analytic-transaction",
  templateUrl: "./analytic-transaction.component.html",
  styleUrls: ["./analytic-transaction.component.scss"],
})
export class AnalyticTransactionComponent implements OnInit, AfterViewInit {
  pageSize = 20;
  public items = [];
  public columns: Object[] = [];
  @ViewChild("ngSearchForm")
  ngSearchForm;
  @ViewChild("actionCellTemplate")
  actionCellTemplate: TemplateRef<any>;
  spinnerVisible: boolean = false;
  searchForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private urlService: CampaignManagerService,
    private alertService: AlertService
  ) {
    this.getAnalyticTransaction();
  }

  ngOnInit(): void {
    this.buildSearchForm();
  }

  ngAfterViewInit(): void {
    this.columns = this.getColumns();
    this.ref.detectChanges();
  }

  public buildSearchForm() {
    this.searchForm = this.fb.group({
      title: [null, [Validators.required]],
    });
  }

  public getAnalyticTransaction() {
    this.alertService.clear();
    this.spinnerVisible = true;
    this.urlService.getAnalyticTransaction().subscribe(
      (result) => {
        this.spinnerVisible = false;
        if (result.codigoRespuesta == 0) {
          this.items = result.busquedaLista;
        } else {
          this.items = [];
          this.alertService.error(result.mensaje);
        }
      },
      (error) => {
        this.spinnerVisible = false;
        console.log(error);
      }
    );
  }

  public doSearch() {
    this.getAnalyticTransaction();
  }

  public add() {}

  public export(row) {}

  public duplicate(row) {}

  public delete(row) {}

  public reset() {
    this.alertService.clear();
    this.items = [];
    this.ngSearchForm.resetForm();
  }

  private getColumns(): Object[] {
    return [
      {
        name: "Descripción",
        prop: "descripcion",
        flexGrow: 1,
      },
      {
        name: "Fecha Creación",
        prop: "fechaAlta",
        flexGrow: 1,
      },
      {
        name: "Usuario Creación",
        prop: "usuarioCambio",
        flexGrow: 1,
      },
      {
        name: "Acciones",
        cellTemplate: this.actionCellTemplate,
        flexGrow: 1,
      },
    ];
  }
}
