import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "src/app/shared/shared.module";
import { MaterialModule } from "src/app/material.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CampaignManagerRoutingModule } from "./campaign-manager-routing.module";
import { AnalyticTransactionComponent } from './components/analytic-transaction/analytic-transaction.component';

@NgModule({
  declarations: [AnalyticTransactionComponent],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CampaignManagerRoutingModule,
  ],
})
export class CampaignManagerModule {}