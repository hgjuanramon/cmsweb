import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthGuard } from "src/app/core/guards/auth.guard";
import { BranchBonusGoalsComponent } from "./components/branch-bonus-goals/branch-bonus-goals.component";
import { Routes, RouterModule } from "@angular/router";
import { BranchGoalsComponent } from "./components/branch-goals/branch-goals.component";
import { TransactionsAgentComponent } from "./components/transactions-agent/transactions-agent.component";
import { TxConstrulograsComponent } from "./components/tx-construlogras/tx-construlogras.component";
import { PrincipalComponent } from "./components/principal/principal.component";
import { TransactionDetailComponent } from "./components/transaction-detail/transaction-detail.component";

const reportRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "goals-branch",
        component: BranchGoalsComponent,
      },
      {
        path: "bonus-goals-branch",
        component: BranchBonusGoalsComponent,
      },
      {
        path: "transaction-by-agent",
        component: TransactionsAgentComponent,
      },
      {
        path: "tx-construlogras",
        component: TxConstrulograsComponent,
      },
      {
        path: "principal",
        component: PrincipalComponent,
      },
      {
        path: "transaction-detail",
        component: TransactionDetailComponent,
      },
    ],
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(reportRoutes)],
  exports: [RouterModule],
  declarations: [],
})
export class ReportRoutingModule {}
