import { Response } from 'src/app/core/models/response';
import { TotalPoswebAgent } from '../entity/total-posweb-agent';

export interface TotalPoswebAgentResponse{
    Respuesta: Response;
    TotalAgentePosWeb: TotalPoswebAgent[];
}