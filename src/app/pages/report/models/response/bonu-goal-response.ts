import { Response } from 'src/app/core/models/response';
import { BonuGoal } from '../entity/bonu-goal';

export interface BonuGoalResponse {
    Respuesta: Response,
    BonificionesMetas: BonuGoal[]
}