import { TxDetail } from "../entity/tx-detail";
import { Response } from "src/app/core/models/response";

export interface TxDetailResponse {
  Respuesta: Response;
  Transacciones: TxDetail[];
  UrlDescarga: string;
}
