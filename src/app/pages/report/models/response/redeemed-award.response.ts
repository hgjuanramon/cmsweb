import { Response } from 'src/app/core/models/response';
import { RedeemedAward } from '../entity/redeemed-award';

export interface RedeemedAwardResponse {
    Respuesta: Response;
    PremiosPos: RedeemedAward[];
}