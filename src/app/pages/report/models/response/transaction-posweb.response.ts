import { Response } from 'src/app/core/models/response';
import { TransactionPosweb } from '../entity/transaction-posweb';

export interface TransactionPoswebResponse{
    Respuesta: Response;
    TxposWeb: TransactionPosweb[];
}