import { Response } from 'src/app/core/models/response';
import { Goals } from '../entity/goals';

export interface BranchGoalResponse{
        Respuesta: Response,
        SucursalesMetas: Goals[]
}

