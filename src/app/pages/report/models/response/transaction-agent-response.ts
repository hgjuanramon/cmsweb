import { TransactionAgent } from '../entity/transaction-agent';
import { Response } from 'src/app/core/models/response';

export interface TransactionAgentResponse{
    Respuesta: Response;
    TxAgentes: TransactionAgent[];
}