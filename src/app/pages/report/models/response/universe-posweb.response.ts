import { Response } from 'src/app/core/models/response';

export interface UniversePoswebResponse{
    Respuesta: Response;
    UrlDescarga: string;
}