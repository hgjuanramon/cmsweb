import { Response } from 'src/app/core/models/response';

export interface TxConstrulograsResponse {
    Respuesta: Response;
    Construlogras: Object[];
}