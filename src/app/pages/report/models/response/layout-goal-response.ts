import { Response } from 'src/app/core/models/response';

export interface LayoutGoalResponse {
    Respuesta: Response;
    Multiplasts: Object[];
}