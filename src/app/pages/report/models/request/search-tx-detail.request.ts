export interface SearchTxDetailRequest {
  IdEmisor: number;
  FechaInicial: string;
  FechaFinal: string;
  Comercio: string;
}
