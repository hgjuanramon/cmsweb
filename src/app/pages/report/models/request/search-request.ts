export interface SearchRequest {
    year?: number,
    month?: number,
    Commerce?: number,
    Date?: string
}