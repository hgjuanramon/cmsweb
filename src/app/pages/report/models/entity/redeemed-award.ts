export interface RedeemedAward {
  IdUsuarioPremio: number;
  Agente: number;
  IdUsuario: number;
  Celular: string;
  NumeroTelefonico: string;
  Participante: string;
  Producto: string;
  ImportePuntos: string;
  FechaCanje: string;
}
