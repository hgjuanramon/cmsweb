export interface TxDetail {
  Comercio: string;
  Fecha: string;
  Hora: string;
  NumTicket: string;
  Tarjeta: string;
  Servicio: string;
  Segmento: string;
  IdTransaccionPagaTodo: string;
  Autorizacion: string;
  Importe: string;
  MontoBonificacion: string;
  MontoRedencion: string;
  Factura: string;
  Medio: string;
  Gerencia: string;
  DescripcionSucursal: string;
  Llavero: string;
  IdSegmento: number;
  IdServicio: string;
}
