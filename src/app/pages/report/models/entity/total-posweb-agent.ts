export interface TotalPoswebAgent{
    Comercio: string;
    IdUsuario: string;
    TipoUsuario: string;
    NombreComercio: string;
    FechaAlta: string;
    FechaCambio: string;
    Estatus: string;
    Bonificaciones: number;
    Redenciones: number;
    Activaciones: number;
}