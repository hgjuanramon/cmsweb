export interface Goals {
    Comercio: number,
    Descripcion: string,
    Meta:  number,
    MetaPuntos: number,
    Tope: number,
    TopePuntos: number,
    FechaActualiza: string
}