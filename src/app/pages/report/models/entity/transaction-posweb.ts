export interface TransactionPosweb{
    IdOperacion: string;
    TipoOperacion: string;
    FechaRegistro: string;
    Importe: string;
    Tarjeta: string;
    Segmento: string;
    Comercio: string;
    MontoBonificado: string;
    MontoRedimido: string;
    TipoAgente: string;
    IdUsuario: string;
    NombreUsuario: string;
    Anio: number;
    Mes: number;
    Dia: number;
}