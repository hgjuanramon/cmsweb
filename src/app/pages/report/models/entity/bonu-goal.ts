export interface BonuGoal {
    Comercio: number;
    IdUsuario: number;
    NombreMeta: string;
    PuntosBonificados: number;
    Cantidad: number;
    Tope: number;
    Meta: number;
    FechaAplicacion: string;
}