export interface TransactionAgent{
    Comercio: number;
    Servicio: string;
    FechaRegistro: string;
    HoraRegistro: string;
    Importe: string;
    Estado: string;
    MedioVenta: string;
    NumTicket: string;
}