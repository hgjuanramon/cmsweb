import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BranchGoalsComponent } from './components/branch-goals/branch-goals.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { BranchBonusGoalsComponent } from './components/branch-bonus-goals/branch-bonus-goals.component';
import { ReportRoutingModule } from './report-routing.module';
import { TransactionsAgentComponent } from './components/transactions-agent/transactions-agent.component';
import { TxConstrulograsComponent } from './components/tx-construlogras/tx-construlogras.component';
import { PrincipalComponent } from './components/principal/principal.component';
import { LayoutGoalComponent } from './components/layout-goal/layout-goal.component';
import { TransactionPoswebComponent } from './components/transaction-posweb/transaction-posweb.component';
import { TotalPoswebAgentComponent } from './components/total-posweb-agent/total-posweb-agent.component';
import { RedeemedAwardComponent } from './components/redeemed-award/redeemed-award.component';
import { TransactionDetailComponent } from './components/transaction-detail/transaction-detail.component';
import { UniversePoswebComponent } from './components/universe-posweb/universe-posweb.component';

@NgModule({
  declarations: [BranchGoalsComponent, BranchBonusGoalsComponent, TransactionsAgentComponent, TxConstrulograsComponent, PrincipalComponent, LayoutGoalComponent, TransactionPoswebComponent, TotalPoswebAgentComponent, RedeemedAwardComponent, TransactionDetailComponent, UniversePoswebComponent],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ReportRoutingModule
  ]
})
export class ReportModule { }
