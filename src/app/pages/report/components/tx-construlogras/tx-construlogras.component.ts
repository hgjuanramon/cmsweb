import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ReportService } from '../../services/report.service';
import { AlertService } from 'src/app/core/services/alert.service';
import { DateHelper } from 'src/app/core/helpers/date-helper';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-tx-construlogras',
  templateUrl: './tx-construlogras.component.html',
  styleUrls: ['./tx-construlogras.component.scss']
})

export class TxConstrulograsComponent implements OnInit, AfterViewInit {
 
  searchForm: FormGroup;
  spinnerVisible = false;
  years : Object[] = [];
  months : Object[] = [];
  @ViewChild("ngSearchForm") ngSearchForm;
  constructor(private fb: FormBuilder, private ref: ChangeDetectorRef, private urlApiService: ReportService, private alertService: AlertService, private dateHelper: DateHelper) { 
    this.years = this.dateHelper.getYears();
    this.months = this.dateHelper.getMonths();
  }

  ngOnInit() {
    this.buildSearchForm();
    this.setDefaultSearchForm();
  }

  ngAfterViewInit(): void {
    this.ref.detectChanges();
  }

  private buildSearchForm(){
    this.searchForm = this.fb.group({
      year: [null, [Validators.required]],
      month: [null, [Validators.required]]
    });
  }

  private setDefaultSearchForm(){
    this.searchForm.patchValue({year: new Date().getFullYear() })
  }

  get year(){ return this.searchForm.get("year") as FormControl;}

  get month(){ return this.searchForm.get("month") as FormControl;}

  public doSearch(): void{
    this.alertService.clear();
    this.spinnerVisible = true;
    this.urlApiService.getAllTxConstrulogras({ year: this.year.value, month: this.month.value }).subscribe(result => {
      this.spinnerVisible = false;
      if(result.Respuesta.CodigoRespuesta == 0){
        this.downloadFile(result.Construlogras,"tx-construlogras");
      }else{
        this.alertService.error(result.Respuesta.Mensaje);
      }
    }, error =>{      
      this.spinnerVisible = false;
      console.log(error);
    })
  }

  public doReset(): void{
    this.alertService.clear();
    this.ngSearchForm.resetForm();
    this.setDefaultSearchForm();
  }

  private downloadFile(items: Object[], nameFile: String = "Reporte") {
    event.preventDefault();
    const ws: XLSX.WorkSheet= XLSX.utils.json_to_sheet(items);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Hoja 1');

    /* save to file */
    XLSX.writeFile(wb, nameFile +'.xlsx');
  }
}
