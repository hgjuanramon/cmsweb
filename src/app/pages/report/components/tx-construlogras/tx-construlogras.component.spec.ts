import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TxConstrulograsComponent } from './tx-construlogras.component';

describe('TxConstrulograsComponent', () => {
  let component: TxConstrulograsComponent;
  let fixture: ComponentFixture<TxConstrulograsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TxConstrulograsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TxConstrulograsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
