import { Component, OnInit, ViewChild } from "@angular/core";
import * as XLSX from "xlsx";
import { ReportService } from "../../services/report.service";
import { AlertService } from "src/app/core/services/alert.service";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { DatePipe } from "@angular/common";

@Component({
  selector: "app-redeemed-award",
  templateUrl: "./redeemed-award.component.html",
  styleUrls: ["./redeemed-award.component.scss"],
  providers: [DatePipe]
})
export class RedeemedAwardComponent implements OnInit {
  spinnerVisible = false;
  searchForm: FormGroup;
  @ViewChild("ngSearchForm") ngSearchForm;
  constructor(
    private fb: FormBuilder,
    private urlReportService: ReportService,
    private alertService: AlertService,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    this.builSearchForm();
  }

  private builSearchForm = (): void => {
    this.searchForm = this.fb.group({
      startDate: [null, [Validators.required]],
      endDate: [null, [Validators.required]]
    });
  };

  get startDate() {
    return this.searchForm.get("startDate") as FormControl;
  }

  get endDate() {
    return this.searchForm.get("endDate") as FormControl;
  }

  public doSearch = (): void => {
    this.alertService.clear();
    this.spinnerVisible = true;
    let parameters = {
      FechaInicio:
        this.datePipe.transform(new Date(this.startDate.value), "yyyy/MM/dd") +
        " 00:00:00",
      FechaFin:
        this.datePipe.transform(new Date(this.endDate.value), "yyyy/MM/dd") +
        " 23:59:59"
    };

    this.urlReportService.getAllRedeemedAward(parameters).subscribe(
      result => {
        this.spinnerVisible = false;
        if (result.Respuesta.CodigoRespuesta == 0) {
          this.downloadFile(result.PremiosPos, "Reporte Premios Canjeados");
        } else {
          this.alertService.error(result.Respuesta.Mensaje);
        }
      },
      error => {
        this.spinnerVisible = false;
        console.log(error);
      }
    );
  };

  public resetForm = (): void => {
    this.alertService.clear();
    this.ngSearchForm.resetForm();
  };

  private downloadFile = (items: Object[], nameFile: String = "Reporte") => {
    event.preventDefault();
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(items, {
      header: [
        "IdUsuarioPremio",
        "FechaCanje",
        "Agente",
        "IdUsuario",
        "Celular",
        "Participante",
        "Producto",
        "ImportePuntos"
      ]
    });
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Hoja 1");

    /* save to file */
    XLSX.writeFile(wb, nameFile + ".xlsx");
  };
}
