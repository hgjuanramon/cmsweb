import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedeemedAwardComponent } from './redeemed-award.component';

describe('RedeemedAwardComponent', () => {
  let component: RedeemedAwardComponent;
  let fixture: ComponentFixture<RedeemedAwardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedeemedAwardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedeemedAwardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
