import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalPoswebAgentComponent } from './total-posweb-agent.component';

describe('TotalPoswebAgentComponent', () => {
  let component: TotalPoswebAgentComponent;
  let fixture: ComponentFixture<TotalPoswebAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalPoswebAgentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalPoswebAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
