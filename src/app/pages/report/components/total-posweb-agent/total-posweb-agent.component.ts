import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ReportService } from '../../services/report.service';
import { AlertService } from 'src/app/core/services/alert.service';
import { DatePipe } from '@angular/common';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-total-posweb-agent',
  templateUrl: './total-posweb-agent.component.html',
  styleUrls: ['./total-posweb-agent.component.scss'],
  providers: [DatePipe]
})
export class TotalPoswebAgentComponent implements OnInit {

  searchForm: FormGroup;
  @ViewChild("ngSearchForm") ngSearchForm;
  spinnerVisible = false;
  constructor(private fb:FormBuilder, private ref: ChangeDetectorRef, private urlReportService: ReportService, private alertService: AlertService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.buildSearchForm();
  }

  ngAfterViewInit(): void {
    this.ref.detectChanges();
  }

  private buildSearchForm(){
    this.searchForm = this.fb.group({
      startDate: [null, [Validators.required]],
      endDate: [null, [Validators.required]]
    });
  }

  get startDate(){ return this.searchForm.get("startDate") as FormControl;}

  get endDate(){ return this.searchForm.get("endDate") as FormControl;}

  public doSearch(){
    this.alertService.clear();
    this.spinnerVisible = true;
    let request = {
      FechaInicio: this.datePipe.transform(new Date(this.startDate.value), "yyyy/MM/dd") + " 00:00:00", 
      FechaFin: this.datePipe.transform(new Date(this.endDate.value), "yyyy/MM/dd") + " 23:59:59" 
    }

    this.urlReportService.getAllTotalPoswebAgent(request).subscribe(result =>{
      this.spinnerVisible = false;
      if(result.Respuesta.CodigoRespuesta == 0){
        this.downloadFile(result.TotalAgentePosWeb, "Reporte Total Agente Posweb");
        this.ngSearchForm.resetForm();
      }else{
        this.alertService.error(result.Respuesta.Mensaje);
      }
    }, error => {
      this.spinnerVisible = false;
      console.log(error);
    })
  }

  public resetForm(){
    this.alertService.clear();
    this.ngSearchForm.resetForm();
  }

  private downloadFile(items: Object[], nameFile: String = "Reporte") {
    event.preventDefault();
    const ws: XLSX.WorkSheet= XLSX.utils.json_to_sheet(items);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Hoja 1');

    /* save to file */
    XLSX.writeFile(wb, nameFile +'.xlsx');
  }

  protected getHeaderTable(){
    return [
      "Comercio",
      "IdUsuario",
      "TipoUsuario",
      "NombreComercio",
      "FechaAlta",
      "FechaCambio",
      "Estatus",
      "Bonificaciones",
      "Redenciones",
      "Activaciones"
    ];
  }
}
