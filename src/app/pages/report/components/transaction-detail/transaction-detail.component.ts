import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  AfterViewInit,
} from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { ReportService } from "../../services/report.service";
import { AlertService } from "src/app/core/services/alert.service";
import { DatePipe } from "@angular/common";
import { TableViewComponent } from "src/app/shared/table-view/table-view.component";

@Component({
  selector: "app-transaction-detail",
  templateUrl: "./transaction-detail.component.html",
  styleUrls: ["./transaction-detail.component.scss"],
  providers: [DatePipe],
})
export class TransactionDetailComponent implements OnInit, AfterViewInit {
  searchForm: FormGroup;
  spinnerVisible = false;
  items: object[] = [];
  public columns: Object[] = [];
  totalOfRecords = 0;
  pageSize = 10;
  urlDownload: string = "#";
  @ViewChild("ngSearchForm") ngSearchForm;
  @ViewChild("tableView") tableView: TableViewComponent<any>;
  constructor(
    private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private urlApiService: ReportService,
    private alertService: AlertService,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    this.buildSearchForm();
  }

  ngAfterViewInit() {
    this.columns = this.getColumns();
    this.ref.detectChanges();
  }

  private buildSearchForm() {
    this.searchForm = this.fb.group({
      FechaInicio: [null, [Validators.required]],
      FechaFinal: [null, [Validators.required]],
      Comercio: [null, [Validators.required]],
    });
  }

  get FechaInicio() {
    return this.searchForm.get("FechaInicio") as FormControl;
  }

  get FechaFinal() {
    return this.searchForm.get("FechaFinal") as FormControl;
  }

  get Comercio() {
    return this.searchForm.get("Comercio") as FormControl;
  }

  public doSearch(): void {
    this.alertService.clear();
    this.spinnerVisible = true;
    this.urlApiService
      .getAllTxDetail({
        IdEmisor: 4,
        FechaInicial:
          this.datePipe.transform(
            new Date(this.FechaInicio.value),
            "yyyy/MM/dd"
          ) + " 00:00:00",
        FechaFinal:
          this.datePipe.transform(
            new Date(this.FechaFinal.value),
            "yyyy/MM/dd"
          ) + " 23:59:59",
        Comercio: this.Comercio.value,
      })
      .subscribe(
        (result) => {
          this.spinnerVisible = false;
          if (result.Respuesta.CodigoRespuesta == 0) {
            this.items = result.Transacciones;
            this.totalOfRecords = result.Transacciones.length;
            this.urlDownload = result.UrlDescarga;
          } else {
            this.items = [];
            this.totalOfRecords = 0;
            this.alertService.error(result.Respuesta.Mensaje);
          }
        },
        (error) => {
          this.spinnerVisible = false;
          console.log(error);
        }
      );
  }

  public doReset(): void {
    this.alertService.clear();
    this.ngSearchForm.resetForm();
  }

  private getColumns() {
    return [
      {
        name: "Comercio",
        prop: "Comercio",
        flexGrow: 0.5,
      },
      {
        name: "Fecha",
        prop: "Fecha",
        flexGrow: 0.5,
      },
      {
        name: "Hora",
        prop: "Hora",
        flexGrow: 0.5,
      },
      {
        name: "Ticket",
        prop: "NumTicket",
        flexGrow: 0.5,
      },
      {
        name: "Tarjeta",
        prop: "Tarjeta",
        flexGrow: 0.5,
      },
      {
        name: "Servicio",
        prop: "Servicio",
        flexGrow: 0.5,
      },
      {
        name: "Segmento",
        prop: "Segmento",
        flexGrow: 0.5,
      },
      {
        name: "Transacción",
        prop: "IdTransaccionPagaTodo",
        flexGrow: 0.5,
      },
      {
        name: "Autorización",
        prop: "Autorizacion",
        flexGrow: 0.5,
      },
      {
        name: "Importe",
        prop: "Importe",
        flexGrow: 0.5,
      },
      {
        name: "Recarga/Bonificación",
        prop: "MontoBonificacion",
        flexGrow: 0.5,
      },
      {
        name: "Compra/Redención",
        prop: "MontoRedencion",
        flexGrow: 0.5,
      },
      {
        name: "Factura",
        prop: "Factura",
        flexGrow: 0.5,
      },
      {
        name: "Gerencia",
        prop: "Gerencia",
        flexGrow: 0.5,
      },
    ];
  }
}
