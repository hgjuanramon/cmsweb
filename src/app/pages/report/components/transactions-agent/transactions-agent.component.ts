import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { TransactionAgent } from '../../models/entity/transaction-agent';
import { TableViewComponent } from 'src/app/shared/table-view/table-view.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReportService } from '../../services/report.service';
import { Title } from '@angular/platform-browser';
import { AlertService } from 'src/app/core/services/alert.service';

@Component({
  selector: 'app-transactions-agent',
  templateUrl: './transactions-agent.component.html',
  styleUrls: ['./transactions-agent.component.scss']
})
export class TransactionsAgentComponent implements OnInit, AfterViewInit {
  
  title = "Consulta Transacción por Agente";

  items : TransactionAgent[] = [];
  public columns : Object[] = [];
  totalOfRecords = 0;
  pageSize = 10; 
  spinnerVisible : boolean = false;
  @ViewChild("tableView") tableView : TableViewComponent<any>;
  @ViewChild("frmSearch") myNgForm;
  searchForm : FormGroup;
  constructor(private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private transAgentService: ReportService,
    private titleService: Title,
    private alertService : AlertService) { }

  ngOnInit() {
    this.buildForm();
    this.titleService.setTitle(this.title);
  }

  buildForm(): void{
    this.searchForm = this.fb.group({
      date: ['',[Validators.required]],
      commerce: ["", [Validators.required, Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$'), Validators.minLength(2), Validators.maxLength(10)]]
    });
  }

  ngAfterViewInit(): void {
    this.columns = this.getColumns();
    this.ref.detectChanges();
  }

  doSearch(formRequest: FormGroup): void{
    this.alertService.clear();
    this.spinnerVisible = true;
    this.transAgentService.getTransactionsByAgent({Commerce: formRequest.value.commerce, Date: formRequest.value.date}).subscribe(result=>{
      this.spinnerVisible = false;
      if(result.Respuesta.CodigoRespuesta != 0){        
        this.alertService.success(result.Respuesta.Mensaje);
        this.items = [];
        this.totalOfRecords = 0;
      }else{
        this.items = result.TxAgentes;
        this.totalOfRecords = result.TxAgentes.length;
      }      
    }, error => {
      this.spinnerVisible = false;
      console.log(error);
    })
  }

  private getColumns() {
    return [
      {
        name: "Comercio",
        prop: "Comercio",
        flexGrow: 0.5
      },{
        name: "Servicio",
        prop: "Servicio",
        flexGrow: 0.5
      },{
        name: "Transacción",
        prop: "IdTx",
        flexGrow: 0.5
      },{
        name: "Num Ticket",
        prop: "NumTicket",
        flexGrow: 0.5
      },{
        name: "Importe",
        prop: "Importe",
        flexGrow: 0.5
      },{
        name: "Fecha Registro",
        prop: "FechaRegistro",
        flexGrow: 0.5
      },{
        name: "Hora Registro",
        prop: "HoraRegistro",
        flexGrow: 0.5
      },{
        name: "Medio Venta",
        prop: "MedioVenta",
        flexGrow: 0.5
      },{
        name: "Estado",
        prop: "Estado",
        flexGrow: 0.5
      }      
    ]
  }

  doReset(){
    this.alertService.clear();
    this.items = [];
    this.myNgForm.resetForm();
  }
}
