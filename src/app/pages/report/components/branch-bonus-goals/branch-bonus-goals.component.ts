import { Component, OnInit, ChangeDetectorRef, ViewChild } from "@angular/core";
import { DateHelper } from "src/app/core/helpers/date-helper";
import { BonuGoal } from "../../models/entity/bonu-goal";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ReportService } from "../../services/report.service";
import { Title } from "@angular/platform-browser";
import { AlertService } from "src/app/core/services/alert.service";
import * as XLSX from "xlsx";

@Component({
  selector: "app-branch-bonus-goals",
  templateUrl: "./branch-bonus-goals.component.html",
  styleUrls: ["./branch-bonus-goals.component.scss"],
  providers: [DateHelper],
})
export class BranchBonusGoalsComponent implements OnInit {
  title = "Consulta Bonificación de Metas";
  years = [];
  months = [];
  items: BonuGoal[] = [];
  public columns: Object[] = [];
  totalOfRecords = 0;
  pageSize = 10;

  searchForm: FormGroup;
  submitted: boolean = false;
  errorForm = "";
  spinnerVisible: boolean = false;
  @ViewChild("frmSearch") myNgForm;
  constructor(
    private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private bonusGoalService: ReportService,
    private dateHelper: DateHelper,
    private titleService: Title,
    public alertService: AlertService
  ) {
    this.years = this.dateHelper.getYears();
    this.months = this.dateHelper.getMonths();
  }

  ngOnInit() {
    this.buildForm();
    this.titleService.setTitle(this.title);
    this.setDefaultSearchForm();
  }

  ngAfterViewInit(): void {
    this.columns = this.getColumns();
    this.ref.detectChanges();
  }

  private buildForm() {
    this.searchForm = this.fb.group({
      year: ["", [Validators.required]],
      month: ["", [Validators.required]],
      commerce: ["", [Validators.pattern("^-?[0-9]\\d*(\\.\\d{1,2})?$")]],
    });
  }

  private setDefaultSearchForm() {
    this.searchForm.patchValue({ year: new Date().getFullYear() });
  }

  public doSearch(formRequest: FormGroup): void {
    this.alertService.clear();
    this.spinnerVisible = true;
    this.bonusGoalService
      .getAllBranchBonusGoals({
        year: formRequest.value.year,
        month: formRequest.value.month,
      })
      .subscribe(
        (response) => {
          this.spinnerVisible = false;
          if (response.Respuesta.CodigoRespuesta !== 0) {
            this.alertService.error(response.Respuesta.Mensaje);
            this.items = [];
          } else {
            if (formRequest.value.commerce) {
              this.items = response.BonificionesMetas.filter(
                (s) => s.Comercio == formRequest.value.commerce
              );
              this.totalOfRecords = this.items.length;
              if (this.items.length == 0) {
                this.alertService.success(
                  "No Existe Información con los Datos Solicitados"
                );
              }
            } else {
              this.items = response.BonificionesMetas;
              this.totalOfRecords = response.BonificionesMetas.length;
            }
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

  public download() {
    if (this.items) {
      this.downloadFile(this.items, "Reporte Bonificación de Metas");
    }
  }

  private downloadFile(items: Object[], nameFile: String = "Reporte") {
    event.preventDefault();

    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(items);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Hoja 1");

    /* save to file */
    XLSX.writeFile(wb, nameFile + ".xlsx");
  }

  private getColumns(): Object[] {
    return [
      {
        name: "Comercio",
        prop: "Comercio",
        flexGrow: 0.5,
      },
      {
        name: "Nombre",
        prop: "Nombre",
        flexGrow: 0.5,
      },
      {
        name: "Nombre Meta",
        prop: "NombreMeta",
        flexGrow: 0.5,
      },
      {
        name: "Puntos Bonificados",
        prop: "PuntosBonificados",
        flexGrow: 0.5,
      },
      {
        name: "Cantidad",
        prop: "Cantidad",
        flexGrow: 0.5,
      },
      {
        name: "Meta",
        prop: "Meta",
        flexGrow: 0.5,
      },
      {
        name: "Tope",
        prop: "Tope",
        flexGrow: 0.5,
      },
      {
        name: "Fecha",
        prop: "FechaAplicacion",
        flexGrow: 0.5,
      },
    ];
  }

  public doReset() {
    this.alertService.clear();
    this.items = [];
    this.myNgForm.resetForm();
    this.setDefaultSearchForm();
  }
}
