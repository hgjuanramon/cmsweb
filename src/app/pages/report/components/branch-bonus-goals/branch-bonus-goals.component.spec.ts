import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchBonusGoalsComponent } from './branch-bonus-goals.component';

describe('BranchBonusGoalsComponent', () => {
  let component: BranchBonusGoalsComponent;
  let fixture: ComponentFixture<BranchBonusGoalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchBonusGoalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchBonusGoalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
