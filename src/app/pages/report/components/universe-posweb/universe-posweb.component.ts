import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { DateHelper } from "src/app/core/helpers/date-helper";
import { ReportService } from "../../services/report.service";
import { AlertService } from "src/app/core/services/alert.service";

@Component({
  selector: "app-universe-posweb",
  templateUrl: "./universe-posweb.component.html",
  styleUrls: ["./universe-posweb.component.scss"],
  providers: [DateHelper],
})
export class UniversePoswebComponent implements OnInit {
  searchForm: FormGroup;
  years = [];
  months = [];
  spinnerVisible: boolean = false;
  @ViewChild("frmSearch") myNgForm;
  title = "Consulta Universo Posweb";
  constructor(
    private fb: FormBuilder,
    private dateHelper: DateHelper,
    private urlService: ReportService,
    private alertService: AlertService
  ) {
    this.years = this.dateHelper.getYears();
    this.months = this.dateHelper.getMonths();
  }

  ngOnInit(): void {
    this.buildFormSearch();
    this.setDefaultSearchForm();
  }

  buildFormSearch() {
    this.searchForm = this.fb.group({
      Anio: [null, [Validators.required]],
      Mes: [null, [Validators.required]],
    });
  }

  private setDefaultSearchForm() {
    this.searchForm.patchValue({ Anio: new Date().getFullYear() });
  }

  public doSearch() {
    this.alertService.clear();
    this.spinnerVisible = true;
    this.urlService.getUniversePosweb(this.searchForm.value).subscribe(
      (result) => {
        this.spinnerVisible = false;
        if (result.Respuesta.CodigoRespuesta != 0) {
          this.alertService.error(result.Respuesta.Mensaje);
        } else {
          window.location.href = result.UrlDescarga;
        }
      },
      (error) => {
        this.spinnerVisible = false;
        console.log(error);
      }
    );
  }

  public doReset() {
    this.alertService.clear();
    this.myNgForm.resetForm();
    this.setDefaultSearchForm();
  }
}
