import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UniversePoswebComponent } from './universe-posweb.component';

describe('UniversePoswebComponent', () => {
  let component: UniversePoswebComponent;
  let fixture: ComponentFixture<UniversePoswebComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniversePoswebComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniversePoswebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
