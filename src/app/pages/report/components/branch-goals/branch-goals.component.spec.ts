import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchGoalsComponent } from './branch-goals.component';

describe('BranchGoalsComponent', () => {
  let component: BranchGoalsComponent;
  let fixture: ComponentFixture<BranchGoalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchGoalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchGoalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
