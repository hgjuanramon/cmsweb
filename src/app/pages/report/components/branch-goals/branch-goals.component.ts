import { Component, OnInit, ViewChild, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { TableViewComponent } from 'src/app/shared/table-view/table-view.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Goals } from '../../models/entity/goals';
import { ReportService } from '../../services/report.service';
import { DateHelper } from 'src/app/core/helpers/date-helper';
import { Title } from '@angular/platform-browser';
import { AlertService } from 'src/app/core/services/alert.service';

@Component({
  selector: 'app-branch-goals',
  templateUrl: './branch-goals.component.html',
  styleUrls: ['./branch-goals.component.scss'],
  providers: [ReportService, DateHelper]
})
export class BranchGoalsComponent implements OnInit, AfterViewInit {

  title = "Consulta de Metas";
  years;
  months;
  items: Goals[] = [];
  public columns: object[] = [];
  totalOfRecords = 0;
  pageSize = 10;

  errorForm = '';
  searchForm : FormGroup;
  submitted = false;
  spinnerVisible : boolean = false;
  @ViewChild("tableView") tableView: TableViewComponent<any>;
  @ViewChild("frmSearch") myNgForm;
  constructor( private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private dateHelper: DateHelper,
    private alertService: AlertService,
    private branchGoalService: ReportService,
    private titleService: Title) {
    this.years = this.dateHelper.getYears();
    this.months = this.dateHelper.getMonths();
   }

  ngOnInit() {
    this.buildForm();    
    this.titleService.setTitle(this.title);
    this.setDefaultSearchForm();
  }

  ngAfterViewInit(): void {
    this.columns = this.getColumns();
    this.ref.detectChanges();
  }

  private buildForm(): void{
    this.searchForm = this.fb.group({
      year: ['',[Validators.required]],
      month: ['',[Validators.required]],
      commerce: ["",[Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')]]
    });
  }

  private setDefaultSearchForm(){
    this.searchForm.patchValue({year: new Date().getFullYear() })
  }
 
  public searchBranchGoals(submittedForm: FormGroup): void{
    this.alertService.clear();
    this.spinnerVisible = true;
    this.branchGoalService.getBranchGoals({ year: submittedForm.value.year, month: submittedForm.value.month})    
    .subscribe(
      response => {
        this.spinnerVisible = false;
        if(response.Respuesta.CodigoRespuesta !== 0){          
          this.alertService.success(response.Respuesta.Mensaje);
          this.items = [];
        }else{
          if(submittedForm.value.commerce){
            this.items = response.SucursalesMetas.filter(s => s.Comercio == submittedForm.value.commerce)
            this.totalOfRecords = this.items.length;
            if(this.items.length == 0){                            
              this.alertService.success("No Existe Información con los Datos Solicitados");
            }
          }else{
            this.items = response.SucursalesMetas;
            this.totalOfRecords = response.SucursalesMetas.length;
          }
        }
      },
    error => {
      this.spinnerVisible = false;
      console.log(error);
    });
  }

  private getColumns(): object[]{
    return [
      {
        name: "Comercio",
        prop: "Comercio",
        flexGrow: 0.5
      },
      {
        name: "Descripción",
        prop: "Descripcion",
        flexGrow: 0.5
      },
      {
        name: "Meta",
        prop: "Meta",
        flexGrow: 0.5
      },
      {
        name: "Meta Puntos",
        prop: "MetaPuntos",
        flexGrow: 0.5
      },
      {
        name: "Tope",
        prop: "Tope",
        flexGrow: 0.5
      },
      {
        name: "Tope Puntos",
        prop: "TopePuntos",
        flexGrow: 0.5
      },
      {
        name: "Fecha",
        prop: "FechaActualiza",
        flexGrow: 0.5
      }
    ]
  }

  public doReset(){
    this.alertService.clear();
    this.items = [];
    this.myNgForm.resetForm();
    this.setDefaultSearchForm();
  }
}
