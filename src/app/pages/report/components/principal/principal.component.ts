import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ChangeDetectorRef,
} from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { Title } from "@angular/platform-browser";
import { TableViewComponent } from "src/app/shared/table-view/table-view.component";

@Component({
  selector: "app-principal",
  templateUrl: "./principal.component.html",
  styleUrls: ["./principal.component.scss"],
})
export class PrincipalComponent implements OnInit {
  title = "Reporte";
  typeReport = 2;
  reportsList = [];
  @Input() items = [];
  @Input() public columns: Object[] = [];
  @Input() pageSize = 11;
  @ViewChild("tableView") tableView: TableViewComponent<any>;
  constructor(
    private fb: FormBuilder,
    private titleService: Title,
    public ref: ChangeDetectorRef
  ) {
    this.reportsList = this.getReportList();
  }

  ngOnInit() {
    this.titleService.setTitle(this.title);
  }

  ngAfterViewInit(): void {
    this.ref.detectChanges();
  }

  public changeEvent(value: string) {
    this.typeReport = parseInt(value);
  }

  private getReportList() {
    return [
      /*{
        id: 1,
        value: "Transacciones Construlogras",
      },*/
      {
        id: 2,
        value: "Layout Metas Posweb",
      },
      /*{
        id: 3,
        value: "Transacciones Posweb",
      },
      {
        id: 4,
        value: "Total Agente Posweb",
      },*/
      {
        id: 5,
        value: "Premios Canjeados",
      },
      {
        id: 6,
        value: "Universo Posweb",
      },
    ];
  }
}
