import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutGoalComponent } from './layout-goal.component';

describe('LayoutGoalComponent', () => {
  let component: LayoutGoalComponent;
  let fixture: ComponentFixture<LayoutGoalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutGoalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutGoalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
