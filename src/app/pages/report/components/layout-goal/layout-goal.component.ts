import { Component, OnInit, ChangeDetectorRef, ViewChild } from "@angular/core";
import { AlertService } from "src/app/core/services/alert.service";
import { DateHelper } from "src/app/core/helpers/date-helper";
import * as XLSX from "xlsx";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";
import { ReportService } from "../../services/report.service";

@Component({
  selector: "app-layout-goal",
  templateUrl: "./layout-goal.component.html",
  styleUrls: ["./layout-goal.component.scss"],
})
export class LayoutGoalComponent implements OnInit {
  searchForm: FormGroup;
  spinnerVisible = false;
  years: Object[] = [];
  months: Object[] = [];
  @ViewChild("ngSearchForm") ngSearchForm;
  constructor(
    private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private urlApiService: ReportService,
    private alertService: AlertService,
    private dateHelper: DateHelper
  ) {
    this.years = this.dateHelper.getYears();
    this.months = this.dateHelper.getMonths();
  }

  ngOnInit() {
    this.buildSearchForm();
    this.setDefaultSearchForm();
  }

  ngAfterViewInit(): void {
    this.ref.detectChanges();
  }

  buildSearchForm() {
    this.searchForm = this.fb.group({
      year: [null, [Validators.required]],
      month: [null, [Validators.required]],
    });
  }

  private setDefaultSearchForm() {
    this.searchForm.patchValue({ year: new Date().getFullYear() });
  }

  get year() {
    return this.searchForm.get("year") as FormControl;
  }

  get month() {
    return this.searchForm.get("month") as FormControl;
  }

  public doSearch(): void {
    this.alertService.clear();
    this.spinnerVisible = true;
    this.urlApiService
      .getAllLayoutGoals({ year: this.year.value, month: this.month.value })
      .subscribe(
        (result) => {
          this.spinnerVisible = false;
          if (result.Respuesta.CodigoRespuesta == 0) {
            let records = this.getDataFilter(result.Multiplasts);
            for (let item in records) {
              this.downloadFile(
                records[item],
                "PTLOY-MTA-CARGA-" +
                  this.year.value +
                  "" +
                  this.month.value +
                  "-" +
                  item
              );
            }
          } else {
            this.alertService.error(result.Respuesta.Mensaje);
          }
        },
        (error) => {
          this.spinnerVisible = false;
          console.log(error);
        }
      );
  }

  public doReset(): void {
    this.alertService.clear();
    this.ngSearchForm.resetForm();
    this.setDefaultSearchForm();
  }

  private downloadFile(items: Object[], nameFile: String = "Reporte") {
    event.preventDefault();
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(items, {
      skipHeader: true,
    });
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Hoja1");

    /* save to file */
    XLSX.writeFile(wb, nameFile + ".xlsx");
  }

  private getDataFilter(records: Object[], filter: string = "Multiplast") {
    let results = this.getGroupGoal(records);
    for (let item in results) {
      this.removeItemList(results[item]);
      var commerce = "";
      switch (item) {
        case "Multiplast":
          commerce = "770001";
          break;
        case "Financiamiento":
          commerce = "770003";
          break;
        default:
          commerce = "770002";
          break;
      }

      results[item].unshift({
        IdUsuario: this.year.value,
        Nombre: this.month.value,
        Comercio: commerce,
      });
    }

    return results;
  }

  private removeItemList(records: object[]) {
    for (var i = 0; i < records.length; i++) {
      delete records[i]["Meta"];
    }
  }

  private getGroupGoal(records: Object[]) {
    const groupBy = (array, key) => {
      // Return the end result
      return array.reduce((result, currentValue) => {
        // If an array already present for key, push it to the array. Else create an array and push the object
        (result[currentValue[key]] = result[currentValue[key]] || []).push(
          currentValue
        );
        // Return the current iteration `result` value, this will be taken as next iteration `result` value and accumulate
        return result;
      }, {}); // empty object is the initial value for result object
    };

    return groupBy(records, "Meta");
  }
}
