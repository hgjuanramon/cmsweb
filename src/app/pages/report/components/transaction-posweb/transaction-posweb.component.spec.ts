import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionPoswebComponent } from './transaction-posweb.component';

describe('TransactionPoswebComponent', () => {
  let component: TransactionPoswebComponent;
  let fixture: ComponentFixture<TransactionPoswebComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionPoswebComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionPoswebComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
