import { Component, OnInit, ViewChild, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReportService } from '../../services/report.service';
import * as XLSX from 'xlsx';
import { AlertService } from 'src/app/core/services/alert.service';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-transaction-posweb',
  templateUrl: './transaction-posweb.component.html',
  styleUrls: ['./transaction-posweb.component.scss'],
  providers: [DatePipe]
})
export class TransactionPoswebComponent implements OnInit, AfterViewInit {

  searchForm: FormGroup;
  @ViewChild("ngSearchForm") ngSearchForm;
  spinnerVisible = false;
  constructor(private fb:FormBuilder, private ref: ChangeDetectorRef, private urlReportService: ReportService, private alertService: AlertService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.buildSearchForm();
  }

  ngAfterViewInit(): void {
    this.ref.detectChanges();
  }

  private buildSearchForm(){
    this.searchForm = this.fb.group({
      startDate: [null, [Validators.required]],
      endDate: [null, [Validators.required]]
    });
  }

  get startDate(){ return this.searchForm.get("startDate") as FormControl;}

  get endDate(){ return this.searchForm.get("endDate") as FormControl;}

  public doSearch(){
    
    this.spinnerVisible = true;
    let request = {
      FechaInicio: this.datePipe.transform(new Date(this.startDate.value), "yyyy/MM/dd") + " 00:00:00", 
      FechaFin: this.datePipe.transform(new Date(this.endDate.value), "yyyy/MM/dd") + " 23:59:59" 
    }

    this.urlReportService.getAllTransactionPosweb(request).subscribe(result =>{
      this.spinnerVisible = false;
      if(result.Respuesta.CodigoRespuesta == 0){
        this.downloadFile(result.TxposWeb, "Reporte Transacciones");
        this.ngSearchForm.resetForm();
      }else{
        this.alertService.error(result.Respuesta.Mensaje);
      }
    }, error => {
      this.spinnerVisible = false;
      console.log(error);
    })
  }

  public resetForm(){
    this.alertService.clear();
    this.ngSearchForm.resetForm();
  }

  private downloadFile(items: Object[], nameFile: String = "Reporte") {
    event.preventDefault();

    const ws: XLSX.WorkSheet= XLSX.utils.json_to_sheet(items);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Hoja 1');

    /* save to file */
    XLSX.writeFile(wb, nameFile +'.xlsx');
  }

  protected getHeaderTable(){
    return [
      "Operación",
      "Tipo Operación",
      "Fecha Registro",
      "Importe",
      "Tarjeta",
      "Segmento",
      "Comercio",
      "Monto Bonificado",
      "Monto Redimido",
      "Tipo Agente",
      "ID Usuario",
      "Nombre Usuario",
      "Año",
      "Mes",
      "Día"
    ];
  }
}
