import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { SearchRequest } from "../models/request/search-request";
import { BranchGoalResponse } from "../models/response/branch-goal-response";
import { BonuGoalResponse } from "../models/response/bonu-goal-response";
import { TransactionAgentResponse } from "../models/response/transaction-agent-response";
import { TxConstrulograsResponse } from "../models/response/tx-construlogras-response";
import { LayoutGoalResponse } from "../models/response/layout-goal-response";
import { TransactionPoswebResponse } from "../models/response/transaction-posweb.response";
import { SearchPosRequest } from "../models/request/search-pos-request";
import { TotalPoswebAgentResponse } from "../models/response/total-posweb-agent.response";
import { RedeemedAwardResponse } from "../models/response/redeemed-award.response";
import { ReportConstant } from "../constants/report.constant";
import { SearchTxDetailRequest } from "../models/request/search-tx-detail.request";
import { TxDetailResponse } from "../models/response/tx-detail.response";
import { SearchUniverseRequest } from '../models/request/search-universe-request';
import { UniversePoswebResponse } from '../models/response/universe-posweb.response';

@Injectable({
  providedIn: "root",
})
export class ReportService {
  constructor(private httpClient: HttpClient) {}

  public getBranchGoals = (
    request: SearchRequest
  ): Observable<BranchGoalResponse> => {
    return this.httpClient.post<BranchGoalResponse>(
      ReportConstant.GET_BRANCH_GOALS,
      { Anio: request.year, Mes: request.month }
    );
  };

  public getAllBranchBonusGoals = (
    request: SearchRequest
  ): Observable<BonuGoalResponse> => {
    return this.httpClient.post<BonuGoalResponse>(
      ReportConstant.GET_ALL_BRANCH_BONUS_GOALS,
      { Anio: request.year, Mes: request.month }
    );
  };

  public getTransactionsByAgent = (
    request: SearchRequest
  ): Observable<TransactionAgentResponse> => {
    return this.httpClient.post<TransactionAgentResponse>(
      ReportConstant.GET_TRANSACTIONS_BY_AGENT,
      { Comercio: request.Commerce, Fecha: request.Date }
    );
  };

  public getAllTxConstrulogras = (
    request: SearchRequest
  ): Observable<TxConstrulograsResponse> => {
    return this.httpClient.post<TxConstrulograsResponse>(
      ReportConstant.GET_ALL_TX_CONSTRULOGRAS,
      { Anio: request.year, Mes: request.month }
    );
  };

  public getAllLayoutGoals = (
    request: SearchRequest
  ): Observable<LayoutGoalResponse> => {
    return this.httpClient.post<LayoutGoalResponse>(
      ReportConstant.GET_ALL_LAYOUT_GOALS,
      { Anio: request.year, Mes: request.month }
    );
  };

  public getAllTransactionPosweb = (
    request: SearchPosRequest
  ): Observable<TransactionPoswebResponse> => {
    return this.httpClient.post<TransactionPoswebResponse>(
      ReportConstant.GET_ALL_TRANSACTION_POSWEB,
      request
    );
  };

  public getAllTotalPoswebAgent = (
    request: SearchPosRequest
  ): Observable<TotalPoswebAgentResponse> => {
    return this.httpClient.post<TotalPoswebAgentResponse>(
      ReportConstant.GET_ALL_TOTAL_POSWEB_AGENT,
      request
    );
  };

  public getAllRedeemedAward = (
    request: SearchPosRequest
  ): Observable<RedeemedAwardResponse> => {
    return this.httpClient.post<RedeemedAwardResponse>(
      ReportConstant.GET_ALL_REDEEMED_AWARD,
      request
    );
  };

  public getAllTxDetail = (
    request: SearchTxDetailRequest
  ): Observable<TxDetailResponse> => {
    return this.httpClient.post<TxDetailResponse>(
      ReportConstant.GET_TX_DETAIL,
      request
    );
  };

public getUniversePosweb = (
  request: SearchUniverseRequest
): Observable<UniversePoswebResponse> => {
  return this.httpClient.post<UniversePoswebResponse>(
    ReportConstant.GET_UNIVERSE_POSWEB,
    request
  );
};
}
