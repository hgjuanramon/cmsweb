import { environment } from "src/environments/environment";

export class ReportConstant {
  public static GET_BRANCH_GOALS = `${environment.urlService}/Callcenter/ObtieneSucursalesMetas`;

  public static GET_ALL_BRANCH_BONUS_GOALS = `${environment.urlService}/Callcenter/ObtieneBonificacionesMetas`;

  public static GET_TRANSACTIONS_BY_AGENT = `${environment.urlService}/Callcenter/ObtieneTxAgentes`;

  public static GET_ALL_TX_CONSTRULOGRAS = `${environment.urlService}/Operacion/ObtieneTxConstrulogras`;

  public static GET_ALL_LAYOUT_GOALS = `${environment.urlService}/Operacion/ObtieneLayoutMultiplast`;

  public static GET_ALL_TRANSACTION_POSWEB = `${environment.urlService}/Operacion/ObtieneTxPosWeb`;

  public static GET_ALL_TOTAL_POSWEB_AGENT = `${environment.urlService}/Operacion/ObtieneTotalAgentePosWeb`;

  public static GET_ALL_REDEEMED_AWARD = `${environment.urlService}/Operacion/ObtienePremiosCanjeados`;

  public static GET_TX_DETAIL = `${environment.urlService}/ReportsPosweb/ObtieneTransacciones`;

  public static GET_UNIVERSE_POSWEB = `${environment.urlService}/ReportsPosweb/ObtieneUniversoPosWeb`;
}
