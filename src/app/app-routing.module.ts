import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { NotFoundComponent } from "./shared/not-found/not-found.component";
import { LoginComponent } from "./pages/login/login.component";
import { LogoutComponent } from "./pages/logout/logout.component";
import { AuthLayoutComponent } from "./shared/layout/auth-layout/auth-layout.component";
import { AdminLayoutComponent } from "./shared/layout/admin-layout/admin-layout.component";
import { AuthGuard } from "./core/guards/auth.guard";
import { RecoverPasswordComponent } from "./pages/recover-password/recover-password.component";
import { ChangePasswordComponent } from "./pages/change-password/change-password.component";

const routes: Routes = [
  {
    path: "home",
    component: AdminLayoutComponent,
    loadChildren: () =>
      import("./pages/home/home.module").then((m) => m.HomeModule),
    canLoad: [AuthGuard],
  },
  {
    path: "reports",
    component: AdminLayoutComponent,
    loadChildren: () =>
      import("./pages/report/report.module").then((m) => m.ReportModule),
    canLoad: [AuthGuard],
  },
  {
    path: "users",
    component: AdminLayoutComponent,
    loadChildren: () =>
      import("./pages/user/user.module").then((m) => m.UserModule),
    canLoad: [AuthGuard],
  },
  {
    path: "users-pos",
    component: AdminLayoutComponent,
    loadChildren: () =>
      import("./pages/user-pos/user-pos.module").then((m) => m.UserPosModule),
    canLoad: [AuthGuard],
  },
  {
    path: "internal-users",
    component: AdminLayoutComponent,
    loadChildren: () =>
      import("./pages/users/users.module").then((m) => m.UsersModule),
    canLoad: [AuthGuard],
  },
  {
    path: "loyalty",
    component: AdminLayoutComponent,
    loadChildren: () =>
      import("./pages/check-data/check-data.module").then(
        (m) => m.CheckDataModule
      ),
    canLoad: [AuthGuard],
  },
  {
    path: "starbucks",
    component: AdminLayoutComponent,
    loadChildren: () =>
      import("./pages/starbucks/starbucks.module").then(
        (m) => m.StarbucksModule
      ),
    canLoad: [AuthGuard],
  },
  {
    path: "control-desk",
    component: AdminLayoutComponent,
    loadChildren: () =>
      import("./pages/control-desk/control-desk.module").then(
        (m) => m.ControlDeskModule
      ),
    canLoad: [AuthGuard],
  },
  {
    path: "office-depot",
    component: AdminLayoutComponent,
    loadChildren: () =>
      import("./pages/office-depot/office-depot.module").then(
        (m) => m.OfficeDepotModule
      ),
    canLoad: [AuthGuard],
  },
  {
    path: "campaign-manager",
    component: AdminLayoutComponent,
    loadChildren: () =>
      import("./pages/campaign-manager/campaign-manager.module").then(
        (m) => m.CampaignManagerModule
      ),
    canLoad: [AuthGuard],
  },
  {
    path: "",
    redirectTo: "login",
    pathMatch: "full",
  },
  {
    path: "login",
    component: AuthLayoutComponent,
    children: [
      {
        path: "",
        component: LoginComponent,
      },
    ],
  },
  {
    path: "logout",
    component: LogoutComponent,
  },
  {
    path: "recover-password",
    component: AuthLayoutComponent,
    children: [
      {
        path: "",
        component: RecoverPasswordComponent,
      },
    ],
  },
  {
    path: "change-password/:token",
    component: AuthLayoutComponent,
    children: [
      {
        path: "",
        component: ChangePasswordComponent,
      },
    ],
  },
  {
    path: "**",
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
