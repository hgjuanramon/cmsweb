import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PagesModule } from "./pages/pages.module";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthService } from "./pages/auth/auth.service";
import { CoreModule } from "./core/core.module";
import { AuthHttpInterceptor } from "./core/interceptor/http-interceptor";
import { AuthGuard } from "./core/guards/auth.guard";
import { MAT_DATE_LOCALE } from "@angular/material/core";
import { NgIdleKeepaliveModule } from "@ng-idle/keepalive";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    PagesModule,
    NgIdleKeepaliveModule.forRoot(),
  ],
  providers: [
    AuthService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true,
    },
    {
      provide: MAT_DATE_LOCALE,
      useValue: "es-GB",
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
