import { FormGroup, AbstractControl } from '@angular/forms';

export class FormValidator {
    // custom validator to check that two fields match
    static mustMatch(controlName: string, matchingControlName: string) {
        return (formGroup: FormGroup) => {
            const control = formGroup.controls[controlName];
            const matchingControl = formGroup.controls[matchingControlName];

            if (matchingControl.errors && !matchingControl.errors.mustMatch) {
                // return if another validator has already found an error on the matchingControl
                return;
            }

            // set error on matchingControl if validation fails
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({ mustMatch: true });
            } else {
                matchingControl.setErrors(null);
            }
        }
    }

    static number(formControl: AbstractControl): { [key: string]: any } | null  {        
        const NUMBER_REGEXP = /^[.\d]+$/;
        const valid = NUMBER_REGEXP.test(formControl.value);
        return valid ? null : { invalidNumber: true };
     }   
}


