import { Page } from './page';

export class PagedDatatable<T> {
    data = new Array<T>();
    page = new Page();
}