import { Response } from './response';

export interface UserRolResponse{
    Respuesta: Response,
    RolesPermisos: UserRol[]
}

export interface Navbar {
    displayName: string;
    disabled?: boolean;
    iconName: string;
    route?: string;
    children?: Navbar[];
}

export interface UserRol{
    Rol: string,
    Menu: string,
    Url: string,
    Id_Url: number
}