export interface navItem{
    displayName: string;
    disabled?: boolean;
    iconName: string;
    route?: string;
    children?: navItem[];
}