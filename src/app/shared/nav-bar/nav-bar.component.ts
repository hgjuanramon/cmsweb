import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { AuthService } from "src/app/pages/auth/auth.service";
import { UserRolResponse, Navbar } from "src/app/core/models/navbar";

@Component({
  selector: "app-nav-bar",
  templateUrl: "./nav-bar.component.html",
  styleUrls: ["./nav-bar.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class NavBarComponent implements OnInit {
  navItems: Navbar[];
  username: string = "Usuario";
  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.buildListMenu();
    this.username = this.authService.getToken();
  }

  buildListMenu = (): void => {
    this.authService.getMenus().subscribe(response => {
      this.navItems = this._buildMenus(response) as Navbar[];
    });
  };

  _buildMenus = (records: UserRolResponse) => {
    var myArray = [];
    if (records.RolesPermisos) {
      var items = records.RolesPermisos.reduce(function(obj, item) {
        obj[item.Rol] = obj[item.Rol] || [];
        obj[item.Rol].push({
          displayName: item.Menu,
          route: item.Url,
          iconName: "arrow_right"
        });
        return obj;
      }, {});

      myArray = Object.keys(items).map(function(key) {
        return {
          displayName: key,
          iconName: "more_vert",
          children: items[key]
        };
      });
    }

    return myArray;
  };
}
