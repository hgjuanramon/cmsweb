import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AuthLayoutComponent } from './layout/auth-layout/auth-layout.component';
import { AdminLayoutComponent } from './layout/admin-layout/admin-layout.component';
import { HorizontalComponent } from './nav-bar/horizontal/horizontal.component';
import { TableViewComponent } from './table-view/table-view.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SpinnerComponent } from './spinner/spinner.component';
import { AlertComponent } from './alert/alert.component';

@NgModule({
  declarations: [NavBarComponent, NotFoundComponent, AuthLayoutComponent, AdminLayoutComponent, HorizontalComponent, TableViewComponent, SpinnerComponent, AlertComponent],
  imports: [
    CommonModule,
    RouterModule,   
    NgxDatatableModule,
    MaterialModule,
    FlexLayoutModule,
  ],
  exports: [NavBarComponent,NotFoundComponent,AuthLayoutComponent,HorizontalComponent, TableViewComponent,SpinnerComponent, AlertComponent]
})
export class SharedModule { }
