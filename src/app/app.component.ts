import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';


declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'Pagatodo Tools';
  

  constructor(private titleService: Title){
    
  }

  ngOnInit():void{
    this.titleService.setTitle(this.title);    
  }  
}
