export const environment = {
  production: true,
  //urlService: "http://192.168.80.31:9701/api",
  urlService: "http://200.53.182.252:9701/api",
  //urlOfficeDepot: "http://192.168.80.31:9701/Utils/api",
  urlOfficeDepot: "http://200.53.182.252:9701/api",
  timeOut: 7200,
  prefixCard: "63950916",
  prefixOfficeDepotCard: "63950902",
  prefixFile: {
    alta: "PLGenAltaAnonima",
  },
  fullcargaVersion: 12,
};
